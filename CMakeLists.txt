cmake_minimum_required (VERSION 3.15)

project(labRemote
  DESCRIPTION "Library for controlling laboratory equipment.")

# The version number.
set(labRemote_VERSION_MAJOR 1)
set(labRemote_VERSION_MINOR 1)

# Nicer structure for binary files
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)


set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake/")

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_MACOSX_RPATH 1)

# Allow internal or external third-party libraries
option(USE_EXTERNAL_JSON "Use an external nlohmann JSON library" OFF)
option(USE_EXTERNAL_INFLUXDBCPP "Use an external influxdb-cpp library" OFF)

option(USE_PYTHON "Make python bindings" OFF)

# Source code
add_subdirectory(src)
add_subdirectory(doc)
