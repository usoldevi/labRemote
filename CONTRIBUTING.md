# Guidelines for contributing to labRemote

__Project Manager:__ Elisabetta Pianori

__Project Maintainers:__
- Timon Heim
- Simone Pagan Griso
- Karol Krizka
- Elisabetta Pianori
- Daniel Antrim

## Definition of Roles

The _Project Manager_'s role is to keep an overview of the development happening in labRemote. The PM's responsibilities include:
- being the main contact for labRemote
- regularly merging the `devel` branch into `main`
- merging all Merge Requests

The _Maintainers_ have a very good knowledge of the labRemote code base and are responsible for reviewing Merge Requests.

## Usage of The devel Branch
The `main` branch is to be assumed as relatively stable. All new development should be merged into the `devel` branch. The `devel` branch is __not__ to be used for applications!

The `devel` branch will be merged into `main` every Monday, with weeks skipped based on the Project Manager's discretion.

## Making Changes to labRemote

Changes to the `labRemote` repository by any developer are implemented by submitting
Merge Requests (MR) to the main repository's `devel` branch.

If you wish to make changes in
order to add new features, fix bugs, etc... you must then first start a  *WIP Merge Request*
as soon as possible (see [here](https://repository.prace-ri.eu/git/help/user/project/merge_requests/work_in_progress_merge_requests.md) for info
about WIP MR workflow). Adding the _WIP_ flag lets everyone know that you have started the work and prevents
duplication of work.

Once you are happy with the state of the work, remove the _WIP_ flag on your MR.
This will let the Project Manager and Maintainers know that the code review process
should begin. See [Review of Merge Requests](#review-of-merge-requests) below.

The MR will be merged by the Project Manager.

### Review of Merge Requests
All merge requests must be approved by at least two Maintainers; one of them being the Project Manager and neither being the person initiating the MR. 

All discussions need to be marked as resolved before a MR can proceed. It is the job of the person initiating the discussion to mark a discussion as resolved.
