// Arduino Uno
#if defined(ARDUINO_AVR_UNO)
static const uint8_t analog_pins[] = {A0, A1, A2, A3, A4, A5};
static const uint8_t n_digital_pins = 14;

// Arduino mega
#elif defined(ARDUINO_AVR_MEGA2560)
static const uint8_t analog_pins[] = {A0, A1, A2,  A3,  A4,  A5,  A6,  A7,
                                      A8, A9, A10, A11, A12, A13, A14, A15};
static const uint8_t n_digital_pins = 54;

// Arduino Nano
#elif defined(ARDUINO_AVR_NANO)
static const uint8_t analog_pins[] = {A0, A1, A2, A3, A4, A5, A6, A7};
static const uint8_t n_digital_pins = 22;

#endif
