# - Try to find influxdb-cpp
# https://github.com/orca-zhang/influxdb-cpp
#
# Once done this will define
#  INFLUXDBCPP_FOUND - System has influxdb-cpp
#  INFLUXDBCPP_INCLUDE_DIR - The influxdb-cpp include directories

FIND_PATH(INFLUXDBCPP_INCLUDE_DIR influxdb.hpp
          HINTS /usr/include)

# FIND_LIBRARY(INFLUXDBCPP_LIBRARY NAMES mpsse libinfluxdbcpp
#               HINTS /usr/lib64 )

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set INFLUXDBCPP_FOUND to TRUE
# if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(influxdbcpp  DEFAULT_MSG
  INFLUXDBCPP_INCLUDE_DIR)

MARK_AS_ADVANCED(INFLUXDBCPP_INCLUDE_DIR)
