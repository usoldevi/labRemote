import sys
import setuptools_scm  # noqa: F401

try:
    from skbuild import setup
except ImportError:
    print('Please update pip, you need pip 10 or greater,\n'
          ' or you need to install the PEP 518 requirements in pyproject.toml yourself', file=sys.stderr)
    raise

setup(
    name="labRemote",
    cmake_args=['-DUSE_PYTHON=on'],
    packages=['labRemote'],
    package_dir={'': 'src'},
    cmake_install_dir='src/labRemote',
    include_package_data=True,
)
