from labRemote import devcom, com

from argparse import ArgumentParser
import sys
from datetime import datetime

import logging
logging.basicConfig()
logger = logging.getLogger("Si7021_example")

def si7021_example(device, baud, address) :

    ##
    ## initialize the communication line
    ##
    try :
        baud_rate = getattr(com.SerialCom.BaudRate, 'Baud{0:d}'.format(baud))
        serial = com.TextSerialCom(device, baud_rate)
    except :
        logger.error(f"Unable to initialize serial communication with device at port \"{device}\"")
        sys.exit(1)
    if serial is None :
        logger.error(f"Returned communication device is None")
        sys.exit(1)
    serial.setTermination("\r\n")
    serial.init()

    ##
    ## setup the I2C bus line
    ##
    i2c = devcom.I2CDevComuino(address, serial)

    ##
    ## initialize the sensor
    ##
    sensor = devcom.Si7021(i2c)
    sensor.init()

    ##
    ## perform continuous measurements
    ##
    while True :
        time.sleep(1)
        sensor.read()
        now = datetime.now()
        logger.info(f"Measurement[{now}]: humidity = {sensor.humidity()}, temperature = {sensor.temperature()}")

if __name__ == "__main__" :

    parser = ArgumentParser(description = "Example of how to use the Si7021 device")
    parser.add_argument("-p", "--port", required = True, type = str,
        help = "Port at which the device is connected (e.g. /dev/ttyAMC0)"
    )
    parser.add_argument("-b", "--baud", default = 9600, type = int,
        help = "Baud rate for serial communication with the device"
    )
    parser.add_argument("-a", "--address", default = 0x40, type = int,
        help = "I2C bus address for the Si7021 device"
    )
    args = parser.parse_args()

    si7021_example(args.port, args.baud, args.address)
