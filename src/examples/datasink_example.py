import labRemote
from labRemote import ec

from datetime import datetime
from argparse import ArgumentParser
import sys
from pathlib import Path

import logging
logging.basicConfig()
logger = logging.getLogger("datasink_example")

def datasink_example(config_file, stream_name) :

    ##
    ## Create the datastream
    ##
    ds = ec.DataSinkConf()
    try :
        ds.setHardwareConfig(config_file)
    except :
        logger.error("Failed to load datastream configuration")
        sys.exit(1)
    if ds is None :
        logger.error("Failed to load datastream configuration (it is None)")
        sys.exit(1)

    stream = ds.getDataStream(stream_name)
    if stream is None :
        logger.error(f"Requested stream \"{stream_name}\" not found")
        sys.exit(1)
    
    ##
    ## record some measurements
    ##
    stream.startMeasurement("climate", datetime.now())
    stream.setField("temperature", 48623)
    stream.recordPoint()
    stream.endMeasurement()
    
    stream.setTag("location","fridge")
    stream.startMeasurement("climate", datetime.now())
    stream.setField("temperature", -23453.1)
    stream.recordPoint()
    stream.endMeasurement()

if __name__ == "__main__":

    parser = ArgumentParser(description = "Example of how to use DataSinks and DataStreams")
    parser.add_argument("config-file", type = str,
        help = "labRemote JSON configuration file for datasink and datastream object(s)"
    )
    parser.add_argument("stream-name", type = str,
        help = "Name of the datastream to use"
    )
    parser.add_argument("-d", "--debug", action = "count", default = 0,
        help = "Enable more verbose printout"
    )
    args = parser.parse_args()

    for _ in range(args.debug) :
        labRemote.incrDebug()

    config_path = Path(args.config_file)
    if not config_path.exists() or not config_path.is_file() :
        logger.error(f"Provided configuration file \"{args.config_file}\" could not be found")
        sys.exit(1)

    datasink_example(args.config_file, args.stream_name)
