//#include "SerialCom.h"
#include <chrono>
#include <cstdint>
#include <iomanip>
#include <sstream>
#include <thread>

#include "Logger.h"
#include "TextSerialCom.h"

// Example code on how to send commands to a Si7021 sensor connected to the
// Aruino via I2C
// https://cdn-learn.adafruit.com/assets/assets/000/035/931/original/Support_Documents_TechnicalDocs_Si7021-A20.pdf

int main(int argc, char* argv[]) {
    TextSerialCom com("/dev/ttyACM2", SerialCom::BaudRate::Baud9600);
    com.setTermination("\r\n");
    com.init();

    logIt::incrDebug();
    logIt::incrDebug();
    logIt::incrDebug();

    // Not fully sure why a long sleep here is needed, but
    // without it the code hangs the first time the Arduino
    // is accessed after uploading a sketch
    std::this_thread::sleep_for(std::chrono::seconds(5));

    //
    // Fun test commands

    com.send("I2C WRITE 40 84B8");
    com.receive();

    com.send("I2C READ 40 1");
    std::string response = com.receive();
    logger(logDEBUG) << response;

    com.send("I2C WRITE 40 0xE7");
    com.receive();

    com.send("I2C READ 40 1");
    std::string response1 = com.receive();
    logger(logINFO) << "Register status: " << response1;

    while (true) {  // measure humidity
        com.send("I2C WRITE 40 F5");
        com.receive();

        com.send("I2C READ 40 3");
        std::string response2 = com.receive();

        std::stringstream ss;
        uint32_t out;
        ss << std::hex << response2;
        ss >> out;
        uint16_t h = (out >> 8) & 0xffff;
        uint16_t hc = out & 0x0000ff;

        logger(logDEBUG) << "Raw Humidity data from sensor: " << response2
                         << "  raw Humidity: " << h << " Checksum:" << hc;

        float humidata = (125 * h / 65536.) - 6;
        logger(logINFO) << "Calibrated humidity: " << humidata;

        com.send("I2C WRITE 40 E0");
        com.receive();

        com.send("I2C READ 40 3");
        std::string response3 = com.receive();

        ss << std::hex << response3;
        ss >> out;
        uint16_t t = (out >> 8) & 0xffff;
        uint16_t tc = out & 0x0000ff;
        logger(logDEBUG) << "Raw Temperature data from sensor: " << response3
                         << "  raw Temperature: " << t << " Checksum:" << tc;

        float tempdata = (175.72 * t / 65536) - 46.85;
        logger(logINFO) << "Calibrated Temperature:" << tempdata;
    }

    return 0;
}
