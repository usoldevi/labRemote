import labRemote
from labRemote import com

from argparse import ArgumentParser
import sys, time

import logging
logging.basicConfig()
logger = logging.getLogger("devcomuino_example")

def decode_sensor_data(response, humidity = True) :

    raw = int(response, 16)
    measurement = (raw >> 8) & 0xffff
    checksum = raw & 0x0000ff
    if humidity :
        calibrated = (125 * measurement/65536.) - 6
    else :
        calibrated = (175.75 * measurement/65536.) - 46.85
    return measurement, checksum, calibrated

def devcomuino_example(device, baud) :

    ##
    ## initialize the communication line
    ##
    try :
        baud_rate = getattr(com.SerialCom.BaudRate, 'Baud{0:d}'.format(baud))
        serial = com.TextSerialCom(device, baud_rate)
    except :
        logger.error(f"Unable to initialize serial communication with device at port \"{device}\"")
        sys.exit(1)
    if serial is None :
        logger.error(f"Returned communication device is None")
        sys.exit(1)
    serial.setTermination("\r\n")
    serial.init()

    ##
    ## wait for Arduino device to sync
    ##
    time.sleep(2)

    ##
    ## a couple of random test commands
    ##
    serial.send("I2C WRITE 40 84B8")
    serial.receive()

    serial.send("I2C READ 40 1")
    logger.info(f"Response: {serial.receive()}")

    serial.send("I2C WRITE 40 0xE7")
    serial.receive()

    serial.send("I2C READ 40 1")
    logger.info(f"Response: {serial.receive()}")

    while True :
        serial.send("I2C WRITE 40 F5")
        serial.receive()

        serial.send("I2C READ 40 3")
        reponse = serial.receive()
        logger.info(f"Response: {response}")

        humidity, checksum, calibrated = decode_sensor_data(response, humidity = True)
        logger.info(f"Raw humidity data from sensor    : {response}, raw humidity: {hex(humidity)}, checksum: {hex(checksum)}")
        logger.info(f"Calibrated humidity              : {calibrated}")

        serial.send("I2C WRITE 40 E0")
        serial.receive()

        serial.send("I2C READ 40 3")
        response = serial.receive()

        temp, checksum, calibrated = decode_sensor_data(response, humidity = False)
        logger.info(f"Raw temperature data from sensor : {response}, raw temperature: {hex(temp)}, checksum: {hex(checksum)}")
        logger.info(f"Calibrated temperature           : {calibrated}")

        time.sleep(2)

if __name__ == "__main__" :

    parser = ArgumentParser(description = "Example usage of devcomuino")
    parser.add_argument("-p", "--port", required = True, type = str,
        help = "Port at which Arduino is connected (e.g. /dev/ttyACM1)"
    )
    parser.add_argument("-b", "--baud", default = 9600, type = int,
        help = "Baud rate for USB communication with the connected Arduino device"
    )
    parser.add_argument("-d", "--debug", action = "count", default = 0,
        help = "Enable more verbose printout messages from labRemote"
    )
    args = parser.parse_args()

    for _ in range(args.debug) :
        args.incrDebug()

    devcomuino_example(args.port, args.baud)
