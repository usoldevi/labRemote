#include <iomanip>
#include <iostream>

#include "CharDeviceCom.h"
#include "DMM6500.h"
#include "Logger.h"

int main(int argc, char* argv[]) {
    DMM6500 meter(argv[1]);

    std::shared_ptr<CharDeviceCom> com =
        std::make_shared<CharDeviceCom>(argv[1]);
    meter.setCom(com);

    meter.reset();

    /* ping multimeter */
    meter.ping(0);

    /* ping scanner card */
    meter.ping(1);

    std::cout << "=======================================scan "
                 "result========================================="
              << std::endl;
    std::cout << std::setw(13) << " Ch1 [Ohm]," << std::setw(13)
              << " Ch2 [Ohm]," << std::setw(13) << " Ch3 [Ohm],"
              << std::setw(13) << " Ch4 [pF]," << std::setw(13) << " Ch5 [pF],"
              << std::setw(13) << " Ch9 [pF]," << std::setw(13) << " Ch10 [pF],"
              << std::endl;
    std::cout << std::setw(12) << meter.measureRES(1, true) << ",";
    std::cout << std::setw(12) << meter.measureRES(2, true) << ",";
    std::cout << std::setw(12) << meter.measureRES(3, true) << ",";
    std::cout << std::setw(12) << meter.measureCAP(4) * 1e12 << ",";
    std::cout << std::setw(12) << meter.measureCAP(5) * 1e12 << ",";
    std::cout << std::setw(12) << meter.measureCAP(9) * 1e12 << ",";
    std::cout << std::setw(12) << meter.measureCAP(10) * 1e12 << ",";
    std::cout << std::endl;
    std::cout << "============================================================="
                 "=============================="
              << std::endl;

    return 0;
}
