#include <ADCDevComuino.h>
#include <DataSinkConf.h>
#include <HIH4000.h>
#include <IDataSink.h>
#include <NTCSensor.h>
#include <PtSensor.h>
#include <TextSerialCom.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <sys/stat.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <thread>

//------ SETTINGS
uint32_t tsleep = 1000;
std::string configFile = "config.json";
std::string streamName = "Climate";
int rport = 0;
//---------------

bool quit = false;
void cleanup(int signum) { quit = true; }

void usage(char* argv[]) {
    std::cout << "Usage: " << argv[0]
              << " [-c config file] [-s stream name] [-p serial port] [-t "
                 "sleep time (ms)]"
              << std::endl;
    std::cout << "   defaults: -c " << configFile << " -s " << streamName
              << " -p " << rport << " -t " << tsleep << std::endl;
}
int main(int argc, char** argv) {
    // Parse command-line
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"port", required_argument, 0, 'p'},
            {"config", required_argument, 0, 'c'},
            {"stream", required_argument, 0, 's'},
            {"tsleep", required_argument, 0, 't'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "s:c:p:t:dh", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 's':
                streamName = optarg;
                break;
            case 'c':
                configFile = optarg;
                break;
            case 'p':
                rport = atoi(optarg);
                break;
            case 't':
                tsleep = atoi(optarg);
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    // open serial device
    SerialCom::BaudRate baud = SerialCom::BaudRate::Baud9600;
    std::shared_ptr<TextSerialCom> sc(
        new TextSerialCom("/dev/ttyACM" + std::to_string(rport), baud));
    sc->init();
    sc->setTermination("\r\n");
    // somehow needed to initiate communication to Arduino
    try {
        sc->send("HELP");
        std::cout << sc->receive() << std::endl;
    } catch (...) {
        // std::cerr << "ERROR testing Arduino communication" << std::endl;
    }
    // create Arduino as ADC device
    std::shared_ptr<ADCDevice> dev(new ADCDevComuino(1., sc));

    // create NTC
    std::shared_ptr<NTCSensor> ntc(
        new NTCSensor(1, dev, 298.15, 10e3, 3435.0, 18e3));
    ntc->init();
    // create Pt1000
    std::shared_ptr<PtSensor> pt(
        new PtSensor(0, dev, 1.e3, 3.9083e-3, -5.775e-7, 970.0));
    pt->init();
    // create HIH4000
    std::shared_ptr<HIH4000> hum(
        new HIH4000(2, dev, std::static_pointer_cast<ClimateSensor>(pt)));
    hum->init();

    // Create sink
    DataSinkConf ds;
    ds.setHardwareConfig(configFile);
    std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

    // Register interrupt for cleanup
    signal(SIGINT, cleanup);

    // request data from selected measurement
    while (!quit) {
        // Ambient T and H
        hum->read();
        stream->setTag("Sensor", "Ambient");
        stream->startMeasurement("environment",
                                 std::chrono::system_clock::now());
        stream->setField("temperature",
                         hum->temperature());  // actual reading from Pt1000
        stream->setField("humidity", hum->humidity());
        stream->recordPoint();
        stream->endMeasurement();
        // module T
        ntc->read();
        stream->setTag("Sensor", "Module");
        stream->startMeasurement("environment",
                                 std::chrono::system_clock::now());
        stream->setField("temperature", ntc->temperature());
        stream->setField("dewpoint", hum->dewPoint());
        stream->recordPoint();
        stream->endMeasurement();
        std::this_thread::sleep_for(std::chrono::milliseconds(tsleep));
    }

    return 0;
}
