#include <string.h>
#include <unistd.h>

#include <iomanip>
#include <iostream>
#include <memory>

#include "ComIOException.h"
#include "Logger.h"
#include "UIOCom.h"

void print_help() {
    std::cout << "usage: uio [driver options] command [command parameters]"
              << std::endl;
    std::cout << std::endl;
    std::cout << "driver options" << std::endl;
    std::cout << " -d dev: device to use (default: /dev/uio0)" << std::endl;
    std::cout << std::endl;
    std::cout << "commands" << std::endl;
    std::cout << " write address data: write data to address " << std::endl;
    std::cout << " read address: read address " << std::endl;
}

int main(int argc, char* argv[]) {
    std::string device = "/dev/uio0";

    // Parse options
    int opt;
    while ((opt = getopt(argc, argv, "d:")) != -1) {
        switch (opt) {
            case 'd':
                device = optarg;
                break;
            case '?':  // unknown option...
                print_help();
                return 1;
                break;
        }
    }

    // Determine the command
    if (argc <= optind) {
        print_help();
        return 1;
    }

    std::string command = argv[optind];

    try {
        // Create the communication object
        std::shared_ptr<UIOCom> uio = std::make_shared<UIOCom>(device, 0x10000);

        // Run the commands
        if (command == "write") {
            // Need two more
            if (argc <= (optind + 2)) {
                print_help();
                return 1;
            }

            unsigned int address = std::stoul(argv[optind + 1], nullptr, 0);
            unsigned int data = std::stoul(argv[optind + 2], nullptr, 0);

            uio->write_reg32(address, data);
        } else if (command == "read") {
            // Need one more
            if (argc <= (optind + 1)) {
                print_help();
                return 1;
            }

            unsigned int address = std::stoul(argv[optind + 1], nullptr, 0);

            unsigned int read = uio->read_reg32(address);
            logger(logINFO) << "0x" << std::hex << std::setw(8)
                            << std::setfill('0') << read;
        }
    } catch (const ComIOException& e) {
        logger(logERROR) << "ComIOException: " << e.what();
    }
    return 0;
}
