from ._labRemote.com import *

for cls in list(locals().values()):
    if not isinstance(cls, type):
        continue
    cls.__module__ = __name__
