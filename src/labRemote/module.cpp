#include <pybind11/pybind11.h>

#include "Logger.h"

namespace py = pybind11;

#include "Logger.h"

void register_com(py::module&);
void register_ps(py::module&);
void register_equipconf(py::module&);
void register_datasink(py::module&);
void register_devcom(py::module&);
void register_chiller(py::module&);
void register_meter(py::module&);

PYBIND11_MODULE(_labRemote, m) {
    py::module com = m.def_submodule("com");
    register_com(com);

    py::module ps = m.def_submodule("ps");
    register_ps(ps);

    py::module ec = m.def_submodule("ec");
    register_equipconf(ec);

    py::module datasink = m.def_submodule("datasink");
    register_datasink(datasink);

    py::module devcom = m.def_submodule("devcom");
    register_devcom(devcom);

    py::module chiller = m.def_submodule("chiller");
    register_chiller(chiller);

    py::module meter = m.def_submodule("meter");
    register_meter(meter);

    m.def("incrDebug", &logIt::incrDebug);
    m.def("setLogLevel", &logIt::setLogLevel);
    m.def("setTimestamp", &logIt::setTimestamp);

    py::enum_<loglevel_e>(m, "loglevel_e")
        .value("logERROR", logERROR)
        .value("logWARNING", logWARNING)
        .value("logINFO", logINFO)
        .value("logDEBUG", logDEBUG)
        .value("logDEBUG1", logDEBUG1)
        .value("logDEBUG2", logDEBUG2)
        .value("logDEBUG3", logDEBUG3)
        .value("logDEBUG4", logDEBUG4);
}
