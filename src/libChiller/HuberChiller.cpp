#include "HuberChiller.h"

#include "ChillerRegistry.h"
REGISTER_CHILLER(HuberChiller)

#include <cmath>

HuberChiller::HuberChiller(const std::string& name)
    : IChiller(name, {"HuberChiller"}) {}

void HuberChiller::init() {
    const std::string& termination = m_com->termination();
    m_com->setTermination("\r");
    m_identity = command("[M01V07C6");
    m_com->setTermination(termination);
}

void HuberChiller::setCom(std::shared_ptr<ICom> com) {
    std::shared_ptr<TextSerialCom> textCom =
        std::dynamic_pointer_cast<TextSerialCom>(com);
    if (!textCom) {
        throw std::runtime_error(
            "HuberChiller::setCom() only supports TextSerialCom.");
    }
    if (!textCom->is_open()) textCom->init();
    m_com = textCom;
}

std::string HuberChiller::command(const std::string& cmd) {
    std::string response = m_com->sendreceive(cmd);
    // The send commands always begin with "[M01", answers always with "[S01",
    // followed by the command qualifier "V" (Verify), "L" (Limits) or "G"
    // (General)
    if (cmd.rfind("[M01") == 0) {
        if (response.rfind("[S01") != 0) {
            logger(logDEBUG)
                << __PRETTY_FUNCTION__
                << "-> Chiller returned unexpected response to command '" << cmd
                << "': '" << response << "'";
            throw std::runtime_error("Unexpected response from chiller");
        }
    } else if (response.rfind(cmd.substr(0, 2)) != 0) {
        // expect PP commands to have slave return same prefix (SP, TI, TE, CA)
        logger(logDEBUG)
            << __PRETTY_FUNCTION__
            << "-> Chiller returned unexpected response to command '" << cmd
            << "': '" << response << "'";
        throw std::runtime_error("Unexpected response from chiller");
    }
    return response;
}

void HuberChiller::turnOn() {
    std::string response = command("CA@ 00001");
    // Expected Slave Response "CA +00001"
}

void HuberChiller::turnOff() {
    std::string response = command("CA@ 00000");
    // Expected Slave Response "CA +00000"
}

void HuberChiller::setTargetTemperature(float temp) {
    std::string cmd = "SP@ ";  // TODO
    if (temp < 0)
        cmd.append("-");
    else
        cmd.append("+");
    if (temp < 100 && temp > -100) cmd.append("0");
    if (temp < 10 && temp > -10) cmd.append("0");
    int setpt = round(abs(temp * 100));
    cmd.append(std::to_string(setpt));
    std::cout << cmd << std::endl;
    command(cmd);
    // Example Master Command: "SP@ -01234" sets to -12.34C
    // Expected Slave Response: "SP -01234"
}

float HuberChiller::getTargetTemperature() {
    std::string response = command("SP?");  // TODO
    float temp;
    if (response.substr(3, 1) == "+")
        temp = 0.01 * (std::stof(response.substr(4, 5)));
    else if (response.substr(3, 1) == "-")
        temp = -0.01 * (std::stof(response.substr(4, 5)));
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
    }
    return temp;
    // Expected Master Command: "SP?"
    // Example Slave Response: "SP +02500" means the setpoint is 25C
}

float HuberChiller::measureTemperature() {
    std::string response = command("TI?");
    float temp;
    if (response.substr(3, 1) == "+")
        temp = 0.01 * (std::stof(response.substr(4, 5)));
    else if (response.substr(3, 1) == "-")
        temp = -0.01 * (std::stof(response.substr(4, 5)));
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
    }
    return temp;
    // Expected Master Command: "TI?"
    // Example Slave Response: "TI +02499" means the internal temperature
    // is 24.99C
}

bool HuberChiller::getStatus() {
    const std::string& termination = m_com->termination();
    m_com->setTermination("\r");
    std::string response = command("[M01G0D******C0");
    m_com->setTermination(termination);

    std::string mode = response.substr(7, 1);
    if (mode == "C")
        std::cout << "The Circulation is on" << std::endl;
    else if (mode == "I")
        std::cout << "Internal Temperature Control is on" << std::endl;
    else if (mode == "O")
        std::cout << "Temperature Control is off" << std::endl;
    else {
        std::cout << "Temperature Control Mode cannot be determined"
                  << std::endl;
        logger(logDEBUG)
            << __PRETTY_FUNCTION__
            << "-> Chiller returned unexpected response to command '"
            << "[M01G0D******C0\r"
            << "': '" << response << "'";
        throw std::runtime_error("Unexpected response from chiller");
    }

    std::string alarm = response.substr(8, 1);
    if (alarm == "0")
        std::cout << "No alarm" << std::endl;
    else
        std::cout << "Alarm!" << std::endl;

    std::string setpoint = response.substr(9, 4);
    int sp;
    std::stringstream ss1;
    ss1 << std::hex << setpoint;
    ss1 >> sp;
    if (sp < 32768)
        std::cout << "The setpoint is " << 0.01 * sp << "C" << std::endl;
    else
        std::cout << "The setpoint is " << -327.68 + (0.01 * sp - 327.68) << "C"
                  << std::endl;

    std::string temp = response.substr(13, 4);
    int ti;
    std::stringstream ss2;
    ss2 << std::hex << temp;
    ss2 >> ti;
    if (ti < 32768)
        std::cout << "The internal temperature is " << 0.01 * ti << "C"
                  << std::endl;
    else
        std::cout << "The internal temperature is "
                  << -327.68 + (0.01 * ti - 327.68) << "C" << std::endl;

    return true;
    // Slave Response Format: "[S01G15sattttiiiieeeepp\r"
    // Slave Response Example: "[S01G15O0FE7009A4C504E7\r"
}

std::string HuberChiller::getFaultStatus() { return m_com->sendreceive("RF"); }
