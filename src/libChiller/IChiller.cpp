#include "IChiller.h"

#include "Logger.h"

IChiller::IChiller(const std::string& name,
                   const std::vector<std::string>& models) {
    m_name = name;
    m_models = models;
}

void IChiller::setCom(std::shared_ptr<ICom> com) {
    if (!com->is_open()) com->init();
    m_com = com;
}

void IChiller::setConfiguration(const nlohmann::json& /*config*/) {
    // default is to do nothing
}

void IChiller::setRampRate(float RR) {
    logger(logWARNING) << "setRampRate is not implemented in this chiller!";
}

float IChiller::getRampRate() {
    logger(logWARNING) << "getRampRate is not implemented in this chiller!";
    return 0.0;
}

float IChiller::ctof(float t) { return (t * 9.0 / 5.0) + 32.0; }

float IChiller::ftoc(float t) { return (t - 32.0) * 5.0 / 9.0; }
