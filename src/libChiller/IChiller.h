#ifndef CHILLER_H
#define CHILLER_H

#include <memory>
#include <string>
#include <vector>

#include "ICom.h"

//! \brief Abstract class for interfacing with a chiller
/**
 * All classes that inherit from this class have Python bindings.
 */
class IChiller {
 public:
    /** IChiller constructor
     * @param name: name of the chiller
     * @param models: List of tested models (empty means no check is performed)
     */
    IChiller(const std::string& name,
             const std::vector<std::string>& models = {});
    virtual ~IChiller() = default;

    //! Set the communication object
    /**
     *
     * The object is initialized if not already.
     *
     * \param com ICom instance for communicating with the chiller
     */
    virtual void setCom(std::shared_ptr<ICom> com);

    /** Store JSON hardware configuration
     * @param config JSON configuration for the given chiller only
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /* Return list of supported models. */
    virtual const std::vector<std::string> getListOfModels() const {
        return m_models;
    }

    //! Initialize the chiller and the interface
    virtual void init() = 0;
    //! Turn the chiller on
    virtual void turnOn() = 0;
    //! Turn the chiller off
    virtual void turnOff() = 0;
    //! Set the ramp rate (degree/minute) of the chiller
    /**
     * \param RR The target ramp rate in degree/minute
     */
    virtual void setRampRate(float RR);
    //! Return the ramp rate that the chiller is set to in degree/minute
    virtual float getRampRate();
    //! Set the target temperature of the chiller
    /**
     * \param temp The target temperature in Celsius
     */
    virtual void setTargetTemperature(float temp) = 0;
    //! Return the temperature that the chiller is set to in Celsius
    virtual float getTargetTemperature() = 0;
    //! Return the current temperature of the chiller in Celsius
    virtual float measureTemperature() = 0;
    //! Get the status of the chiller
    /*
     * \return true if chiller is on and working properly, and false otherwise
     */
    virtual bool getStatus() = 0;

 protected:
    std::shared_ptr<ICom> m_com;

    /** Store device name */
    std::string m_name;

    /** List of supported models */
    std::vector<std::string> m_models;

    //! Convert Celcius to Fahrenheit
    float ctof(float t);
    //! Convert Fahrenheit to Celcius
    float ftoc(float t);
};

#endif
