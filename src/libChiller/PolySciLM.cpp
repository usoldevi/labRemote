#include "PolySciLM.h"

#include "ChillerRegistry.h"
REGISTER_CHILLER(PolySciLM)

PolySciLM::PolySciLM(const std::string& name)
    : IChiller(name, {"PolySciLM"}), m_convert(false) {}

void PolySciLM::init() { m_convert = !getTemperatureUnit(); }

void PolySciLM::command(const std::string& cmd) {
    std::string response = m_com->sendreceive(cmd);
    if (!response.empty()) {
        if (response[0] == '!')
            return;
        else if (response[0] == '?') {
            logger(logDEBUG)
                << __PRETTY_FUNCTION__
                << "-> Error in chiller on executing command '" << cmd << "'";
            throw std::runtime_error("Error in chiller");
        }
    }
    logger(logDEBUG) << __PRETTY_FUNCTION__
                     << "-> Chiller returned unexpected response to command '"
                     << cmd << "'";
    throw std::runtime_error("Unexpected response from chiller");
}

void PolySciLM::turnOn() { command("SO1"); }

void PolySciLM::turnOff() { command("SO0"); }

void PolySciLM::setTargetTemperature(float temp) {
    std::string cmd = "SS";
    if (m_convert) temp = ctof(temp);
    cmd.append(std::to_string(temp));
    command(cmd);
}

float PolySciLM::getTargetTemperature() {
    std::string response = m_com->sendreceive("RS");
    float temp;
    if (response.substr(0, 1) == "+")
        temp = std::stof(response.substr(1));
    else if (response.substr(0, 1) == "-")
        temp = -std::stof(response.substr(1));
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
    }
    if (m_convert) temp = ftoc(temp);
    return temp;
}

float PolySciLM::measureTemperature() {
    std::string response = m_com->sendreceive("RT");
    float temp;
    if (response.substr(0, 1) == "+")
        temp = std::stof(response.substr(1));
    else if (response.substr(0, 1) == "-")
        temp = -std::stof(response.substr(1));
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
    }
    if (m_convert) temp = ftoc(temp);
    return temp;
}

float PolySciLM::measurePressure() {
    std::string response = m_com->sendreceive("RP");
    float press;
    if (response.substr(0, 1) == "+")
        press = std::stof(response.substr(1));
    else if (response.substr(0, 1) == "-")
        press = -std::stof(response.substr(1));
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
    }
    if (m_convert) press = ftoc(press);
    return press;
}

float PolySciLM::measureFlow() {
    std::string response = m_com->sendreceive("RL");
    float flow;
    if (response.substr(0, 1) == "+")
        flow = std::stof(response.substr(1));
    else if (response.substr(0, 1) == "-")
        flow = -std::stof(response.substr(1));
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
    }
    if (m_convert) flow = ftoc(flow);
    return flow;
}

bool PolySciLM::getStatus() {
    std::string response = m_com->sendreceive("RW");
    if (response == "1")
        return true;
    else if (response == "0")
        return false;
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
        return false;
    }
}

std::string PolySciLM::getFaultStatus() { return m_com->sendreceive("RF"); }

bool PolySciLM::getTemperatureUnit() {
    std::string response = m_com->sendreceive("RU");
    if (response == "C")
        return true;
    else if (response == "F")
        return false;
    else {
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> Unexpected response: " << response;
        throw std::runtime_error("Unexpected response from chiller");
    }
}
