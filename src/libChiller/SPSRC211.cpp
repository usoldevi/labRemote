#include "SPSRC211.h"

#include "ChillerRegistry.h"
REGISTER_CHILLER(SPSRC211)

#include <cmath>
#include <iomanip>

#include "Logger.h"

SPSRC211::SPSRC211(const std::string& name) : IChiller(name, {"SPSRC211"}) {}

void SPSRC211::init() {
    command("POLL");       // verify establishment of communication
    command("DEGREES=0");  // set temperature unit to be Celsius

    int faultcode = getFaultStatus();
    if (faultcode != 0) {
        turnOff();
        logger(logERROR) << __PRETTY_FUNCTION__
                         << " -> Chiller in alarm status, alarm code: "
                         << faultcode;
        throw std::runtime_error("Chiller in alarm status");
    }
    logger(logDEBUG) << "Chiller is currently " << (getStatus() ? "ON" : "OFF");
}

std::string SPSRC211::command(const std::string& cmd) {
    // There are two types of commands which will give two different types of
    // response

    // 1. query type (e.g.: "PTLOC?"): it will return something like
    // 'OK\rF044=+0028.42!\r' (note the two terminations here)

    // 2. non-query type (e.g.: "START"): it will return something like 'OK!\r'

    // Keep reading the buffer until a '!' is seen

    m_com->send(cmd);
    std::string response;
    do {
        response = m_com->receive();
        logger(logDEBUG1) << "command: " << cmd << ", response: " << response;
    } while (response.find("!") == std::string::npos);

    // ignore E041: System already stopped has received a "STOP" command.
    // ignore E042: System already started has received a "START" command.

    if (response[0] == 'E' && response.find("E041") == std::string::npos &&
        response.find("E042") == std::string::npos) {
        logger(logERROR) << __PRETTY_FUNCTION__
                         << " -> Received an error message from chiller: "
                         << response;
        throw std::runtime_error("Received an error message from chiller");
    }
    return response;
}

float SPSRC211::parseString(const std::string& output) {
    // example output: F044=+0021.78!
    // need to return +21.78
    if (output.length() != 14 || output[4] != '=' || output[13] != '!') {
        logger(logERROR) << __PRETTY_FUNCTION__
                         << " -> Received an invalid output from chiller: "
                         << output;
        throw std::runtime_error("Received an invalid output from chiller");
    }
    return std::stof(output.substr(5, 8));
}

void SPSRC211::turnOn() {
    command("START");
    command("PUMPSW=-1");
    command("REFRSW=-1");
    logger(logDEBUG) << "Chiller has been turned "
                     << (getStatus() ? "ON" : "OFF");
}

void SPSRC211::turnOff() {
    command("PUMPSW=0");
    command("REFRSW=0");
    command("STOP");
    logger(logDEBUG) << "Chiller has been turned "
                     << (getStatus() ? "ON" : "OFF");
}

void SPSRC211::setRampRate(float RR) {
    std::stringstream sRR;
    sRR << std::fixed << std::setprecision(1) << fabs(RR);
    std::string cmd = "RR=" + sRR.str();
    command(cmd);
}

float SPSRC211::getRampRate() {
    std::string response = command("RR?");
    return parseString(response);
}

void SPSRC211::setTargetTemperature(float temp) {
    std::string cmd = "SP=";
    if (temp < 0)
        cmd.append("-");
    else
        cmd.append("+");
    cmd.append(std::to_string(abs(temp)));
    command(cmd);
}

float SPSRC211::getTargetTemperature() {
    std::string response = command("SP?");
    return parseString(response);
}

float SPSRC211::measureTemperature() {
    std::string response = command("PTLOC?");
    return parseString(response);
}

bool SPSRC211::getStatus() {
    int pump = int(parseString(command("PUMPSW?")));
    int refr = int(parseString(command("REFRSW?")));
    if (pump == -1 && refr == -1)
        return true;
    else
        return false;
}

int SPSRC211::getFaultStatus() { return int(parseString(command("ALMCODE?"))); }
