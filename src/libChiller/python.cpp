#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "nlohmann/json.hpp"
#include "pybind11_json/pybind11_json.hpp"
namespace py = pybind11;

#include "HuberChiller.h"
#include "IChiller.h"
#include "PolySciLM.h"

class PyIChiller : public IChiller {
 public:
    using IChiller::IChiller;

    void setConfiguration(const nlohmann::json& config) override {
        PYBIND11_OVERLOAD_PURE(void, IChiller, setConfiguration, config);
    }

    void init() override { PYBIND11_OVERLOAD_PURE(void, IChiller, init, ); }

    void turnOn() override { PYBIND11_OVERLOAD_PURE(void, IChiller, turnOn, ); }

    void turnOff() override {
        PYBIND11_OVERLOAD_PURE(void, IChiller, turnOff, );
    }

    void setTargetTemperature(float temp) override {
        PYBIND11_OVERLOAD_PURE(void, IChiller, setTargetTemperature, temp);
    }

    float getTargetTemperature() override {
        PYBIND11_OVERLOAD_PURE(float, IChiller, getTargetTemperature, );
    }

    float measureTemperature() override {
        PYBIND11_OVERLOAD_PURE(float, IChiller, measureTemperature, );
    }

    bool getStatus() override {
        PYBIND11_OVERLOAD_PURE(bool, IChiller, getStatus, );
    }
};

void register_chiller(py::module& m) {
    py::class_<IChiller, PyIChiller, std::shared_ptr<IChiller>>(m, "IChiller")
        .def(py::init<const std::string&, std::vector<std::string>>())
        .def("setConfiguration", &IChiller::setConfiguration)
        .def("init", &IChiller::init)
        .def("setCom", &IChiller::setCom)
        .def("turnOn", &IChiller::turnOn)
        .def("turnOff", &IChiller::turnOff)
        .def("setTargetTemperature", &IChiller::setTargetTemperature)
        .def("getTargetTemperature", &IChiller::getTargetTemperature)
        .def("measureTemperature", &IChiller::measureTemperature)
        .def("getStatus", &IChiller::getStatus);

    py::class_<HuberChiller, IChiller, std::shared_ptr<HuberChiller>>(
        m, "HuberChiller")
        .def(py::init<const std::string&>())
        .def("init", &HuberChiller::init)
        .def("setCom", &HuberChiller::setCom)
        .def("turnOn", &HuberChiller::turnOn)
        .def("turnOff", &HuberChiller::turnOff)
        .def("setTargetTemperature", &HuberChiller::setTargetTemperature)
        .def("getTargetTemperature", &HuberChiller::getTargetTemperature)
        .def("measureTemperature", &HuberChiller::measureTemperature)
        .def("getStatus", &HuberChiller::getStatus)
        .def("getFaultStatus", &HuberChiller::getFaultStatus);

    py::class_<PolySciLM, IChiller, std::shared_ptr<PolySciLM>>(m, "PolySciLM")
        .def(py::init<const std::string&>())
        .def("init", &PolySciLM::init)
        .def("turnOn", &PolySciLM::turnOn)
        .def("turnOff", &PolySciLM::turnOff)
        .def("setTargetTemperature", &PolySciLM::setTargetTemperature)
        .def("getTargetTemperature", &PolySciLM::getTargetTemperature)
        .def("measureTemperature", &PolySciLM::measureTemperature)
        .def("measurePressure", &PolySciLM::measurePressure)
        .def("measureFlow", &PolySciLM::measureFlow)
        .def("getStatus", &PolySciLM::getStatus)
        .def("getFaultStatus", &PolySciLM::getFaultStatus);
}
