#ifndef CHARDEVICECOM_H
#define CHARDEVICECOM_H

#include <termios.h>

#include <string>
#include <unordered_map>

#include "ICom.h"

/**
 * Implementation of communication via a character device.
 */
class CharDeviceCom : public ICom {
 public:
    /** \brief Create communication object and set settings
     * @param port Device corresponding to the character device
     */
    CharDeviceCom(const std::string& port);
    CharDeviceCom();

    ~CharDeviceCom();

    /** Open device and configure
     */
    virtual void init();

    /** \brief Configure on JSON object
     *
     * Valid keys:
     *  - `device`: Path to character device
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     * \param length Number of characters in `buf` that should be sent
     */
    virtual void send(char* buf, size_t length);

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     */
    virtual void send(const std::string& buf);

    /** Read data from device
     *
     * Throw `std::runtime_error` on error.
     *
     * \return Received data
     */
    virtual std::string receive();

    /** Read data from device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Buffer where to store results
     * \param length Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual uint32_t receive(char* buf, size_t length);

    /** Write data to device and receive reply
     *
     * Throw `std::runtime_error` on error.
     *
     * \param cmd Data to be sent
     *
     * \return Returned data
     */
    virtual std::string sendreceive(const std::string& cmd);

    /** Write data to device and receive reply
     *
     * Throw `std::runtime_error` on error.
     *
     * \param wbuf Data to be sent
     * \param wlength Number of characters in `wbuf` that should be sent
     * \param rbuf Buffer where to store results
     * \param rlength Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual void sendreceive(char* wbuf, size_t wlength, char* rbuf,
                             size_t rlength);

    /** Request exlusive access to device.
     *
     * If a single hardware bus is used to connect multiple devices,
     * the access to all of them should be locked to remove changes
     * of cross-talk.
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void lock();

    /** Release exlusive access to device.
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void unlock();

 private:
    /** \brief Configure device for serial communication
     */
    void config();

    /** Configuration @{ */

    /// Name of device
    std::string m_port;

    /** @} */

    /** Run-time variables @{ */

    //! File handle for communication
    int m_dev = 0;

    //! Count number of lock() calls on this device
    uint32_t m_lock_counter = 0;

    /** @} */

    //! Maximum number of characters to read using string buffers
    static const unsigned MAX_READ = 4096;

    //! Temporary buffer for ::read()
    char m_tmpbuf[MAX_READ];
};

#endif
