#ifndef GPIBSERIALCOM_H
#define GPIBSERIALCOM_H

#include "TextSerialCom.h"

/**
 * Implementation of block serial communication with POSIX
 * read/write calls that uses ASCII strings for data
 * transmission in the presence of a GPIB bus.
 *
 * All data packets are deliminiated with "\n\r". This suffix
 * is stripped internally.
 *
 * The [Prologix GPIB-USB
 * controller](http://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf) is
 * assumed.
 */
class GPIBSerialCom : public TextSerialCom {
 public:
    /** \brief Create serial communication object and set settings
     * @param gpib_addr Address of device on GPIB bus
     * @param port Device corresponding to the serial port
     * @param baud Baud rate to use
     * @param useParityBit Use parity bit
     * @param twoStopBits Use two stop bits instead of one
     * @param flowControl Enable hardware flow control
     * @param charsize Size of a character
     */
    GPIBSerialCom(
        uint16_t gpib_addr, const std::string& port,
        SerialCom::BaudRate baud = SerialCom::BaudRate::Baud19200,
        bool parityBit = false, bool twoStopBits = false,
        bool flowControl = false,
        SerialCom::CharSize charsize = SerialCom::CharSize::CharSize8);
    GPIBSerialCom();

    ~GPIBSerialCom();

    /** Open device and configure
     */
    virtual void init();

    /** \brief Configure serial device based on JSON object
     *
     * Valid keys:
     *  - `gpib_addr`: Address of device on GPIB bus
     *  - `read_tmo_ms`: Timeout (ms) to wait for new data, 0 means don't set
     * (default: none)
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** Send data to device
     *
     * Sets the GPIB address before sending data.
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     * \param length Number of characters in `buf` that should be sent
     */
    virtual void send(char* buf, size_t length);

    /** Send data to device
     *
     * Sets the GPIB address before sending data.
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     */
    virtual void send(const std::string& buf);

    /** Read data from device
     *
     * Requests the Prologix box to send the currently queued
     * data. The address is not set.
     *
     * Throw `std::runtime_error` on error.
     *
     * \return Received data
     */
    virtual std::string receive();

    /** Read data from device
     *
     * Requests the Prologix box to send the currently queued
     * data. The address is not set.
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Buffer where to store results
     * \param length Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual uint32_t receive(char* buf, size_t length);

 private:
    //! Address of device on GPIB bus
    uint16_t m_gpib_addr = 0;

    //! Timeout for waiting for new data on receive
    uint16_t m_read_tmo_ms = 0;
};

#endif
