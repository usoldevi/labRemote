#ifndef SERIALCOM_H
#define SERIALCOM_H

#include <termios.h>

#include <string>
#include <unordered_map>

#include "ICom.h"

/**
 * Implementation of block serial communication with POSIX
 * receive/send calls.
 *
 * # Timeout
 * A timeout is implemented to prevent reading from a serial device
 * getting stuck when no data is available. The timeout is defined
 * as the amount of time to wait between bytes being received.
 *
 * Reading zero bytes is valid and does not return an error.
 *
 * On Linux, this is done using the VMIN/VTIME parameters. This means
 * that the timeout setting is rounded to the nearest tenth of a second.
 */
class SerialCom : public ICom {
 public:
    /** Available baud rates */
    enum class BaudRate : speed_t {
        Baud0 = B0,
        Baud50 = B50,
        Baud75 = B75,
        Baud110 = B110,
        Baud134 = B134,
        Baud150 = B150,
        Baud200 = B200,
        Baud300 = B300,
        Baud600 = B600,
        Baud1200 = B1200,
        Baud1800 = B1800,
        Baud2400 = B2400,
        Baud4800 = B4800,
        Baud9600 = B9600,
        Baud19200 = B19200,
        Baud38400 = B38400,
        Baud57600 = B57600,
        Baud115200 = B115200,
        Baud230400 = B230400,
        Baud460800 = B460800,
        Baud500000 = B500000,
        Baud576000 = B576000,
        Baud921600 = B921600,
        Baud1000000 = B1000000,
        Baud1152000 = B1152000,
        Baud1500000 = B1500000,
        Baud2000000 = B2000000,
        Baud2500000 = B2500000,
        Baud3000000 = B3000000,
        Baud3500000 = B3500000,
        Baud4000000 = B4000000
    };

    /** Available character sizes */
    enum class CharSize : tcflag_t {
        CharSize5 = CS5,
        CharSize6 = CS6,
        CharSize7 = CS7,
        CharSize8 = CS8
    };

 public:
    /** \brief Create serial communication object and set settings
     * @param port Device corresponding to the serial port
     * @param baud Baud rate to use
     * @param useParityBit Use parity bit
     * @param twoStopBits Use two stop bits instead of one
     * @param flowControl Enable hardware flow control
     * @param charsize Size of a character
     */
    SerialCom(const std::string& port, BaudRate baud = BaudRate::Baud19200,
              bool parityBit = false, bool twoStopBits = false,
              bool flowControl = false,
              CharSize charsize = CharSize::CharSize8);
    SerialCom();

    ~SerialCom();

    /** Open device and configure
     */
    virtual void init();

    /** \brief Configure serial device based on JSON object
     *
     * Valid keys:
     *  - `device`: Path to serial device
     *  - `baudrate`: Baud rate (default: `"B19200"`)
     *  - `parityBit`: Enable parity bit (default: `false`)
     *  - `twoStopBits`: Use two stop bits instead of one (default: `false`)
     *  - `flowControl`: Enable hardware flow control (default: `false`)
     *  - `charsize`: Size of a character (default: `"C8"`)
     *  - `timeout`: Timeout in seconds (default: `1` second)
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** \brief Set timeout
     *
     * If serial port is open, the timeout is updated.
     *
     * \param timeout Timeout in seconds
     */
    void setTimeout(float timeout);

    /** \brief Retrieve current timeout
     *
     * \return Timeout in seconds
     */
    float getTimeout();

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     * \param length Number of characters in `buf` that should be sent
     */
    virtual void send(char* buf, size_t length);

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     */
    virtual void send(const std::string& buf);

    /** Read data from device
     *
     * Returns any characters available in the buffer, waiting up
     * to `timeout` if no data is available.
     *
     * Throw `std::runtime_error` on error.
     *
     * \return Received data
     */
    virtual std::string receive();

    /** Read data from device
     *
     * Returns any characters available in the buffer, waiting up
     * to `timeout` if no data is available.
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Buffer where to store results
     * \param length Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual uint32_t receive(char* buf, size_t length);

    /** Write data to device and receive reply
     *
     * Throw `std::runtime_error` on error.
     *
     * \param cmd Data to be sent
     *
     * \return Returned data
     */
    virtual std::string sendreceive(const std::string& cmd);

    /** Write data to device and receive reply
     *
     * Throw `std::runtime_error` on error.
     *
     * \param wbuf Data to be sent
     * \param wlength Number of characters in `wbuf` that should be sent
     * \param rbuf Buffer where to store results
     * \param rlength Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual void sendreceive(char* wbuf, size_t wlength, char* rbuf,
                             size_t rlength);

    /** Flush any data in the device buffers
     */
    void flush();

    /** Toggle the Data Terminal Ready pin
     *
     * \param on New DTR value
     */
    void setDTR(bool on);

    /** Request exlusive access to device.
     *
     * If a single hardware bus is used to connect multiple devices,
     * the access to all of them should be locked to remove changes
     * of cross-talk.
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void lock();

    /** Release exlusive access to device.
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void unlock();

 private:
    /** \brief Configure device for serial communication
     */
    void config();

    /** Configuration @{ */

    /// Name of device
    std::string m_port;

    /// Baud rate to use
    BaudRate m_baudrate = BaudRate::Baud19200;

    /// Use parity bit
    bool m_parityBit = false;

    // Use two stop bits intead of one
    bool m_twoStopBits = false;

    // Enable hardware flow control
    bool m_flowControl = false;

    // Size of a character
    CharSize m_charsize = CharSize::CharSize8;

    // Timeout
    float m_timeout = 1;

    /** @} */

    /** Run-time variables @{ */

    //! File handle for communication
    int m_dev = 0;

    //! Current settings
    struct termios m_tty;

    //! Settings frm before config
    struct termios m_tty_old;

    //! Count number of lock() calls on this device
    uint32_t m_lock_counter = 0;

    /** @} */

    //! Maximum number of characters to read using string buffers
    static const unsigned MAX_READ = 4096;

    //! Temporary buffer for ::read()
    char m_tmpbuf[MAX_READ];

    //! Maps valid baud rate settings to `BaudRate` type
    static const std::unordered_map<std::string, SerialCom::BaudRate>
        mapBAUDRATE;

    //! Maps valid char size settings to `CharSize` type
    static const std::unordered_map<std::string, SerialCom::CharSize>
        mapCHARSIZE;
};

#endif
