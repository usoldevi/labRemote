#ifndef TEXTSERIALCOM_H
#define TEXTSERIALCOM_H

#include "SerialCom.h"

/**
 * Implementation of block serial communication with POSIX
 * receive/send calls that uses ASCII strings for data
 * transmission.
 *
 * All data packets are deliminiated with a configurable termination
 * string that is stripped internally. The termination is configurable
 * via the `termination` JSON paramater. By default, it is a single `\n`.
 *
 * In order to handle "asymmetric" termination in which the termination
 * characters for messages being sent and received are different, there
 * is the `returnTermination` JSON parameter which specifies what
 * termination character to expect when receiving data packets from a device.
 * By default it is `\n` and if it is not specified in the JSON configuration
 * for the TextSerialCom instance, it will be set to match the `termination`
 * JSON parameter.
 */
class TextSerialCom : public SerialCom {
 public:
    /** \brief Create serial communication object and set settings
     * @param port Device corresponding to the serial port
     * @param baud Baud rate to use
     * @param useParityBit Use parity bit
     * @param twoStopBits Use two stop bits instead of one
     * @param flowControl Enable hardware flow control
     * @param charsize Size of a character
     */
    TextSerialCom(
        const std::string& port,
        SerialCom::BaudRate baud = SerialCom::BaudRate::Baud19200,
        bool parityBit = false, bool twoStopBits = false,
        bool flowControl = false,
        SerialCom::CharSize charsize = SerialCom::CharSize::CharSize8);
    TextSerialCom();

    ~TextSerialCom() = default;

    /** \brief Set termination string
     *
     * If `returnTermination` is not provided, then the termination string
     * used for received data packets will be set to `termination`.
     *
     * \param termination New termination string
     * \param returnTermination New return termination string
     */
    void setTermination(const std::string& termination,
                        const std::string& returnTermination = "");

    /**
     * \return Current termination string
     */
    std::string termination() const;

    /**
     * \return Current returnTermination string
     */
    std::string returnTermination() const;

    /** \brief Configure serial device based on JSON object
     *
     * Valid keys:
     *  - `termination`: terminating string for data packets
     *  - `returnTermination`: termination string for received data packets
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     * \param length Number of characters in `buf` that should be sent
     */
    virtual void send(char* buf, size_t length);

    /** Send data to device
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Data to be sent
     */
    virtual void send(const std::string& buf);

    /** Read data from device
     *
     * The serial port is read until the termination is detected.
     *
     * Throw `std::runtime_error` on error.
     *
     * \return Received data
     */
    virtual std::string receive();

    /** Read data from device
     *
     * The serial port is read until the returnTermination is detected.
     *
     * Throw `std::runtime_error` on error.
     *
     * \param buf Buffer where to store results
     * \param length Number of characters to receive.
     *
     * \return Number of characters received
     */
    virtual uint32_t receive(char* buf, size_t length);

 private:
    //! Termination strings for data packets
    std::string m_termination = "\n";
    std::string m_returnTermination = "\n";
};

#endif
