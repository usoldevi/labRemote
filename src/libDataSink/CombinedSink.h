#ifndef COMBINEDSINK_H
#define COMBINEDSINK_H

#include <vector>

#include "IDataSink.h"

//! \brief Data sink that handles saving data to multiple sinks.
/**
 * All requests are directly forwarded to all sub-sinks registered with the
 * `addSink` command.
 *
 * The data streams used by DataSinkConf::getDataStream are instances of
 * CombinedSink. See the DataSinkConf documentation on how to define data
 * streams in JSON files.
 */
class CombinedSink : public IDataSink {
 public:
    CombinedSink(const std::string& name);

    //! \brief Configure sink based on JSON object
    /**
     * Valid keys:
     *  - no configuration
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    //! \brief Does nothing
    /**
     * Sub-sinks should be initialized during their creation
     */
    virtual void init();

    //! \brief Add a sub-sink
    /**
     * Adds a sub-sink to the list of sinks for forwarding of all
     * commands.
     *
     * \param sink Sub-sink to add.
     */
    void addSink(std::shared_ptr<IDataSink> sink);

    virtual void setTag(const std::string& name, const std::string& value);
    virtual void setTag(const std::string& name, int8_t value);
    virtual void setTag(const std::string& name, int32_t value);
    virtual void setTag(const std::string& name, int64_t value);
    virtual void setTag(const std::string& name, double value);

    virtual void startMeasurement(
        const std::string& measurement,
        std::chrono::time_point<std::chrono::system_clock> time);
    virtual void setField(const std::string& name, const std::string& value);
    virtual void setField(const std::string& name, int8_t value);
    virtual void setField(const std::string& name, int32_t value);
    virtual void setField(const std::string& name, int64_t value);
    virtual void setField(const std::string& name, double value);

    virtual void recordPoint();
    virtual void endMeasurement();

 private:
    std::vector<std::shared_ptr<IDataSink>> m_sinks;
};

#endif  // COMBINEDSINK_H
