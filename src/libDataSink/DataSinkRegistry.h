#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "ClassRegistry.h"
#include "IDataSink.h"

// Configuration helpers and registry for specific sink implementations
namespace EquipRegistry {
/** Register new data sink type into registry */
bool registerDataSink(
    const std::string& model,
    std::function<std::shared_ptr<IDataSink>(const std::string&)> f);

/** Get new instance of given sink type */
std::shared_ptr<IDataSink> createDataSink(const std::string& model,
                                          const std::string& name);

/** List available sink types */
std::vector<std::string> listDataSink();
}  // namespace EquipRegistry

#define REGISTER_DATASINK(model)                                               \
    static bool _registered_##model = EquipRegistry::registerDataSink(         \
        #model, std::function<std::shared_ptr<IDataSink>(const std::string&)>( \
                    [](const std::string& name) {                              \
                        return std::shared_ptr<IDataSink>(new model(name));    \
                    }));
