#ifndef IDATASINK_H
#define IDATASINK_H

#include <chrono>
#include <nlohmann/json.hpp>
#include <set>
#include <string>

/**
 * A generic data sink for saving tabular data.
 *
 * A series of data points, grouped by time into measurements,
 * can be streamed into a data sink.
 *
 * All classes that inherit from this class have Python bindings
 *
 * # Data Points and Measurements
 * ## Data Points
 * A data point is a collection of basic types (string, double, int)
 * corresponding to different variables. It can be thought of as a row.
 *
 * The end of a data point is indicated using the `recordPoint`.
 *
 * ## Measurement
 *
 * A measurement is a collection of data points grouped by a timestamp. All data
 * points in a measurement must have the same set of variables. The timestamp
 * shared by all data points in a given measurement is defined at the beginning
 * of the said measurement. It is up to the implementation on how to deal with
 * the timestamp.
 *
 * A set of measurements can be thought of a large table with the timestamp as a
 * column. The rows with the same time value belong to the same measurement.
 *
 * The start of a new measurement is indicated using `startMeasurement` and the
 * end with `endMeasurement`. At the start, the name of the measurement and the
 * corresponding time needs to be specified.
 *
 * Specific sink implementations can require that measurements sharing the same
 * name use the same set of variables. It is up to the implementation to enforce
 * such a requirement.
 *
 * # Variables
 *
 * There are two types of variables and are treated differently; fields and tag.
 *
 * *Tags* correspond to meta-data and are constant for a measurement. They also
 * remain set for the reminded of the data-taking, although values can be
 * changed at any time (outside of a measurement).
 *
 * *Fields* correspond to data and are only valid for the duration of a
 * measurement. Once set, a field exists until the end of a measurement. A field
 * is not removed by `recordPoint`. Thus data that is constant during
 * a measurement only needs to be set once.
 *
 * The following variable names are reserved and cannot be be used: `time`.
 *
 * Implemetations can use the `checkReserved` function to check whether a
 * variable name is reserved.
 *
 * # Saving Data
 *
 * It is up to a specific sink implementation to decide when to save data. For
 * example, the `ConsoleSink` prints out data as soon as a data point completes.
 * But the `JSONSink`, due to the more complex structure, writes data to disk
 * once a measurement ends.
 *
 * The `recordPoint` and `endMeasurement` are used to indicate to the
 * implementation at which point a data point or measurement ends. However there
 * is no requirement in which function the actual data saving needs to happen.
 *
 * # Example
 *
 * A typical loop could look like this.
 *
 * ```
 * std::shared_ptr<IDataSink> datasink=std::make_shared<SpecificSink>("name");
 * while(forever)
 *  {
 *    // Run IV scan
 *    datasink->setTag("module", "myFirstModule"); // present for all data
 * points in ivscan datasink->startMeasurement("ivscan",
 * std::chrono::system_clock::now()); datasink->setField("Ilimit",
 * ps->getCurrentProtect()); // present for all data points in ivscan for(double
 * V=0; V<1000; V+=100)
 *      {
 *        ps->setVoltageLevel(V);
 *        datasink->setField("I", ps->measureCurrent());
 *        datasink->setField("V", ps->measureVoltage());
 *        datasink->recordPoint();
 *      }
 *    datasink->endMeasurement();
 *
 *    // Monitor climate for the next minute
 *    for(every second for minute)
 *      {
 *        // Record a data point
 *        datasink->startMeasurement("climate",
 * std::chrono::system_clock::now()); datasink->setField("temperature",
 * measureTemperature()); datasink->setField("humidity", measureHumidity());
 *        datasink->recordPoint();
 *        datasink->endMeasurement();
 *      }
 *  }
 * ```
 */
class IDataSink {
 public:
    /**
     * Create a sink with name.
     *
     * \param name Name of sink for identification
     */
    IDataSink(const std::string& name);

    /** \brief Set communication settings from JSON object
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config) = 0;

    /** \brief Initialize data sink
     *
     * Should be called after all settings are set and before any
     * data is saved.
     */
    virtual void init() = 0;

    /** \name Tags
     * @{
     */

    /**
     * Set tag of type string.
     *
     * \param name Tag name
     * \param value Tag value
     */
    virtual void setTag(const std::string& name, const std::string& value) = 0;

    /**
     * Set tag of type int8_t
     *
     * \param name Tag name
     * \param value Tag value
     */
    virtual void setTag(const std::string& name, int8_t value) = 0;

    /**
     * Set tag of type int32_t
     *
     * \param name Tag name
     * \param value Tag value
     */
    virtual void setTag(const std::string& name, int32_t value) = 0;

    /**
     * Set tag of type int64_t
     *
     * \param name Tag name
     * \param value Tag value
     */
    virtual void setTag(const std::string& name, int64_t value) = 0;

    /**
     * Set tag of type double
     *
     * \param name Tag name
     * \param value Tag value
     */
    virtual void setTag(const std::string& name, double value) = 0;

    /** @} */

    /** \name Measurement
     * @{
     */

    /**
     * Indicates the start of a measurement.
     *
     * All field variables should be reset at this point.
     *
     * \param measurement Name of the measurement
     * \param time Timestamp corresponding to the data in the measurement.
     */
    virtual void startMeasurement(
        const std::string& measurement,
        std::chrono::time_point<std::chrono::system_clock> time) = 0;

    /**
     * Set field of type string
     *
     * \param name Field name
     * \param value Field value
     */
    virtual void setField(const std::string& name,
                          const std::string& value) = 0;

    /**
     * Set field of type int8_t
     *
     * \param name Field name
     * \param value Field value
     */
    virtual void setField(const std::string& name, int8_t value) = 0;

    /**
     * Set field of type int32_t
     *
     * \param name Field name
     * \param value Field value
     */
    virtual void setField(const std::string& name, int32_t value) = 0;

    /**
     * Set field of type int64_t
     *
     * \param name Field name
     * \param value Field value
     */
    virtual void setField(const std::string& name, int64_t value) = 0;

    /**
     * Set field of type double
     *
     * \param name Field name
     * \param value Field value
     */
    virtual void setField(const std::string& name, double value) = 0;

    /**
     * Indicates the end of a data point.
     */
    virtual void recordPoint() = 0;

    /**
     * Indicates the end of a measurement.
     */
    virtual void endMeasurement() = 0;

    /** @} */

 protected:
    /**
     * Check if a variable name is reserved.
     *
     * \param name Variable name
     *
     * \return True if reserved, false otherwise.
     */
    bool checkReserved(const std::string& name);

 private:
    std::string m_name;

    //! Reserved variable names
    std::set<std::string> m_reserved = {"time"};
};

#endif  // IDATASINK_H
