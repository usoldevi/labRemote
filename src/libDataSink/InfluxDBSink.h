#ifndef INFLUXDBSINK_H
#define INFLUXDBSINK_H

#include <unordered_map>

#include "IDataSink.h"

// Forward declarte InfluxDB handle
namespace influxdb_cpp {
struct server_info;
};

//! \brief Data sink that prints to the console
/**
 * Allows communication between labRemote and InfluxDB based on "influxdb.h", a
 * third-party InfluxDB library for C++
 * (https://github.com/orca-zhang/influxdb-cpp).
 *
 * Uploads data points to an existing database in InfluxDB. A new measurement
 * (data table) will be created if the specified one doesn't exist in the
 * database.
 *
 * The connectivity parameters (host, port and database name) need to be
 * specified in the configuration file.
 */
class InfluxDBSink : public IDataSink {
 public:
    //! \brief Initializes the InfluxDB sink
    /**
     * Initializes an influxDB sink. The sink is identified by "name", and it
     * can be used to upload data to different measurements inside the same
     * database.
     */
    InfluxDBSink(const std::string& name);

    //! \brief Reads the DB configuration from a JSON object.
    /**
     * See an example in `src/configs/input-hw.json`.
     *
     * Valid keys:
     *  - `host`:     IP address of InfluxDB (hostname not supported)
     *  - `port`:     Port to connect to
     *  - `database`: Name of database to use
     *  - `username`: [optional] Username to connect to the server
     *  - `password`: [optional] Password to connect to the server
     *  - `precision`: [optional] Precision of double values, default is 5
     *  - `timeout`: [optional] Timeout (s) for HTTP requests, default is 0
     *
     * * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    //! \brief Initialize connection to the database
    /**
     * Initializes connection to the InfluxDB server and checks whether
     * the specified database exists.
     */
    virtual void init();

    //! \brief Starts a measurement
    /**
     * If the measurement (data table) doesn't exist, it will be created.
     */
    virtual void startMeasurement(
        const std::string& measurement,
        std::chrono::time_point<std::chrono::system_clock> time);

    virtual void setTag(const std::string& name, const std::string& value);
    virtual void setTag(const std::string& name, double value);
    virtual void setTag(const std::string& name, int8_t value);
    virtual void setTag(const std::string& name, int32_t value);
    virtual void setTag(const std::string& name, int64_t value);

    virtual void setField(const std::string& name, const std::string& value);
    virtual void setField(const std::string& name, int32_t value);
    virtual void setField(const std::string& name, double value);
    virtual void setField(const std::string& name, int8_t value);
    virtual void setField(const std::string& name, int64_t value);

    //! \brief Changes the precision of `double` values
    /**
     * Changes the precision of `double` values
     *
     * \param precision Number of digits past the decimal point.
     */
    void setPrecision(int precision);

    //! \brief Uploads a single row of data to influxDB
    /**
     * The timestamp will be uploaded with milliseconds precision.
     */
    virtual void recordPoint();

    //! \brief Cleans up the local variables
    virtual void endMeasurement();

 private:
    // Error handling
    int m_retval;
    std::string m_resp;

    //! \brief Checks for errors coming from InfluxDB
    /**
     * Checks for errors coming from InfluxDB
     * \param retval the return value to be checked
     * \param resp the response from InfluxDB
     */
    void errorCheck(int retval, std::string& resp);

    // the influxdb_cpp object to be filled with data
    std::shared_ptr<influxdb_cpp::server_info> m_si;

    // measurement configuration
    std::string m_host;
    int m_port;
    std::string m_dbName;
    std::string m_usr = "";
    std::string m_pwd = "";
    std::string m_measName;

    // Precision of "double" values in fields
    int m_prec = 5;

    // Timeout (seconds) for HTTP requests
    unsigned int m_timeout = 0;

    unsigned long long m_timestamp;

    // Fields and Tags
    std::unordered_map<std::string, std::string> m_fieldsString;
    std::unordered_map<std::string, int64_t> m_fieldsInt;
    std::unordered_map<std::string, double> m_fieldsDouble;

    std::unordered_map<std::string, std::string> m_tagsString;

    //! \brief Sends any command to InflxDB and gets its answer
    /**
     * Sends any command to be run in InfluxDB's console.
     * \param query the command
     * \param resp the answer
     */
    void query(std::string& resp, const std::string& query);
};

#endif  // INFLUXDBSINK_H
