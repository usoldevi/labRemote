#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "CSVSink.h"
#include "CombinedSink.h"
#include "ConsoleSink.h"
#include "DataSinkRegistry.h"
#include "IDataSink.h"
#include "InfluxDBSink.h"
#include "nlohmann/json.hpp"
#include "pybind11/chrono.h"
#include "pybind11_json/pybind11_json.hpp"

namespace py = pybind11;
namespace nl = nlohmann;

class PyIDataSink : public IDataSink {
 public:
    using IDataSink::IDataSink;

    void setConfiguration(const nl::json& config) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setConfiguration, config);
    }

    void init() override { PYBIND11_OVERLOAD_PURE(void, IDataSink, init); }

    void setTag(const std::string& name, const std::string& value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setTag, name, value);
    }

    void setTag(const std::string& name, int8_t value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setTag, name, value);
    }

    void setTag(const std::string& name, int32_t value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setTag, name, value);
    }

    void setTag(const std::string& name, int64_t value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setTag, name, value);
    }

    void setTag(const std::string& name, double value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setTag, name, value);
    }

    void startMeasurement(
        const std::string& measurement,
        std::chrono::time_point<std::chrono::system_clock> time) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, startMeasurement, measurement,
                               time);
    }

    void setField(const std::string& name, const std::string& value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setField, name, value);
    }

    void setField(const std::string& name, int8_t value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setField, name, value);
    }

    void setField(const std::string& name, int32_t value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setField, name, value);
    }

    void setField(const std::string& name, int64_t value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setField, name, value);
    }

    void setField(const std::string& name, double value) override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, setField, name, value);
    }

    void recordPoint() override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, recordPoint);
    }

    void endMeasurement() override {
        PYBIND11_OVERLOAD_PURE(void, IDataSink, endMeasurement);
    }
};

// template trampoline to reduce code duplication
template <class DataSink = ConsoleSink>
class PyDataSink : public DataSink {
 public:
    using DataSink::DataSink;

    void setConfiguration(const nl::json& config) override {
        PYBIND11_OVERLOAD(void, DataSink, setConfiguration, config);
    }

    void init() override { PYBIND11_OVERLOAD(void, DataSink, init); }

    void setTag(const std::string& name, const std::string& value) override {
        PYBIND11_OVERLOAD(void, DataSink, setTag, name, value);
    }

    void setTag(const std::string& name, int8_t value) override {
        PYBIND11_OVERLOAD(void, DataSink, setTag, name, value);
    }

    void setTag(const std::string& name, int32_t value) override {
        PYBIND11_OVERLOAD(void, DataSink, setTag, name, value);
    }

    void setTag(const std::string& name, int64_t value) override {
        PYBIND11_OVERLOAD(void, DataSink, setTag, name, value);
    }

    void setTag(const std::string& name, double value) override {
        PYBIND11_OVERLOAD(void, DataSink, setTag, name, value);
    }

    void startMeasurement(
        const std::string& measurement,
        std::chrono::time_point<std::chrono::system_clock> time) override {
        PYBIND11_OVERLOAD(void, DataSink, startMeasurement, measurement, time);
    }

    void setField(const std::string& name, const std::string& value) override {
        PYBIND11_OVERLOAD(void, DataSink, setField, name, value);
    }

    void setField(const std::string& name, int8_t value) override {
        PYBIND11_OVERLOAD(void, DataSink, setField, name, value);
    }

    void setField(const std::string& name, int32_t value) override {
        PYBIND11_OVERLOAD(void, DataSink, setField, name, value);
    }

    void setField(const std::string& name, int64_t value) override {
        PYBIND11_OVERLOAD(void, DataSink, setField, name, value);
    }

    void setField(const std::string& name, double value) override {
        PYBIND11_OVERLOAD(void, DataSink, setField, name, value);
    }

    void recordPoint() override {
        PYBIND11_OVERLOAD(void, DataSink, recordPoint);
    }

    void endMeasurement() override {
        PYBIND11_OVERLOAD(void, DataSink, endMeasurement);
    }
};

void register_datasink(py::module& m) {
    py::class_<IDataSink, PyIDataSink, std::shared_ptr<IDataSink>>(m,
                                                                   "IDataSink")
        .def(py::init<const std::string&>())
        .def("setConfiguration", &IDataSink::setConfiguration)
        .def("init", &IDataSink::init)
        .def("setTag",
             (void (IDataSink::*)(const std::string&, const std::string&)) &
                 IDataSink::setTag)
        .def("setTag", (void (IDataSink::*)(const std::string&, int8_t)) &
                           IDataSink::setTag)
        .def("setTag", (void (IDataSink::*)(const std::string&, int32_t)) &
                           IDataSink::setTag)
        .def("setTag", (void (IDataSink::*)(const std::string&, int64_t)) &
                           IDataSink::setTag)
        .def("setTag", (void (IDataSink::*)(const std::string&, double)) &
                           IDataSink::setTag)
        .def("startMeasurement", &IDataSink::startMeasurement)
        .def("setField",
             (void (IDataSink::*)(const std::string&, const std::string&)) &
                 IDataSink::setField)
        .def("setField", (void (IDataSink::*)(const std::string&, int8_t)) &
                             IDataSink::setField)
        .def("setField", (void (IDataSink::*)(const std::string&, int32_t)) &
                             IDataSink::setField)
        .def("setField", (void (IDataSink::*)(const std::string&, int64_t)) &
                             IDataSink::setField)
        .def("setField", (void (IDataSink::*)(const std::string&, double)) &
                             IDataSink::setField)
        .def("recordPoint", &IDataSink::recordPoint)
        .def("endMeasurement", &IDataSink::endMeasurement);

    py::class_<CombinedSink, PyDataSink<CombinedSink>, IDataSink,
               std::shared_ptr<CombinedSink>>(m, "CombinedSink")
        .def(py::init<const std::string&>())
        .def("addSink", &CombinedSink::addSink);
    py::class_<ConsoleSink, PyDataSink<ConsoleSink>, IDataSink,
               std::shared_ptr<ConsoleSink>>(m, "ConsoleSink")
        .def(py::init<const std::string&>());
    py::class_<CSVSink, PyDataSink<CSVSink>, IDataSink,
               std::shared_ptr<CSVSink>>(m, "CSVSink")
        .def(py::init<const std::string&>());
    py::class_<InfluxDBSink, PyDataSink<InfluxDBSink>, IDataSink,
               std::shared_ptr<InfluxDBSink>>(m, "InfluxDBSink")
        .def(py::init<const std::string&>())
        .def("setPrecision", &InfluxDBSink::setPrecision);

    m.def("listDataSink", &EquipRegistry::listDataSink);
}
