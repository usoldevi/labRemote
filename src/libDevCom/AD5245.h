#ifndef AD5245_H
#define AD5245_H

#include <memory>

#include "I2CCom.h"
#include "Potentiometer.h"

/** \brief AD5245: 256-Position I2C-Compatible Digital Potentiometer
 *
 * [Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/AD5245.pdf)
 */
class AD5245 : public Potentiometer {
 public:
    /**
     * \param RAB Maximum resistance in Ohm's (5k, 10k or 100k)
     * \param com I2C communication
     */
    AD5245(double RAB, std::shared_ptr<I2CCom> com);
    virtual ~AD5245() = default;

    virtual void writeCount(int32_t pos, uint8_t ch = 0);
    virtual int32_t readCount(uint8_t ch = 0);

 private:
    std::shared_ptr<I2CCom> m_com;
};

#endif  // AD5245_H
