#include "AD56X9.h"

#include "DeviceComRegistry.h"
#include "LinearCalibration.h"
#include "NotSupportedException.h"
#include "OutOfRangeException.h"
REGISTER_DEVCOM(AD56X9, DACDevice)

const std::map<AD56X9::Model, AD56X9ModelInfo> AD56X9::ModelInfo = {
    {AD56X9::Model::AD5629, AD56X9ModelInfo({.MaxValue = 0xFFF})},
    {AD56X9::Model::AD5669, AD56X9ModelInfo({.MaxValue = 0xFFFF})}};

AD56X9::AD56X9(double reference, Model model, std::shared_ptr<I2CCom> com)
    : DACDevice(std::make_shared<LinearCalibration>(
          reference, ModelInfo.at(model).MaxValue)),
      m_model(model),
      m_com(com) {}

AD56X9::~AD56X9() {}

void AD56X9::setCount(int32_t counts) {
    counts &= ModelInfo.at(m_model).MaxValue;
    if (m_model == Model::AD5629) counts <<= 4;
    counts &= 0xFFFF;
    m_com->write_block({0x3F, static_cast<uint8_t>((counts >> 8) & 0xFF),
                        static_cast<uint8_t>((counts >> 0) & 0xFF)});
}

void AD56X9::setCount(uint8_t ch, int32_t counts) {
    if (ch > 7) throw OutOfRangeException(ch, 0, 7);

    if (m_model == Model::AD5629) counts <<= 4;
    counts &= 0xFFFF;

    m_com->write_block({static_cast<uint8_t>(0x30 | (ch & 0xF)),
                        static_cast<uint8_t>((counts >> 8) & 0xFF),
                        static_cast<uint8_t>((counts >> 0) & 0xFF)});
}

void AD56X9::setCount(const std::vector<uint8_t>& chs,
                      const std::vector<int32_t>& data) {
    for (std::size_t i = 0; i < chs.size(); i++) setCount(chs[i], data[i]);
}

int32_t AD56X9::readCount() {
    throw NotSupportedException("AD56X9 does not support reading.");
}

int32_t AD56X9::readCount(uint8_t ch) {
    throw NotSupportedException("AD56X9 does not support reading.");
}

void AD56X9::readCount(const std::vector<uint8_t>& chs,
                       std::vector<int32_t>& counts) {
    throw NotSupportedException("AD56X9 does not support reading.");
}
