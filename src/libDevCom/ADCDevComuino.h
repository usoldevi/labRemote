#ifndef ADCDEVCOMUINO
#define ADCDEVCOMUINO

#include <stdint.h>

#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "ADCDevice.h"
#include "Logger.h"
#include "TextSerialCom.h"

//! \brief Object to interface with Arduino's analog pins
/**
 * The 'ADCDevComuino' class creates an interface for communicating
 * with the analog pins on an Arduino via the ADC protocol setup in
 * arduino/devcomuino/devcomuino.ino. It implements the 'ADCDevice'
 * abstract class.
 *
 * The pin number of the desired analog pin may be passed via the
 * channel argument to all functions.
 *
 * Uses a linear calibration to convert the raw pin readout to
 * voltage.
 */
class ADCDevComuino : public ADCDevice {
 public:
    /**
     * \param reference The reference voltage on the Arduino that
     *   corresponds to a readout of 1024 (either 5 or 3.3). Used
     *   for converting the raw readout to voltage. May be set
     *   to 1024 for no convertion.
     * \param com The TextSerialCom to communicate with the Arduino
     */
    ADCDevComuino(double reference, std::shared_ptr<TextSerialCom> com);
    ~ADCDevComuino();

    //! Get the raw readout of the Arduino pin A0
    int32_t readCount();

    //! Get the raw readout of an Arduino analog pin
    /**
     * \param ch The pin number of the desired analog pin
     */
    int32_t readCount(uint8_t ch);

    //! Bulk readout of multiple analog pins
    /**
     * \param chs List of pin numbers to read
     * \param data Vector where readings will be stored
     */
    void readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data);

 private:
    std::shared_ptr<TextSerialCom> m_com;
};

#endif  // ADCDEVCOMUINO_H
