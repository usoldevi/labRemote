#ifndef ADCDEVICE_H
#define ADCDEVICE_H

#include <cstdint>
#include <map>
#include <memory>
#include <vector>

#include "DeviceCalibration.h"
#include "DummyCalibration.h"

//! \brief Abstract implementation of an analogue to digital converter
/**
 * The `ADCDevice` class serves two functions:
 * - Defines an abstract interface for reading ACD values in counts.
 * - Conversion from counts to volts using the `DeviceCalibration` classes.
 *
 * Multi-channel ADC's are supported via an optional channel argument to
 * most functions.
 *
 * ## Calibration
 *
 * Calibration handles converting the raw ADC readings (counts) to physical
 * units. The raw values can be retrieved using the `readCount` functions,
 * while the calibrated values are returned by the `read` functions.
 *
 * The units themselves are not defined and don't have to corresponds to the
 * to the voltage on the ADC input. For example, consider an ADC used to measure
 * the voltage across a current sense resistor. The calibration can be defined
 * such that the `read` function returns current (ΔV/R) instead of voltage (ΔV).
 *
 * For multi-channel ADC's, a per-channel calibration can be applied using the
 * `setCalibration(ch,...)` function. If no per-channel calibration is set for
 * a specific channel, then the global calibration (set via constructor or
 * `setCalibration(...)`) is used.
 */
class ADCDevice {
 public:
    /**
     * \param calibration Global calibration for all channels.
     */
    ADCDevice(std::shared_ptr<DeviceCalibration> calibration =
                  std::make_shared<DummyCalibration>());
    virtual ~ADCDevice();

    //! Set global calibration
    /**
     * \param calibration Global calibration for all channels.
     */
    void setCalibration(std::shared_ptr<DeviceCalibration> calibration);

    //! Set per-channel calibration
    /**
     * Override global calibration for specific channels.
     *
     * \param ch Channel to calibrate
     * \param calibration Calibration for `ch`
     */
    void setCalibration(uint8_t ch,
                        std::shared_ptr<DeviceCalibration> calibration);

    //! Read calibrated ADC value of "default" channel (volts)
    /**
     * \return result of `readCount`, followed by calibration.
     */
    double read();

    //! Read calibrated ADC value of channel `ch` (volts)
    /**
     * \param ch Channel to read
     *
     * \return result of `readCount`, followed by calibration.
     */
    double read(uint8_t ch);

    //! Bulk read calibrated ADC values of channels `chs` (volts)
    /**
     * The `data` vector used to store the results does not
     * need to be the same size as `chs`. It will be cleared
     * and allocated to the right size by this function.
     *
     * \param chs List of channel numbers to read
     * \param data Vector where readings will be stored
     *
     * \return result of `readCount`, followed by calibration.
     */
    void read(const std::vector<uint8_t>& chs, std::vector<double>& data);

    //! Read ADC value of "default" channel (counts)
    /**
     * Implementation of reading from the ADC goes here
     */
    virtual int32_t readCount() = 0;

    //! Read ADC value of channel `ch` (counts)
    /**
     * Implementation of reading from the ADC goes here
     *
     * \param ch Channel to read
     */
    virtual int32_t readCount(uint8_t ch) = 0;

    //! Bulk read ADC values of channels `chs` (counts)
    /**
     * Implementation of reading from the ADC goes here
     *
     * The `data` vector used to store the results does not
     * need to be the same size as `chs`. The implementatino
     * of `readCount` should clear and allocated the vector
     * to the right size.
     *
     * \param chs List of channel numbers to read
     * \param data Vector where readings will be stored
     */
    virtual void readCount(const std::vector<uint8_t>& chs,
                           std::vector<int32_t>& data) = 0;

 private:
    //! Find `DeviceCalibration` for given channel
    /**
     * The global calibration is returned if `ch` does not have a per-channel
     * calibration set.
     *
     * \return The calibration to be used for the given channel. (per-channel or
     * global)
     */
    std::shared_ptr<DeviceCalibration> findCalibration(uint8_t ch) const;

    std::shared_ptr<DeviceCalibration> m_calibration;
    std::map<uint8_t, std::shared_ptr<DeviceCalibration>> m_channelCalibration;
};

#endif  // ADCDEVICE_H
