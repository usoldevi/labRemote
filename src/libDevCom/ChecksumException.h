#ifndef CHECKSUMEXCEPTION_H
#define CHECKSUMEXCEPTION_H

#include <iostream>

#include "ComException.h"

class ChecksumException : ComException {
 public:
    ChecksumException(uint32_t checksum);

    virtual const char* what() const throw();

 private:
    std::string m_msg;
};

#endif  // CHECKSUMEXCEPTION_H
