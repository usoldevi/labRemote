#include "ClimateSensor.h"

#include <cmath>
#include <limits>

float ClimateSensor::dewPoint() {
    if (humidity() == 0)  // zero humidity!
        return -std::numeric_limits<float>::max();

    float N = (std::log(humidity() / 100) +
               (17.27 * temperature()) / (237.3 + temperature())) /
              17.27;
    float dew = 237.3 * N / (1 - N);

    return dew;
}
