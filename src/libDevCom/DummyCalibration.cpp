#include "DummyCalibration.h"

#include <cmath>

double DummyCalibration::calibrate(int32_t counts) { return counts; }

int32_t DummyCalibration::uncalibrate(double value) { return round(value); }
