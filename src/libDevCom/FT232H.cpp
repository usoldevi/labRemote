#include "FT232H.h"

// labremote
#include "DeviceComRegistry.h"
#include "Logger.h"
REGISTER_DEVCOM(FT232H, MPSSEChip)

FT232H::FT232H(MPSSEChip::Protocol protocol, MPSSEChip::Speed speed,
               MPSSEChip::Endianness endianness, const std::string& description,
               const std::string& serial)
    : MPSSEChip(protocol, speed, endianness, 0x0403, 0x6014, description,
                serial) {}
