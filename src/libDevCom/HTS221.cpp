#include "HTS221.h"

#include <math.h>
#include <unistd.h>

#include "ChecksumException.h"
#include "NotSupportedException.h"

HTS221::HTS221(std::shared_ptr<I2CCom> i2c) : m_i2c(i2c) {}

void HTS221::init() {
    // Read temperature calibration
    m_T0_degC = m_i2c->read_reg8(0x32);
    m_T1_degC = m_i2c->read_reg8(0x33);

    uint8_t T1T0msb = m_i2c->read_reg8(0x35);
    m_T0_degC |= ((T1T0msb & 0x3) << 8);
    m_T1_degC |= ((T1T0msb & 0xC) << 6);

    m_T0_OUT = (m_i2c->read_reg8(0x3D) << 8) | (m_i2c->read_reg8(0x3C) << 0);
    m_T1_OUT = (m_i2c->read_reg8(0x3F) << 8) | (m_i2c->read_reg8(0x3E) << 0);

    // Read humidity calibration
    m_H0_rH = m_i2c->read_reg8(0x30);
    m_H1_rH = m_i2c->read_reg8(0x31);

    m_H0_T0_OUT = (m_i2c->read_reg8(0x37) << 8) | (m_i2c->read_reg8(0x36) << 0);
    m_H1_T0_OUT = (m_i2c->read_reg8(0x3B) << 8) | (m_i2c->read_reg8(0x3A) << 0);

    // Power on the device
    powerDown(false);
}

void HTS221::reset() {}

void HTS221::powerDown(bool value) {
    uint8_t CTRL_REG1 = m_i2c->read_reg8(0x20);
    m_i2c->write_reg8(0x20, CTRL_REG1 | ((!value) << 7));
}

void HTS221::oneShot() {
    uint8_t CTRL_REG2 = m_i2c->read_reg8(0x21);
    m_i2c->write_reg8(0x21, CTRL_REG2 | 1);
}

void HTS221::read() {
    oneShot();  // Start a one shot measurement
    do {
        m_status = m_i2c->read_reg8(0x27);
    } while (((m_status & 0x3) != 3));

    // Get temperature
    int16_t T_OUT =
        (m_i2c->read_reg8(0x2B) << 8) | (m_i2c->read_reg8(0x2A) << 0);

    m_temperature =
        (m_T1_degC - m_T0_degC) * (T_OUT - m_T0_OUT) / (m_T1_OUT - m_T0_OUT) +
        m_T0_degC;
    m_temperature /= 8;

    // Get humidity
    int16_t H_T_OUT =
        (m_i2c->read_reg8(0x29) << 8) | (m_i2c->read_reg8(0x28) << 0);

    m_humidity = (m_H1_rH - m_H0_rH) * (H_T_OUT - m_H0_T0_OUT) /
                     (m_H1_T0_OUT - m_H0_T0_OUT) +
                 m_H0_rH;
    m_humidity /= 2;
}

uint8_t HTS221::status() const { return m_status; }

float HTS221::temperature() const { return m_temperature; }

float HTS221::humidity() const { return m_humidity; }

float HTS221::pressure() const {
    throw NotSupportedException("Si7021 does not have a pressure sensor");
}
