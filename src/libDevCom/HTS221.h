#ifndef HTS221_H
#define HTS221_H

#include <memory>

#include "ClimateSensor.h"
#include "I2CCom.h"

/**
 * The HTS221 climate sensor.
 * [Datasheet](https://www.st.com/resource/en/datasheet/hts221.pdf)
 */
class HTS221 : public ClimateSensor {
 public:
    HTS221(std::shared_ptr<I2CCom> i2c);
    virtual ~HTS221() = default;

    virtual void init();
    virtual void reset();
    virtual void read();

    virtual uint8_t status() const;
    virtual float temperature() const;
    virtual float humidity() const;
    virtual float pressure() const;

    //! Set power down bit
    /**
     * \param value True(/false) to enable(/disable) power down mode
     */
    void powerDown(bool value);

    //! Trigger a new measurement when running in one-shot mode.
    void oneShot();

 private:
    std::shared_ptr<I2CCom> m_i2c;

    //
    // Calibration information

    // Temperature
    int16_t m_T0_degC;
    int16_t m_T1_degC;
    int16_t m_T0_OUT;
    int16_t m_T1_OUT;

    // Humidity
    int16_t m_H0_rH;
    int16_t m_H1_rH;
    int16_t m_H0_T0_OUT;
    int16_t m_H1_T0_OUT;

    //
    // Data
    uint8_t m_status;
    float m_temperature;
    float m_humidity;
};

#endif  // HTS221_H
