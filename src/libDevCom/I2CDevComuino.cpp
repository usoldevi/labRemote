#include "I2CDevComuino.h"

#include <chrono>
#include <iomanip>
#include <sstream>
#include <thread>

#include "ComIOException.h"
#include "Logger.h"
#include "ScopeLock.h"

I2CDevComuino::I2CDevComuino(uint8_t deviceAddr,
                             std::shared_ptr<TextSerialCom> serial)
    : I2CCom(deviceAddr), m_serial(serial) {}

void I2CDevComuino::write_reg32(uint32_t address, uint32_t data) {
    write_block({static_cast<uint8_t>(address & 0xFF),
                 static_cast<uint8_t>((data >> 24) & 0xFF),
                 static_cast<uint8_t>((data >> 16) & 0xFF),
                 static_cast<uint8_t>((data >> 8) & 0xFF),
                 static_cast<uint8_t>((data >> 0) & 0xFF)});
}

void I2CDevComuino::write_reg16(uint32_t address, uint16_t data) {
    write_block({static_cast<uint8_t>(address & 0xFF),
                 static_cast<uint8_t>((data >> 8) & 0xFF),
                 static_cast<uint8_t>((data >> 0) & 0xFF)});
}

void I2CDevComuino::write_reg8(uint32_t address, uint8_t data) {
    write_block({static_cast<uint8_t>(address & 0xFF), data});
}

void I2CDevComuino::write_reg32(uint32_t data) {
    write_block({static_cast<uint8_t>((data >> 24) & 0xFF),
                 static_cast<uint8_t>((data >> 16) & 0xFF),
                 static_cast<uint8_t>((data >> 8) & 0xFF),
                 static_cast<uint8_t>((data >> 0) & 0xFF)});
}

void I2CDevComuino::write_reg16(uint16_t data) {
    write_block({static_cast<uint8_t>((data >> 8) & 0xFF),
                 static_cast<uint8_t>((data >> 0) & 0xFF)});
}

void I2CDevComuino::write_reg8(uint8_t data) { write_block({data}); }

void I2CDevComuino::write_block(uint32_t address,
                                const std::vector<uint8_t>& data) {
    std::vector<uint8_t> inbuf = data;
    inbuf.insert(inbuf.begin(), static_cast<uint8_t>(address & 0xFF));
    write_block(inbuf);
}

void I2CDevComuino::write_block(const std::vector<uint8_t>& data) {
    ScopeLock lock(this);
    std::stringstream cmd;
    cmd << "I2C WRITE " << std::hex << std::setfill('0') << std::setw(2)
        << (uint32_t)deviceAddr() << " ";
    for (uint8_t x : data)
        cmd << std::hex << std::setfill('0') << std::setw(2) << (uint32_t)x
            << std::dec;

    m_serial->send(cmd.str());

    std::string response = m_serial->receive();
}

uint32_t I2CDevComuino::read_reg32(uint32_t address) {
    write_reg32(address);

    std::vector<uint8_t> data(4);
    read_block(data);

    return (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | (data[3] << 0);
}

uint32_t I2CDevComuino::read_reg24(uint32_t address) {
    write_reg32(address);

    std::vector<uint8_t> data(3);
    read_block(data);
    return ((data[0] << 16) | (data[1] << 8) | (data[2] << 0));
}

uint16_t I2CDevComuino::read_reg16(uint32_t address) {
    write_reg32(address);

    std::vector<uint8_t> data(2);
    read_block(data);

    return (data[0] << 8) | (data[1] << 0);
}

uint8_t I2CDevComuino::read_reg8(uint32_t address) {
    write_reg32(address);

    std::vector<uint8_t> data(1);
    read_block(data);

    return (data[0] << 0);
}

uint32_t I2CDevComuino::read_reg32() {
    std::vector<uint8_t> data(4);
    read_block(data);

    return (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | (data[3] << 0);
}

uint32_t I2CDevComuino::read_reg24() {
    std::vector<uint8_t> data(3);
    read_block(data);

    return ((data[0] << 16) | (data[1] << 8) | (data[2] << 0));
}

uint16_t I2CDevComuino::read_reg16() {
    std::vector<uint8_t> data(2);
    read_block(data);

    return (data[0] << 8) | (data[1] << 0);
}

uint8_t I2CDevComuino::read_reg8() {
    std::vector<uint8_t> data(1);
    read_block(data);

    return (data[0] << 0);
}

void I2CDevComuino::read_block(uint32_t address, std::vector<uint8_t>& data) {
    write_reg32(address);
    read_block(data);
}

void I2CDevComuino::read_block(std::vector<uint8_t>& data) {
    ScopeLock lock(this);
    std::stringstream cmd;
    cmd << "I2C READ " << std::hex << std::setfill('0') << std::setw(2)
        << (uint32_t)deviceAddr() << " " << data.size();
    m_serial->send(cmd.str());
    // std::this_thread::sleep_for(std::chrono::seconds(1));
    std::string response = m_serial->receive();

    for (uint32_t i = 0; i < response.size() / 2; i++) {
        std::string byte = response.substr(2 * i, 2);
        data[i] = std::stoul(byte.c_str(), nullptr, 16);
    }
}

void I2CDevComuino::lock() {
    logger(logDEBUG4) << __PRETTY_FUNCTION__ << " -> TextSerialCom locked!";
    m_serial->lock();
}

void I2CDevComuino::unlock() {
    logger(logDEBUG4) << __PRETTY_FUNCTION__ << " -> TextSerialCom unlocked!";
    m_serial->unlock();
}
