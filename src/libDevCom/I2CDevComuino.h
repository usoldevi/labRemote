#ifndef I2CDEVCOMUINO_H
#define I2CDEVCOMUINO_H

#include <memory>

#include "I2CCom.h"
#include "TextSerialCom.h"

//! \brief Implementation of the I2C communication for devices connected to an
//! arduino
/**
 * it is based on the arduino sketch located in
 * arduino/devcomuino/devcomuino.ino the device needs to be connected to the
 * SDA/SCL pins of the arduino the SDA/SCL pins can not be used for analog
 * readings
 */

class I2CDevComuino : public I2CCom {
 public:
    I2CDevComuino(uint8_t deviceAddr, std::shared_ptr<TextSerialCom> serial);
    virtual ~I2CDevComuino() = default;

    //
    // Write commands
    virtual void write_reg32(uint32_t address, uint32_t data);
    virtual void write_reg16(uint32_t address, uint16_t data);
    virtual void write_reg8(uint32_t address, uint8_t data);

    virtual void write_reg32(uint32_t data);
    virtual void write_reg16(uint16_t data);
    virtual void write_reg8(uint8_t data);

    virtual void write_block(uint32_t address,
                             const std::vector<uint8_t>& data);
    virtual void write_block(const std::vector<uint8_t>& data);

    //
    // Read commands
    virtual uint32_t read_reg32(uint32_t address);
    virtual uint32_t read_reg24(uint32_t address);
    virtual uint16_t read_reg16(uint32_t address);
    virtual uint8_t read_reg8(uint32_t address);

    virtual uint32_t read_reg32();
    virtual uint32_t read_reg24();
    virtual uint16_t read_reg16();
    virtual uint8_t read_reg8();

    virtual void read_block(uint32_t address, std::vector<uint8_t>& data);
    virtual void read_block(std::vector<uint8_t>& data);

    /** Request exclusive access to device.
     *
     * If a single hardware bus is used to connect multiple devices,
     * the access to all of them should be locked to remove changes
     * of cross-talk.
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void lock();

    /** Release exclusive access to device.
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void unlock();

 private:
    /** Communication */
    std::shared_ptr<TextSerialCom> m_serial;
};

#endif  // I2CDEVCOMUINO_H
