#include "LinearCalibration.h"

#include <cmath>

LinearCalibration::LinearCalibration(double reference, uint32_t max,
                                     double offset)
    : m_reference(reference), m_max(max), m_offset(offset) {}

double LinearCalibration::calibrate(int32_t counts) {
    return (double)counts / m_max * m_reference + m_offset;
}

int32_t LinearCalibration::uncalibrate(double value) {
    return round((value - m_offset) * m_max / m_reference);
}
