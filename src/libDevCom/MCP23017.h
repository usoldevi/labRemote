#ifndef MCP23017_H
#define MCP23017_H

#include "I2CCom.h"
#include "IOExpander.h"

// Register map
#define MCP23017_IODIRA 0x00
#define MCP23017_IODIRB 0x01
#define MCP23017_IPOLA 0x02
#define MCP23017_IPOLB 0x03
#define MCP23017_GPINTENA 0x04
#define MCP23017_GPINTENB 0x05
#define MCP23017_DEFVALA 0x06
#define MCP23017_DEFVALB 0x07
#define MCP23017_INTCONA 0x08
#define MCP23017_INTCONB 0x09
#define MCP23017_IOCONA 0x0A
#define MCP23017_IOCONB 0x0B
#define MCP23017_GPPUA 0x0C
#define MCP23017_GPPUB 0x0D
#define MCP23017_INTFA 0x0E
#define MCP23017_INTFB 0x0F
#define MCP23017_INTCAPA 0x10
#define MCP23017_INTCAPB 0x11
#define MCP23017_GPIOA 0x12
#define MCP23017_GPIOB 0x13
#define MCP23017_OLATA 0x14
#define MCP23017_OLATB 0x15

//! 16-Bit I2C I/O Expander with Serial Interface
/**
 * [Reference](https://www.microchip.com/wwwproducts/en/mcp23017)
 */
class MCP23017 : public IOExpander {
 public:
    MCP23017(std::shared_ptr<I2CCom> com);
    virtual ~MCP23017() = default;

    virtual uint32_t getIO();
    virtual void setIO(uint32_t input);
    virtual void setInternalPullUp(uint32_t value);
    virtual void write(uint32_t value);
    virtual uint32_t read();

 private:
    std::shared_ptr<I2CCom> m_com;
};

#endif  // MCP23017_H
