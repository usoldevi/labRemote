#ifndef MCP3425_H
#define MCP3425_H

#include <stdint.h>

#include <memory>

#include "ADCDevice.h"
#include "I2CCom.h"

class MCP3425 : public ADCDevice {
 public:
    // Bit resolution
    enum Resolution { bit12 = 12, bit14 = 14, bit16 = 16 };

    // Conversion mode
    enum ConversionMode { Shot = 0, Cont = 1 };

    // Gain
    enum Gain { x1 = 0, x2 = 1, x4 = 2, x8 = 3 };

    // Initialisation of ADC
    MCP3425(Resolution bit_res, ConversionMode conv_mode, Gain gain,
            std::shared_ptr<I2CCom> com);
    MCP3425(std::shared_ptr<I2CCom> com);
    virtual ~MCP3425();

    // Read value from ADC
    virtual int32_t readCount();
    virtual int32_t readCount(uint8_t ch);
    virtual void readCount(const std::vector<uint8_t>& chs,
                           std::vector<int32_t>& data);

 private:
    // Properties of device
    Resolution m_resolution;
    ConversionMode m_conv_mode;
    Gain m_gain;

    // Pointer for I2C communication
    std::shared_ptr<I2CCom> m_com;
};

#endif  // MCP3425_H
