#ifndef NTCSENSOR_H
#define NTCSENSOR_H

#include <cstdint>
#include <string>

#include "ADCDevComuino.h"
#include "ClimateSensor.h"
#include "TextSerialCom.h"

//! \brief An implementation of 'ClimateSensor' for reading an NTC sensor
/**
 * Measures temperature using an NTC connected to one of the analog pins
 * on an ADC device.
 */
class NTCSensor : public ClimateSensor {
 public:
    /**
     * \param chan The channel number of the ADC channel connected to the NTC
     * \param dev The ADCDevice for interfacing with the Adruino or similar -
     * should be calibrated to return measured voltage as 0...100% of the
     * operational voltage \param steinhart Whether Steinhart-Hart equation for
     * the NTC-temperature conversion is used, which is more accurate than the
     * default beta formula. Please see e.g.
     * https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation,
     * https://en.wikipedia.org/wiki/Thermistor#B_or_%CE%B2_parameter_equation
     * \param Tref_A The reference temperature at which the NTC's resistance is
     *   given (in degrees Kelvin). Or Steinhart coefficient A if Steinhart-Hart
     * temperature conversion is used. \param Rref_B The NTC's resistance at
     * temperature Tref. Or Steinhart coefficient B if Steinhart-Hart
     * temperature conversion is used. \param Bntc_C The B value of the NTC. Or
     * Steinhart coefficient C if Steinhart-Hart temperature conversion is used.
     * \param readoverntc Whether the voltage drop over the NTC or the divider
     * resistor is read by the ADC.
     * False if NTC is top resistor in divider (connected to Vin),
     * True if NTC is bottom (connected to GND).
     * Please see e.g
     * https://www.petervis.com/electronics%20guides/calculators/thermistor/thermistor.html
     * \param Rdiv The resistance of the resistor in the voltage divider
     * \param Vsup The total supply voltage across the voltage divider circuit
     * (the NTC and the resistor)
     */
    NTCSensor(uint8_t chan, std::shared_ptr<ADCDevice> dev,
              bool steinhart = false, float Tref_A = 298.15,
              float Rref_B = 10000.0, float Bntc_C = 3435.0,
              bool readoverntc = false, float Rdiv = 10000.0, float Vsup = 5.0);
    virtual ~NTCSensor();

    //! Does nothing (requried to implement ClimateSensor)
    virtual void init();
    //! Does nothing (required to implement ClimateSensor)
    virtual void reset();
    //! Reads the temperature from the NTC and stores it in m_temperature
    virtual void read();

    //! Returns the temperature (in Celsius) from the most recent reading
    //  (defaults to -273.15)
    virtual float temperature() const;
    //! Throws a Not Supported exception (required to implement ClimateSensor)
    virtual float humidity() const;
    //! Throws a Not Supported exception (required to implement ClimateSensor
    virtual float pressure() const;

    virtual float getRntc(float Vout);

 private:
    //! Interface with the ADC device
    std::shared_ptr<ADCDevice> m_adcdev;
    //! The temperature at which the NTC's resistance is given (in Kelvin).
    float m_Tref;
    //! The resistance of the NTC at temperature m_Tref.
    float m_Rref;
    //! The B value of the NTC.
    float m_Bntc;
    //! Steinhart-Hart coefficient A
    float m_shA;
    //! Steinhart-Hart coefficient B
    float m_shB;
    //! Steinhart-Hart coefficient C
    float m_shC;
    //! The resistance of the resistor in the voltage divider circuit
    float m_Rdiv;
    //! The total voltage across the voltage divider (the NTC and the resistor)
    float m_Vsup;
    //! The channel on the ADC device connected to the NTC
    uint8_t m_chan;
    //! The temperature recorded in the last reading (defaults to -273.15)
    float m_temperature = -273.15;
    //! If the voltage drop over the NTC or the divider resistor is read by the
    //! ADC. Please see e.g.
    //! https://www.petervis.com/electronics%20guides/calculators/thermistor/thermistor.html
    bool m_readoverntc;
    //! If using Steinhart-Hart equation for the NTC-temperature conversion.
    //! Please see e.g.
    //! https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation
    bool m_steinhart;

    //! Converts the NTC resistance to temperature in Celsius
    float RtoC(float Rntc);
};

#endif  // NTCSENSOR_H
