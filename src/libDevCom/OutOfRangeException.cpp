#include "OutOfRangeException.h"

OutOfRangeException::OutOfRangeException(uint32_t ch, uint32_t minCh,
                                         uint32_t maxCh)
    : m_ch(ch),
      m_minCh(minCh),
      m_maxCh(maxCh),
      m_msg("OutOfRangeException: " + std::to_string(ch) + " must be between " +
            std::to_string(minCh) + " and " + std::to_string(maxCh)) {}

const char* OutOfRangeException::what() const throw() { return m_msg.c_str(); }
