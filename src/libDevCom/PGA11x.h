#ifndef PGA11x_H
#define PGA11x_H

#include <memory>

#include "MuxDevice.h"
#include "SPICom.h"

/** \brief PGA11x class of multiplexers.
 *
 * [Datasheet](https://www.ti.com/lit/ds/symlink/pga117.pdf)
 */
class PGA11x : public MuxDevice {
 public:
    PGA11x(std::shared_ptr<SPICom> com);
    ~PGA11x() = default;

    virtual void select(uint32_t ch);

 private:
    std::shared_ptr<SPICom> m_com;
};

#endif  // PGA11x_H
