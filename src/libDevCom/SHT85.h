#ifndef SHT85_H
#define SHT85_H

#include <memory>

#include "ClimateSensor.h"
#include "I2CCom.h"

/**
 * The SHT85 climate sensor.
 * [Datasheet](https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/2_Humidity_Sensors/Datasheets/Sensirion_Humidity_Sensors_SHT85_Datasheet.pdf)
 */
class SHT85 : public ClimateSensor {
 public:
    SHT85(std::shared_ptr<I2CCom> i2c);
    virtual ~SHT85();

    virtual void init();
    virtual void reset();
    virtual void read();

    virtual float temperature() const;
    virtual float humidity() const;
    virtual float pressure() const;

 private:
    std::shared_ptr<I2CCom> m_i2c;

    int m_status;
    float m_temperature;
    float m_humidity;

    uint8_t calcCRC(uint8_t byte0, uint8_t byte1, uint8_t crc) const;
};

#endif  // SHT85_H
