#ifndef SPICOM_H
#define SPICOM_H

#include <string>
#include <vector>

#include "DeviceCom.h"

class SPICom : public DeviceCom {
 public:
    SPICom();
    virtual ~SPICom();
};

#endif  // SPICOM_H
