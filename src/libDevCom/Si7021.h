#ifndef AdafruitSi7021_H
#define AdafruitSi7021_H

#include <memory>

#include "ClimateSensor.h"
#include "I2CCom.h"
/** \brief Si7021
 *
 * Implementation for the Si7021 humiidity/temperature sensor
 *
 * [Progamming
 * Manual](https://www.silabs.com/documents/public/data-sheets/Si7021-A20.pdf)
 */

class Si7021 : public ClimateSensor {
 public:
    Si7021(std::shared_ptr<I2CCom> i2c);
    virtual ~Si7021();

    virtual void init();
    virtual void reset();
    virtual void read();

    virtual float temperature() const;
    virtual float humidity() const;
    virtual float pressure() const;

 private:
    std::shared_ptr<I2CCom> m_i2c;

    float m_temperature;
    float m_humidity;
};

#endif  // Si7021_H
