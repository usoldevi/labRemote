#include "TCA9534.h"

#include "DeviceComRegistry.h"
REGISTER_DEVCOM(TCA9534, IOExpander)

TCA9534::TCA9534(std::shared_ptr<I2CCom> com) : IOExpander(), m_com(com) {}

uint32_t TCA9534::getIO() {
    return m_com->read_reg8(static_cast<uint32_t>(Register::Direction));
}

void TCA9534::setIO(uint32_t input) {
    m_com->write_reg8(static_cast<uint32_t>(Register::Direction), input);
}

void TCA9534::write(uint32_t value) {
    m_com->write_reg8(static_cast<uint32_t>(Register::Output), value);
}

uint32_t TCA9534::read() {
    return m_com->read_reg8(static_cast<uint32_t>(Register::Input));
}
