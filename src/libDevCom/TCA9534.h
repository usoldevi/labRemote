#ifndef TCA9534_H
#define TCA9534_H

#include "I2CCom.h"
#include "IOExpander.h"

//! TCA9534 IO expander
/**
 * [datasheet](https://www.ti.com/product/TCA9534)
 */
class TCA9534 : public IOExpander {
 private:
    //! Registers
    enum class Register {
        Input = 0x0,
        Output = 0x1,
        Polarity = 0x2,
        Direction = 0x3
    };

 public:
    TCA9534(std::shared_ptr<I2CCom> com);
    virtual ~TCA9534() = default;

    virtual uint32_t getIO();
    virtual void setIO(uint32_t input);
    virtual void write(uint32_t value);
    virtual uint32_t read();

 private:
    std::shared_ptr<I2CCom> m_com;
};

#endif  // TCA9534_H
