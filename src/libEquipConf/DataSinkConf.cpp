#include "DataSinkConf.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

#include "CombinedSink.h"
#include "DataSinkRegistry.h"
#include "FileUtils.h"  // labremote_schema_file
#include "Logger.h"

// json
#include <nlohmann/json-schema.hpp>
using nlohmann::json;
using nlohmann::json_schema::json_validator;

using json = nlohmann::json;

////////////////////
// Configuration
////////////////////

DataSinkConf::DataSinkConf() {}

DataSinkConf::~DataSinkConf() {}

DataSinkConf::DataSinkConf(const std::string& hardwareConfigFile) {
    setHardwareConfig(hardwareConfigFile);
}

DataSinkConf::DataSinkConf(const json& hardwareConfig) {
    setHardwareConfig(hardwareConfig);
}

void DataSinkConf::setHardwareConfig(const std::string& hardwareConfigFile) {
    // load JSON object from file
    std::ifstream i(hardwareConfigFile);
    if (!i.good()) {
        throw std::runtime_error("Provided datasink configuration file \"" +
                                 hardwareConfigFile +
                                 "\" could not be found or opened");
    }
    i >> m_hardwareConfig;

    // validate the loaded schema
    json_validator validator;
    std::string schema_path = utils::labremote_schema_file();
    if (schema_path != "") {
        std::ifstream ifs_schema(schema_path, std::ios::in);
        validator.set_root_schema(json::parse(ifs_schema));
        try {
            validator.validate(m_hardwareConfig);
        } catch (std::exception& e) {
            logger(logERROR) << "The provided JSON configuration failed the "
                                "schema check (input file: "
                             << hardwareConfigFile << ")";
            logger(logERROR)
                << "Are using an old-style labRemote JSON configuration? If "
                   "so, try running the following command:";
            logger(logERROR)
                << "    python /path/to/labRemote/scripts/update_config.py "
                << hardwareConfigFile;
            logger(logERROR)
                << "The error messsage from the JSON validation was: "
                << e.what();
        }
    } else {
        logger(logWARNING) << "Could not locate schema definition, cannot "
                              "validate datasink configuration!";
    }
}

void DataSinkConf::setHardwareConfig(const json& hardwareConfig) {
    // store JSON config file
    m_hardwareConfig = hardwareConfig;

    // validate the loaded schema
    json_validator validator;
    std::string schema_path = utils::labremote_schema_file();
    if (schema_path != "") {
        std::ifstream ifs_schema(schema_path, std::ios::in);
        validator.set_root_schema(json::parse(ifs_schema));
        try {
            validator.validate(m_hardwareConfig);
        } catch (std::exception& e) {
            logger(logERROR)
                << "The provided JSON configuration failed the schema check";
            logger(logERROR)
                << "Are using an old-style labRemote JSON configuration? If "
                   "so, try running the following command:";
            logger(logERROR)
                << "    python /path/to/labRemote/scripts/update_config.py";
            logger(logERROR)
                << "The error messsage from the JSON validation was: "
                << e.what();
        }
    } else {
        logger(logWARNING) << "Could not locate schema definition, cannot "
                              "validate datasink configuration!";
    }
}

json DataSinkConf::getDataSinkConf(const std::string& label) {
    for (const auto& datasink : m_hardwareConfig["datasinks"]) {
        if (datasink["name"] == label) return datasink;
    }
    return json();
}

json DataSinkConf::getDataStreamConf(const std::string& label) {
    for (const auto& datastream : m_hardwareConfig["datastreams"]) {
        if (datastream["name"] == label) return datastream;
    }
    return json();
}

////////////////////
// General private
////////////////////

std::shared_ptr<IDataSink> DataSinkConf::getDataSink(const std::string& name) {
    // first check if an object with the same name/type is already available
    // (was already instantiated)
    if (m_dataSinks.find(name) != m_dataSinks.end()) return m_dataSinks[name];

    // Otherwise, create the object
    // check first if hardware configuration is available

    json reqSinkConf = getDataSinkConf(name);
    if (reqSinkConf.empty()) {
        logger(logWARNING)
            << "Requested data sink not found in input configuration file: "
            << name;
        return nullptr;
    }

    // setup sink
    std::shared_ptr<IDataSink> ds =
        EquipRegistry::createDataSink(reqSinkConf["sinktype"], name);

    // configure and initialize
    ds->setConfiguration(reqSinkConf);
    ds->init();

    return ds;
}

std::shared_ptr<CombinedSink> DataSinkConf::getDataStream(
    const std::string& name) {
    // first check if an object with the same name/type is already available
    // (was already instantiated)
    if (m_dataStreams.find(name) != m_dataStreams.end())
        return m_dataStreams[name];

    // Otherwise, create the object
    // check first if hardware configuration is available

    json reqStreamConf = getDataStreamConf(name);
    if (reqStreamConf.empty()) {
        logger(logWARNING)
            << "Requested data stream not found in input configuration file: "
            << name;
        return nullptr;
    }

    // setup sink
    std::shared_ptr<CombinedSink> ds =
        std::dynamic_pointer_cast<CombinedSink>(EquipRegistry::createDataSink(
            reqStreamConf.value("sinktype", "CombinedSink"), name));

    // Check if combined sink
    if (!ds) {
        logger(logWARNING)
            << "Requested data stream does not derive from CombinedSink: "
            << name;
        return nullptr;
    }

    // Add sub sinks
    auto sinks = reqStreamConf.find("sinks");
    if (sinks == reqStreamConf.end()) {
        logger(logWARNING) << "No sub-sinks defined in " << name
                           << " combined sink.";
    } else {
        for (const auto& sink : sinks.value().items()) {
            std::shared_ptr<IDataSink> sds = getDataSink(sink.value());
            if (sds == nullptr) continue;
            ds->addSink(sds);
        }
    }

    // configure and initialize
    ds->setConfiguration(reqStreamConf);
    ds->init();

    return ds;
}
