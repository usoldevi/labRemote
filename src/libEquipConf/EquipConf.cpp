#include "EquipConf.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

#include "ChillerRegistry.h"
#include "ComRegistry.h"
#include "FileUtils.h"  // labremote_schema_file
#include "IChiller.h"
#include "ICom.h"
#include "IMeter.h"
#include "IPowerSupply.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "PowerSupplyChannel.h"
#include "PowerSupplyRegistry.h"

// json
#include <nlohmann/json-schema.hpp>
using nlohmann::json;
using nlohmann::json_schema::json_validator;

using json = nlohmann::json;

////////////////////
// Configuration
////////////////////

EquipConf::EquipConf() {}

EquipConf::~EquipConf() {}

EquipConf::EquipConf(const std::string& hardwareConfigFile) {
    setHardwareConfig(hardwareConfigFile);
}

EquipConf::EquipConf(const json& hardwareConfig) {
    setHardwareConfig(hardwareConfig);
}

void EquipConf::setHardwareConfig(const std::string& hardwareConfigFile) {
    // load JSON object from file
    std::ifstream i(hardwareConfigFile);
    if (!i.good()) {
        throw std::runtime_error("Provided equipment configuration file \"" +
                                 hardwareConfigFile +
                                 "\" could not be found or opened");
    }
    i >> m_hardwareConfig;

    // validate the loaded schema
    json_validator validator;
    std::string schema_path = utils::labremote_schema_file();
    if (schema_path != "") {
        std::ifstream ifs_schema(schema_path, std::ios::in);
        validator.set_root_schema(json::parse(ifs_schema));
        try {
            validator.validate(m_hardwareConfig);
        } catch (std::exception& e) {
            logger(logERROR) << "The provided JSON configuration failed the "
                                "schema check (input file: "
                             << hardwareConfigFile << ")";
            logger(logERROR)
                << "Are using an old-style labRemote JSON configuration? If "
                   "so, try running the following command:";
            logger(logERROR)
                << "    python /path/to/labRemote/scripts/update_config.py "
                << hardwareConfigFile;
            logger(logERROR)
                << "The error messsage from the JSON validation was: "
                << e.what();
        }
    } else {
        logger(logWARNING) << "Could not locate schema definition, cannot "
                              "validate equipment configuration!";
    }
}

void EquipConf::setHardwareConfig(const json& hardwareConfig) {
    // store JSON config file
    m_hardwareConfig = hardwareConfig;

    // validate the loaded schema
    json_validator validator;
    std::string schema_path = utils::labremote_schema_file();
    if (schema_path != "") {
        std::ifstream ifs_schema(schema_path, std::ios::in);
        validator.set_root_schema(json::parse(ifs_schema));
        try {
            validator.validate(m_hardwareConfig);
        } catch (std::exception& e) {
            logger(logERROR)
                << "The provided JSON configuration failed the schema check";
            logger(logERROR)
                << "Are using an old-style labRemote JSON configuration? If "
                   "so, try running the following command:";
            logger(logERROR)
                << "    python /path/to/labRemote/scripts/update_config.py";
            logger(logERROR)
                << "The error messsage from the JSON validation was: "
                << e.what();
        }
    } else {
        logger(logWARNING) << "Could not locate schema definition, cannot "
                              "validate equipment configuration!";
    }
}

json EquipConf::getDeviceConf(const std::string& label) {
    for (const auto& device : m_hardwareConfig["devices"]) {
        if (device["name"] == label) return device;
    }
    return json();
}

json EquipConf::getChannelConf(const std::string& label) {
    for (const auto& channel : m_hardwareConfig["channels"]) {
        if (channel["name"] == label) return channel;
    }
    return json();
}

////////////////////
// General private
////////////////////

////////////////////
// Power-Supply
////////////////////

std::shared_ptr<IPowerSupply> EquipConf::getPowerSupply(
    const std::string& name) {
    // first check if an object with the same name/type is already available
    // (was already instantiated)
    if (m_listPowerSupply.find(name) != m_listPowerSupply.end()) {
        return m_listPowerSupply[name];
    }

    // Otherwise, create the object
    // check first if hardware configuration is available
    json reqPSConf = getDeviceConf(name);
    if (reqPSConf.empty()) {
        logger(logWARNING)
            << "Requested device not found in input configuration file: "
            << name;
        return nullptr;
    }
    if (reqPSConf["hw-type"] != "PS") {
        logger(logWARNING) << "Requested power supply device has wrong hw-type "
                              "field in configuration."
                           << name << ", type = " << reqPSConf["hw-type"];
        return nullptr;
    }

    // setup device
    std::shared_ptr<IPowerSupply> ps =
        EquipRegistry::createPowerSupply(reqPSConf["hw-model"], name);

    // configure already the PS
    ps->setConfiguration(reqPSConf);

    // Setup communication object
    if (!reqPSConf.contains("communication")) {
        logger(logWARNING)
            << "Requested device is missing communication configuration";
        return nullptr;
    }
    ps->setCom(createCommunication(reqPSConf["communication"]));

    // Check
    ps->checkCompatibilityList();

    // update the cache of loaded power supplies
    m_listPowerSupply[name] = ps;

    return ps;
}

std::shared_ptr<PowerSupplyChannel> EquipConf::getPowerSupplyChannel(
    const std::string& name) {
    // first check if an object with the same name/type is already available
    // (was already instantiated)
    if (m_listPowerSupplyChannel.find(name) != m_listPowerSupplyChannel.end())
        return m_listPowerSupplyChannel[name];

    // Otherwise, create the object
    // check first if hardware configuration is available
    json reqChConf = getChannelConf(name);
    if (reqChConf.empty()) {
        logger(logWARNING)
            << "Requested channel not found in input configuration file: "
            << name;
        return nullptr;
    }

    if (reqChConf["hw-type"] != "PS") {
        logger(logWARNING) << "Requested power supply channel has wrong "
                              "hw-type field in configuration."
                           << name << ", type = " << reqChConf["hw-type"];
        return nullptr;
    }

    // Get physical power supply
    std::shared_ptr<IPowerSupply> ps = getPowerSupply(reqChConf["device"]);
    if (ps == nullptr) {
        logger(logWARNING) << "Cannot fetch requested power supply: "
                           << reqChConf["device"];
        return nullptr;
    }

    // Other settings
    unsigned ch = reqChConf["channel"];

    // Create!
    std::shared_ptr<PowerSupplyChannel> theChannel =
        std::make_shared<PowerSupplyChannel>(name, ps, ch);

    // configure already the PS
    if (reqChConf.contains("program"))
        theChannel->setProgram(reqChConf["program"]);

    // update the cache of loaded power supply channel objects
    m_listPowerSupplyChannel[name] = theChannel;

    return theChannel;
}

std::shared_ptr<IMeter> EquipConf::getMeter(const std::string& name) {
    // check if we have already constructed the requested IMeter instance,
    // and if so return that one
    if (m_listMeter.find(name) != m_listMeter.end()) {
        return m_listMeter[name];
    }

    // create the requested IMeter instance based on the loaded hardware
    // configuration
    json reqMeterConf = getDeviceConf(name);
    if (reqMeterConf.empty()) {
        logger(logWARNING)
            << "Requested device not found in input configuration file: "
            << name;
        return nullptr;
    }

    if (reqMeterConf["hw-type"] != "Meter") {
        logger(logWARNING) << "Requested meter device has wrong hw-type field "
                              "in configuration (device name = "
                           << name
                           << ", provided hw-type = " << reqMeterConf["hw-type"]
                           << ", expected hw-type = Meter)";
        return nullptr;
    }

    // setup the device
    std::shared_ptr<IMeter> meter =
        EquipRegistry::createMeter(reqMeterConf["hw-model"], name);

    // configure the meter
    meter->setConfiguration(reqMeterConf);

    // setup the communication object for this meter
    if (!reqMeterConf.contains("communication")) {
        // this check is implicitly handled by the schema checking...
        logger(logWARNING)
            << "Requested device is missing communication configuration";
        return nullptr;
    }

    meter->setCom(createCommunication(reqMeterConf["communication"]));

    // check
    meter->checkCompatibilityList();

    // update the cache of loaded meter objects
    m_listMeter[name] = meter;

    return meter;
}

std::shared_ptr<IChiller> EquipConf::getChiller(const std::string& name) {
    // check fi we have already constructed the requested IChiller instance,
    // and if so return that one
    if (m_listChiller.find(name) != m_listChiller.end()) {
        return m_listChiller[name];
    }

    // create the requested IChiller instance based on the loaded hardware
    // configuration
    json reqChillerConf = getDeviceConf(name);
    if (reqChillerConf.empty()) {
        logger(logWARNING)
            << "Requested device not found in input configuration file: "
            << name;
        return nullptr;
    }

    if (reqChillerConf["hw-type"] != "Chiller") {
        logger(logWARNING) << "Requested chiller device has wrong hw-type "
                              "field in configuration (device name = "
                           << name << ", provided hw-type = "
                           << reqChillerConf["hw-type"]
                           << ", expected hw-type = Chiller)";
        return nullptr;
    }

    // setup the device
    std::shared_ptr<IChiller> chiller =
        EquipRegistry::createChiller(reqChillerConf["hw-model"], name);

    // configure the meter
    chiller->setConfiguration(reqChillerConf);

    // setup the communication object for this chiller
    if (!reqChillerConf.contains("communication")) {
        // this check is implicitly handled by the schema checking...
        logger(logWARNING)
            << "Requested device is missing communication configuration";
        return nullptr;
    }

    chiller->setCom(createCommunication(reqChillerConf["communication"]));

    // update the cache of loaded chiller objects
    m_listChiller[name] = chiller;

    return chiller;
}

std::shared_ptr<ICom> EquipConf::createCommunication(const json& config) const {
    if (!config.contains("protocol"))
        throw std::runtime_error(
            "Communicaiton block is missing protocol definition.");

    std::shared_ptr<ICom> com = EquipRegistry::createCom(config["protocol"]);
    com->setConfiguration(config);
    com->init();

    return com;
}
