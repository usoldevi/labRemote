add_library(Meter SHARED)
target_sources(Meter
  PRIVATE
  IMeter.cpp
  MeterRegistry.cpp
  Keithley2000.cpp
  Keithley199.cpp
  Fluke8842.cpp
  HP3478A.cpp
  PM6680.cpp
  DMM6500.cpp
  KeysightDAQ970A.cpp
  )
target_link_libraries(Meter PRIVATE Com Utils)
target_include_directories(Meter PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(Meter PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS Meter)
