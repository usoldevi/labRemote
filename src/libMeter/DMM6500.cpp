#include "DMM6500.h"

#include "IMeter.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "ScopeLock.h"
REGISTER_METER(DMM6500)

DMM6500::DMM6500(const std::string& name) : IMeter(name, {"DMM6500"}) {}

bool DMM6500::ping(unsigned dev) {
    std::string result = "";
    if (dev == 0) {
        logger(logDEBUG) << "ping the multimeter.....";
        result = m_com->sendreceive("*IDN?");
    }
    if (dev == 1) {
        logger(logDEBUG) << "ping scanner card.....";
        result = m_com->sendreceive(":SYSTem:CARD1:IDN?");
    }
    result.erase(result.size() - 1);
    if (result.empty())
        throw std::runtime_error("Failed communication with the device");
    else
        logger(logDEBUG) << result;

    return !result.empty();
}

std::string DMM6500::identify() {
    std::string idn = m_com->sendreceive("*IDN?");
    return idn;
}

void DMM6500::autowait() {
    m_com->send("*OPC");
    int ESRValue = 0;
    while ((ESRValue & 1) == 0) {
        ESRValue = std::stoi(m_com->sendreceive("*ESR?"));
        std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
    }
}

void DMM6500::send(const std::string& cmd) {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    m_com->send("*CLS");
    m_com->send(cmd);
    this->autowait();
}

std::string DMM6500::sendreceive(const std::string& cmd) {
    m_com->send("*CLS");
    std::string buf = m_com->sendreceive(cmd);
    this->autowait();
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
    buf.erase(buf.size() - 1);
    return buf;
}

void DMM6500::reset() {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Initialising: ";
    this->send("*RST");
}

void DMM6500::sendScanCommand(std::string channel) {
    this->send(":ROUT:SCAN:CRE (@" + channel + ")");
    this->send(":ROUT:SCAN:COUNT:SCAN 1");
    this->send(":TRAC:CLE");
    this->send("INIT");
    this->send("*WAI");
}

// measure DC voltage with high precision
// take average of 10 repeatings
double DMM6500::measureDCV(unsigned channel) {
    std::string s_ch = "";
    if (channel > 0)  // use scanner card
        s_ch = ", (@" + std::to_string(channel) + ")";

    ScopeLock lock(m_com);
    this->send("*RST");
    this->send(":SENS:FUNC \"VOLT:DC\"" + s_ch);
    this->send(":SENS:VOLT:RANG:AUTO ON" + s_ch);
    this->send(":SENS:VOLT:INP AUTO" + s_ch);
    this->send(":SENS:VOLT:NPLC 1" + s_ch);
    this->send(":SENS:VOLT:AZER ON" + s_ch);
    this->send(":SENS:VOLT:AVER:TCON REP" + s_ch);
    this->send(":SENS:VOLT:AVER:COUN 10" + s_ch);
    this->send(":SENS:VOLT:AVER ON" + s_ch);

    if (channel > 0)  // use scanner card
        this->sendScanCommand(std::to_string(channel));

    return std::stod(this->sendreceive(":READ?"));
}

std::vector<double> DMM6500::measureDCV(const std::vector<unsigned>& channels) {
    std::vector<double> measurements(channels.size());
    for (unsigned channel : channels) {
        measurements.push_back(measureDCV(channel));
    }
    return measurements;
}

// measure resistance (2W or 4W)
// take average of 10 repeatings
double DMM6500::measureRES(unsigned channel, bool use4w) {
    std::string n_func = "RES";
    if (use4w) n_func = "FRES";

    std::string s_ch = "";
    if (channel > 0)  // use scanner card
        s_ch = ", (@" + std::to_string(channel) + ")";

    ScopeLock lock(m_com);
    this->send("*RST");
    this->send(":SENS:FUNC \"" + n_func + "\"" + s_ch);
    this->send(":SENS:" + n_func + ":RANG:AUTO ON" + s_ch);
    if (use4w) this->send(":SENS:" + n_func + ":OCOM ON" + s_ch);
    this->send(":SENS:" + n_func + ":AZER ON" + s_ch);
    this->send(":SENS:" + n_func + ":NPLC 1" + s_ch);
    this->send(":SENS:" + n_func + ":AVER:TCON REP" + s_ch);
    this->send(":SENS:" + n_func + ":AVER:COUN 10" + s_ch);
    this->send(":SENS:" + n_func + ":AVER ON" + s_ch);

    if (channel > 0)  // use scanner card
        this->sendScanCommand(std::to_string(channel));

    return std::stod(this->sendreceive(":READ?"));
}

std::vector<double> DMM6500::measureRES(const std::vector<unsigned>& channels,
                                        bool use4w) {
    std::vector<double> measurements(channels.size());
    for (unsigned channel : channels) {
        measurements.push_back(measureRES(channel, use4w));
    }
    return measurements;
}

// measure capacitance
// take average of 10 repeatings
double DMM6500::measureCAP(unsigned channel) {
    std::string s_ch = "";
    if (channel > 0)  // use scanner card
        s_ch = ", (@" + std::to_string(channel) + ")";

    ScopeLock lock(m_com);
    this->send("*RST");
    this->send(":SENS:FUNC \"CAP\"" + s_ch);
    this->send(":SENS:CAP:RANG:AUTO ON" + s_ch);
    this->send(":SENS:CAP:AVER:TCON REP" + s_ch);
    this->send(":SENS:CAP:AVER:COUN 10" + s_ch);
    this->send(":SENS:CAP:AVER ON" + s_ch);

    if (channel > 0)  // use scanner card
        this->sendScanCommand(std::to_string(channel));

    return std::stod(this->sendreceive(":READ?"));
}

std::vector<double> DMM6500::measureCAP(const std::vector<unsigned>& channels) {
    std::vector<double> measurements(channels.size());
    for (unsigned channel : channels) {
        measurements.push_back(measureCAP(channel));
    }
    return measurements;
}

// measure DC current with high precision
// take average of 10 repeatings
double DMM6500::measureDCI(unsigned channel) {
    std::string s_ch = "";
    if (channel > 0)  // use scanner card
        s_ch = ", (@" + std::to_string(channel) + ")";

    ScopeLock lock(m_com);
    this->send("*RST");
    this->send(":SENS:FUNC \"CURR:DC\"" + s_ch);
    this->send(":SENS:CURR:RANG:AUTO ON" + s_ch);
    this->send(":SENS:CURR:NPLC 1" + s_ch);
    this->send(":SENS:CURR:AZER ON" + s_ch);
    this->send(":SENS:CURR:AVER:TCON REP" + s_ch);
    this->send(":SENS:CURR:AVER:COUN 10" + s_ch);
    this->send(":SENS:CURR:AVER ON" + s_ch);

    if (channel > 0)  // use scanner card
        this->sendScanCommand(std::to_string(channel));

    return std::stod(this->sendreceive(":READ?"));
}

std::vector<double> DMM6500::measureDCI(const std::vector<unsigned>& channels) {
    std::vector<double> measurements(channels.size());
    for (unsigned channel : channels) {
        measurements.push_back(measureDCI(channel));
    }
    return measurements;
}
