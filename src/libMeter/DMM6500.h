#ifndef DMM6500_H
#define DMM6500_H
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "IMeter.h"

/*
 DMM6500 multimeter
 Author: Zhicai Zhang
 Date: Feb 2021
 Reference 1:
 https://www.tek.com/tektronix-and-keithley-digital-multimeter/dmm6500-manual/model-dmm6500-6-1-2-digit-multimeter-3
 Reference 2:
 https://www.tek.com/manual/model-dmm6500-6-1-2-digit-multimeter-user-manual
 2000-Scan card note:
 https://www.tek.com/default-accessory-series-manual/model-2000-scan-scanner-card
*/
class DMM6500 : public IMeter {
 public:
    DMM6500(const std::string& name);

    /** ping the device
     * @param dev: index of the device to ping (if there are multiple parts
     * connected) dev = 0 is for the main meter dev > 0 is for other parts
     * that's connected to the meter
     */
    virtual bool ping(unsigned dev = 0);

    virtual std::string identify();

    virtual void reset();

    /** measure DC voltage (unit: V)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     */
    virtual double measureDCV(unsigned channel = 0);
    virtual std::vector<double> measureDCV(
        const std::vector<unsigned>& channels);

    /** measure DC current (unit: A)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     */
    virtual double measureDCI(unsigned channel = 0);
    virtual std::vector<double> measureDCI(
        const std::vector<unsigned>& channels);

    /*
     measure resistance (unit: Ohm)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
     * @param use4w: whether or not use 4-wire method
    */
    virtual double measureRES(unsigned channel = 0, bool use4w = false);
    virtual std::vector<double> measureRES(
        const std::vector<unsigned>& channels, bool use4w = false);

    /*
     measure capacitance (unit: F)
     * @param channel: channel ID to perform the measurement
     * channel = 0 means without scanner card
     * channel > 0 means measure with specific channel on scanner card
    */
    virtual double measureCAP(unsigned channel = 0);
    virtual std::vector<double> measureCAP(
        const std::vector<unsigned>& channels);

 private:
    void send(const std::string& cmd);
    void sendScanCommand(std::string channel = "1");
    std::string sendreceive(const std::string& cmd);
    void autowait();

    std::chrono::milliseconds m_wait{10};
};

#endif
