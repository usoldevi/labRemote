#ifndef IMETER_H
#define IMETER_H

#include <iostream>
#include <string>
#include <vector>

#include "ICom.h"

/* Generic multimeter interface
    a typical workflow:
    IMeter meter(name);
    meter.setCom(com);
    meter.reset();
    meter.ping(0);
    meter.measureDCV();
    //....
*/
class IMeter {
 public:
    /** Constructor.
     * @param name: name of the meter
     * @param models: list of tested models (empty means no check is performed)
     */
    IMeter(const std::string& name, std::vector<std::string> models = {});

    /** Set communication object
     * The object is initialized if not already.
     *
     * Communication with the meter is checked after
     * setting the communication object.
     * If the device does not respond to `ping()`, an exception is thrown.
     *
     * @param com ICom instance for communicating with the IMeter instance
     */
    virtual void setCom(std::shared_ptr<ICom> com);

    /** Store JSON hardware configuration
     * @param config JSON configuration for the given meter only
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** ping the device
     * @param dev: index of the device to ping (if there are multiple parts
     * connected) dev = 0 is for the main meter dev > 0 is for other parts
     * that's connected to the meter
     */
    virtual bool ping(unsigned dev = 0);

    virtual void reset() = 0;

    /** Check that the device model is supported by checking that
     * model identifier of the connected meter contains
     * one of the supported models for that meter
     * Throws exception if compatibility is not found
     * If the list of model identifiers is empty, no check is performed
     */
    virtual void checkCompatibilityList();

    /* Return list of supported models. */
    virtual std::vector<std::string> getListOfModels();

    /** Returns the model of the meter connected.
     * Meter classes should return an empty string if this functionality does
     * not exist
     */
    virtual std::string identify() = 0;

    /** measure DC voltage (unit: V)
     * @param channel: channel ID to perform the measurement
     */
    virtual double measureDCV(unsigned channel = 0);

    /** measure DC voltage (unit: V) across a set of channels
     * @param channels: list of channels to measure
     */
    virtual std::vector<double> measureDCV(
        const std::vector<unsigned>& channels);

    /** measure DC current (unit: A)
     * @param channel: channel ID to perform the measurement
     */
    virtual double measureDCI(unsigned channel = 0);

    /** measure DC current (unit: A) across a set of channels
     * @param channels: list of channels to measure
     */
    virtual std::vector<double> measureDCI(
        const std::vector<unsigned>& channels);

    /** measure resistance (unit: Ohm)
     * @param channel: channel ID to perform the measurement
     * @param use4w: whether or not to use 4-wire method
     */
    virtual double measureRES(unsigned channel = 0, bool use4w = false);

    /** measure resistance (unit: Ohm) across a set of channels
     * @param channels: list of channels to measure
     * @param use4w: whether or not to use 4-wire method
     */
    virtual std::vector<double> measureRES(
        const std::vector<unsigned>& channels, bool use4w = false);

    /** measure capacitance (unit: F)
     * @param channel: channel ID to perform the measurement
     */
    virtual double measureCAP(unsigned channel = 0);

    /** measure capacitance (unit: F) across a set of channels
     * @param channels: list of channels to measure
     */
    virtual std::vector<double> measureCAP(
        const std::vector<unsigned>& channels);

 protected:
    std::shared_ptr<ICom> m_com = nullptr;

    /** Store device configuration name */
    std::string m_name;

    std::vector<std::string> m_models;
};

#endif
