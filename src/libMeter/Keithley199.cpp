#include "Keithley199.h"

#include "IMeter.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "ScopeLock.h"
#include "StringUtils.h"
REGISTER_METER(Keithley199)

Keithley199::Keithley199(const std::string& name)
    : IMeter(name, {"Keithley199"}) {}

bool Keithley199::ping(unsigned dev) {
    std::string result = "";
    logger(logDEBUG) << "ping the multimeter.....";
    result = m_com->receive();
    utils::rtrim(result);
    if (result.empty()) {
        throw std::runtime_error("Failed communication with the device");
    } else {
        logger(logDEBUG) << result;
    }

    return !result.empty();
}

std::string Keithley199::identify() {
    logger(logWARNING) << "Keithley199 does not have identity() function.";
    std::string idn = "Keithley199";
    return idn;
}

void Keithley199::send(const std::string& cmd) {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    m_com->send(cmd);
}

std::string Keithley199::sendreceive(const std::string& cmd) {
    std::string buf = m_com->sendreceive(cmd);
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
    utils::rtrim(buf);
    return buf;
}

void Keithley199::reset() {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Initialising: ";
    m_com->send("''CLEAR''");
}

// Check for overflow in reading
std::string Keithley199::getOutputStatus(std::string output) {
    logger(logDEBUG) << __PRETTY_FUNCTION__
                     << " -> Getting output status (N/O) from " << output;
    return output.substr(0, 1);
}

// Check which reading was performed with meter
std::string Keithley199::getOutputType(std::string output) {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Getting output type from "
                     << output;
    return output.substr(1, 3);
}

// Interpret string output from meter
double Keithley199::getOutputReading(std::string output) {
    logger(logDEBUG) << __PRETTY_FUNCTION__
                     << " -> Getting output reading from " << output;

    double reading = std::stod(output.substr(4, output.find("E") - 4));
    std::string sign = output.substr(4, 1);

    int reading_power = std::stoi(output.substr(
        output.find("E") + 2, output.size() - output.find("E") + 1));
    std::string reading_power_sign = output.substr(output.find("E") + 1, 1);

    if (sign.compare("-") == 0) reading = -1.0 * reading;
    if (reading_power_sign.compare("-") == 0)
        reading_power = reading_power * -1.0;

    return reading * pow(10., reading_power);
}

// measure DC voltage
double Keithley199::measureDCV(unsigned channel) {
    ScopeLock lock(m_com);

    m_com->send("''X''");
    m_com->send("''F0R0X''");
    std::string output = m_com->receive();

    if (getOutputType(output).compare("DCV") != 0)
        throw std::runtime_error("Meter asked to read DC voltage (DCV), but " +
                                 output + " read instead!");
    if (getOutputStatus(output).compare("O") == 0)
        logger(logWARNING) << "Output reading has overflow.";

    return getOutputReading(output);
}

// measure resistance
double Keithley199::measureRES(unsigned channel, bool use4w) {
    ScopeLock lock(m_com);

    m_com->send("''X''");
    m_com->send("''F2R0X''");
    std::string output = m_com->receive();

    if (getOutputType(output).compare("OHM") != 0)
        throw std::runtime_error("Meter asked to read resistance (OHM), but " +
                                 output + " read instead!");
    if (getOutputStatus(output).compare("O") == 0)
        logger(logWARNING) << "Output reading has overflow.";

    return getOutputReading(output);
}

// measure DC current
double Keithley199::measureDCI(unsigned channel) {
    ScopeLock lock(m_com);

    m_com->send("''X''");
    m_com->send("''XF3R0X''");
    std::string output = m_com->receive();

    if (getOutputType(output).compare("DCI") != 0)
        throw std::runtime_error("Meter asked to read DC current (DCI), but " +
                                 output + " read instead!");
    if (getOutputStatus(output).compare("O") == 0)
        logger(logWARNING) << "Output reading has overflow.";

    return getOutputReading(output);
}
