#include "Keithley2000.h"

#include "IMeter.h"
#include "Logger.h"
#include "MeterRegistry.h"
#include "ScopeLock.h"
#include "StringUtils.h"
REGISTER_METER(Keithley2000)

Keithley2000::Keithley2000(const std::string& name)
    : IMeter(name, {"Keithley2000"}) {}

bool Keithley2000::ping(unsigned dev) {
    std::string result = "";
    if (dev == 0) {
        logger(logDEBUG) << "ping the multimeter.....";
        result = m_com->sendreceive("*IDN?");
    }
    if (dev == 1) {
        logger(logDEBUG) << "ping scanner card.....";
        result = m_com->sendreceive("*OPT?");
    }
    utils::rtrim(result);
    if (result.empty()) {
        throw std::runtime_error("Failed communication with the device");
    } else {
        logger(logDEBUG) << result;
    }

    return !result.empty();
}

std::string Keithley2000::identify() {
    std::string idn = m_com->sendreceive("*IDN?");
    return idn;
}

void Keithley2000::autowait() {
    m_com->send("*OPC");
    int ESRValue = 0;
    while ((ESRValue & 1) == 0) {
        ESRValue = std::stoi(m_com->sendreceive("*ESR?"));
        std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
    }
}

void Keithley2000::send(const std::string& cmd) {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    m_com->send("*CLS");
    m_com->send(cmd);
    this->autowait();
}

std::string Keithley2000::sendreceive(const std::string& cmd) {
    m_com->send("*CLS");
    std::string buf = m_com->sendreceive(cmd);
    this->autowait();
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
    utils::rtrim(buf);
    return buf;
}

void Keithley2000::reset() {
    logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> Initialising: ";
    this->send("*RST");
}

void Keithley2000::sendScanCommand(std::string channel) {
    this->send(":ROUT:OPEN:ALL");
    this->send(":TRAC:CLE");
    this->send(":ROUT:CLOS (@ " + channel + ")");
    this->send(":ROUT:SCAN (@ " + channel + ")");
    this->send(":ROUT:SCAN:LSEL INT");
    this->send(":INIT");
    this->send("*WAI");
}

// measure DC voltage with high precision
// take average of 10 repeatings
double Keithley2000::measureDCV(unsigned channel) {
    ScopeLock lock(m_com);
    this->send("*RST");
    this->send(":SENS:FUNC \"VOLT:DC\"");

    if (channel > 0)  // use scanner card
        this->sendScanCommand(std::to_string(channel));

    return std::stod(this->sendreceive(":READ?"));
}

std::vector<double> Keithley2000::measureDCV(
    const std::vector<unsigned>& channels) {
    std::vector<double> measurements(channels.size());
    for (unsigned channel : channels) {
        measurements.push_back(measureDCV(channel));
    }
    return measurements;
}

// measure resistance (2W or 4W)
// take average of 10 repeatings
double Keithley2000::measureRES(unsigned channel, bool use4w) {
    std::string n_func = "RES";
    if (use4w) n_func = "FRES";

    ScopeLock lock(m_com);
    this->send("*RST");
    this->send(":SENS:FUNC \"" + n_func + "\"");

    if (channel > 0)  // use scanner card
        this->sendScanCommand(std::to_string(channel));

    return std::stod(this->sendreceive(":READ?"));
}

std::vector<double> Keithley2000::measureRES(
    const std::vector<unsigned>& channels, bool use4w) {
    std::vector<double> measurements(channels.size());
    for (unsigned channel : channels) {
        measurements.push_back(measureRES(channel, use4w));
    }
    return measurements;
}

// measure DC current with high precision
// take average of 10 repeatings
double Keithley2000::measureDCI(unsigned channel) {
    ScopeLock lock(m_com);
    this->send("*RST");
    this->send(":SENS:FUNC \"CURR:DC\"");

    if (channel > 0)  // use scanner card
        this->sendScanCommand(std::to_string(channel));

    return std::stod(this->sendreceive(":READ?"));
}

std::vector<double> Keithley2000::measureDCI(
    const std::vector<unsigned>& channels) {
    std::vector<double> measurements(channels.size());
    for (unsigned channel : channels) {
        measurements.push_back(measureDCI(channel));
    }
    return measurements;
}
