#ifndef METER_REGISTRY_H
#define METER_REGISTRY_H

#include <map>
#include <string>
#include <vector>

#include "ClassRegistry.h"
#include "IMeter.h"

namespace EquipRegistry {

bool registerMeter(
    const std::string& model,
    std::function<std::shared_ptr<IMeter>(const std::string&)> f);

std::shared_ptr<IMeter> createMeter(const std::string& model,
                                    const std::string& name);

std::vector<std::string> listMeter();
}  // namespace EquipRegistry

#define REGISTER_METER(model)                                               \
    static bool _registered_##model = EquipRegistry::registerMeter(         \
        #model, std::function<std::shared_ptr<IMeter>(const std::string&)>( \
                    [](const std::string& name) {                           \
                        return std::shared_ptr<IMeter>(new model(name));    \
                    }));

#endif  // METER_REGISTRY_H
