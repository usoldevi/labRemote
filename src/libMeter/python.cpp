#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "pybind11_json/pybind11_json.hpp"

// std/stl
#include <string>
#include <vector>

// labRemote
#include "DMM6500.h"
#include "Fluke8842.h"
#include "ICom.h"
#include "IMeter.h"
#include "Keithley2000.h"
#include "KeysightDAQ970A.h"

namespace py = pybind11;

class PyIMeter : public IMeter {
 public:
    using IMeter::IMeter;
    void setCom(std::shared_ptr<ICom> com) override {
        PYBIND11_OVERLOAD(void, IMeter, setCom, com);
    }
    void setConfiguration(const nlohmann::json& config) override {
        PYBIND11_OVERLOAD(void, IMeter, setConfiguration, config);
    }
    bool ping(unsigned dev) override {
        PYBIND11_OVERLOAD(bool, IMeter, ping, dev);
    }
    void reset() override { PYBIND11_OVERLOAD_PURE(void, IMeter, reset, ); }
    void checkCompatibilityList() override {
        PYBIND11_OVERLOAD(void, IMeter, checkCompatibilityList, );
    }
    std::vector<std::string> getListOfModels() {
        PYBIND11_OVERLOAD(std::vector<std::string>, IMeter, getListOfModels, );
    }
    std::string identify() override {
        PYBIND11_OVERLOAD_PURE(std::string, IMeter, identify, );
    }
    double measureDCV(unsigned channel) override {
        PYBIND11_OVERLOAD(double, IMeter, measureDCV, channel);
    }
    std::vector<double> measureDCV(
        const std::vector<unsigned>& channels) override {
        PYBIND11_OVERLOAD(std::vector<double>, IMeter, measureDCV, channels);
    }
    double measureDCI(unsigned channel) override {
        PYBIND11_OVERLOAD(double, IMeter, measureDCI, channel);
    }
    std::vector<double> measureDCI(
        const std::vector<unsigned>& channels) override {
        PYBIND11_OVERLOAD(std::vector<double>, IMeter, measureDCI, channels);
    }
    double measureRES(unsigned channel, bool use4w) override {
        PYBIND11_OVERLOAD(double, IMeter, measureRES, channel, use4w);
    }
    std::vector<double> measureRES(const std::vector<unsigned>& channels,
                                   bool use4w) override {
        PYBIND11_OVERLOAD(std::vector<double>, IMeter, measureRES, channels,
                          use4w);
    }
    double measureCAP(unsigned channel) override {
        PYBIND11_OVERLOAD(double, IMeter, measureCAP, channel);
    }
    std::vector<double> measureCAP(
        const std::vector<unsigned>& channels) override {
        PYBIND11_OVERLOAD(std::vector<double>, IMeter, measureCAP, channels);
    }
};  // PyIMeter

void register_meter(py::module& m) {
    py::class_<IMeter, PyIMeter, std::shared_ptr<IMeter>>(m, "IMeter")
        .def(py::init<const std::string&, std::vector<std::string>>())
        .def("setCom", &IMeter::setCom)
        .def("setConfiguration", &IMeter::setConfiguration)
        .def("ping", &IMeter::ping)
        .def("reset", &IMeter::reset)
        .def("checkCompatibilityList", &IMeter::checkCompatibilityList)
        .def("getListOfModels", &IMeter::getListOfModels)
        .def("identify", &IMeter::identify)
        .def("measureDCV",
             static_cast<double (IMeter::*)(unsigned)>(&IMeter::measureDCV))
        .def("measureDCV",
             static_cast<std::vector<double> (IMeter::*)(
                 const std::vector<unsigned>&)>(&IMeter::measureDCV))
        .def("measureDCI",
             static_cast<double (IMeter::*)(unsigned)>(&IMeter::measureDCI))
        .def("measureDCI",
             static_cast<std::vector<double> (IMeter::*)(
                 const std::vector<unsigned>&)>(&IMeter::measureDCI))
        .def("measureRES", static_cast<double (IMeter::*)(unsigned, bool)>(
                               &IMeter::measureRES))
        .def("measureRES",
             static_cast<std::vector<double> (IMeter::*)(
                 const std::vector<unsigned>&, bool)>(&IMeter::measureRES))
        .def("measureCAP",
             static_cast<double (IMeter::*)(unsigned)>(&IMeter::measureCAP))
        .def("measureCAP",
             static_cast<std::vector<double> (IMeter::*)(
                 const std::vector<unsigned>&)>(&IMeter::measureCAP));

    // DMM6500
    py::class_<DMM6500, IMeter, std::shared_ptr<DMM6500>>(m, "DMM6500")
        .def(py::init<const std::string&>())
        .def("ping", &DMM6500::ping)
        .def("identify", &DMM6500::identify)
        .def("reset", &DMM6500::reset)
        .def("measureDCV",
             static_cast<double (DMM6500::*)(unsigned)>(&DMM6500::measureDCV))
        .def("measureDCV",
             static_cast<std::vector<double> (DMM6500::*)(
                 const std::vector<unsigned>&)>(&DMM6500::measureDCV))
        .def("measureDCI",
             static_cast<double (DMM6500::*)(unsigned)>(&DMM6500::measureDCI))
        .def("measureDCI",
             static_cast<std::vector<double> (DMM6500::*)(
                 const std::vector<unsigned>&)>(&DMM6500::measureDCI))
        .def("measureRES", static_cast<double (DMM6500::*)(unsigned, bool)>(
                               &DMM6500::measureRES))
        .def("measureRES",
             static_cast<std::vector<double> (DMM6500::*)(
                 const std::vector<unsigned>&, bool)>(&DMM6500::measureRES))
        .def("measureCAP",
             static_cast<double (DMM6500::*)(unsigned)>(&DMM6500::measureCAP))
        .def("measureCAP",
             static_cast<std::vector<double> (DMM6500::*)(
                 const std::vector<unsigned>&)>(&DMM6500::measureCAP));

    // KEITHLEY2000
    py::class_<Keithley2000, IMeter, std::shared_ptr<Keithley2000>>(
        m, "Keithley2000")
        .def(py::init<const std::string&>())
        .def("ping", &Keithley2000::ping)
        .def("identify", &Keithley2000::identify)
        .def("reset", &Keithley2000::reset)
        .def("measureDCV", static_cast<double (Keithley2000::*)(unsigned)>(
                               &Keithley2000::measureDCV))
        .def("measureDCV",
             static_cast<std::vector<double> (Keithley2000::*)(
                 const std::vector<unsigned>&)>(&Keithley2000::measureDCV))
        .def("measureDCI", static_cast<double (Keithley2000::*)(unsigned)>(
                               &Keithley2000::measureDCI))
        .def("measureDCI",
             static_cast<std::vector<double> (Keithley2000::*)(
                 const std::vector<unsigned>&)>(&Keithley2000::measureDCI))
        .def("measureRES",
             static_cast<double (Keithley2000::*)(unsigned, bool)>(
                 &Keithley2000::measureRES))
        .def("measureRES", static_cast<std::vector<double> (Keithley2000::*)(
                               const std::vector<unsigned>&, bool)>(
                               &Keithley2000::measureRES));

    // KeysightDAQ970A
    py::class_<KeysightDAQ970A, IMeter, std::shared_ptr<KeysightDAQ970A>>
        daq970a(m, "KeysightDAQ970A");
    daq970a.def(py::init<const std::string&>())
        .def("ping", &KeysightDAQ970A::ping)
        .def("identify", &KeysightDAQ970A::identify)
        .def("reset", &KeysightDAQ970A::reset)
        .def("measureDCV", static_cast<double (KeysightDAQ970A::*)(unsigned)>(
                               &KeysightDAQ970A::measureDCV))
        .def("measureDCV",
             static_cast<std::vector<double> (KeysightDAQ970A::*)(
                 const std::vector<unsigned>&)>(&KeysightDAQ970A::measureDCV))
        .def("measureDCI", static_cast<double (KeysightDAQ970A::*)(unsigned)>(
                               &KeysightDAQ970A::measureDCI))
        .def("measureDCI",
             static_cast<std::vector<double> (KeysightDAQ970A::*)(
                 const std::vector<unsigned>&)>(&KeysightDAQ970A::measureDCI))
        .def("measureRES",
             static_cast<double (KeysightDAQ970A::*)(unsigned, bool)>(
                 &KeysightDAQ970A::measureRES))
        .def("measureRES", static_cast<std::vector<double> (KeysightDAQ970A::*)(
                               const std::vector<unsigned>&, bool)>(
                               &KeysightDAQ970A::measureRES))
        .def("measureCAP", static_cast<double (KeysightDAQ970A::*)(unsigned)>(
                               &KeysightDAQ970A::measureCAP))
        .def("measureCAP",
             static_cast<std::vector<double> (KeysightDAQ970A::*)(
                 const std::vector<unsigned>&)>(&KeysightDAQ970A::measureCAP))
        .def("setMeasurementStat", &KeysightDAQ970A::setMeasurementStat)
        .def("setTrigCount", &KeysightDAQ970A::setTrigCount)
        .def("getAverage", &KeysightDAQ970A::getAverage)
        .def("getStdDeviation", &KeysightDAQ970A::getStdDeviation)
        .def("getMaximum", &KeysightDAQ970A::getMaximum)
        .def("getMinimum", &KeysightDAQ970A::getMinimum);

    py::enum_<KeysightDAQ970A::Statistic>(daq970a, "Statistic")
        .value("All", KeysightDAQ970A::Statistic::All)
        .value("Average", KeysightDAQ970A::Statistic::Average)
        .value("StdDeviation", KeysightDAQ970A::Statistic::StdDeviation)
        .value("Minimum", KeysightDAQ970A::Statistic::Minimum)
        .value("Maximum", KeysightDAQ970A::Statistic::Maximum)
        .value("PeakToPeak", KeysightDAQ970A::Statistic::PeakToPeak);

    // Fluke8842
    py::class_<Fluke8842, IMeter, std::shared_ptr<Fluke8842>>(m, "Fluke8842")
        .def(py::init<const std::string&>())
        .def("ping", &Fluke8842::ping)
        .def("identify", &Fluke8842::identify)
        .def("reset", &Fluke8842::reset)
        .def("measureDCV", static_cast<double (Fluke8842::*)(unsigned)>(
                               &Fluke8842::measureDCV))
        .def("measureDCI", static_cast<double (Fluke8842::*)(unsigned)>(
                               &Fluke8842::measureDCI));
}  // register_meter
