#ifndef AGILENTE3631APS_H
#define AGILENTE3631APS_H

#include <string>

#include "AgilentPs.h"

/**
 * Implementation for the Agilent E3631A Triple Output
 * DC Power Supply ([Programming
 * Manual](http://literature.cdn.keysight.com/litweb/pdf/E3631-90002.pdf)).
 *
 * Using numerical channel mapping from the power supply
 * operations manual:
 *  - 1: P6V
 *  - 2: P25V
 *  - 3: M25V
 */
class AgilentE3631APs : public AgilentPs {
 public:
    AgilentE3631APs(const std::string& name);
    ~AgilentE3631APs() = default;

    /** Turn on all channels
     *
     * E3631A does not support enabling specific channels.
     *
     * @param channel ignored
     */
    virtual void turnOn(unsigned channel);

    /** Turn off all channels
     *
     * E3631A does not support enabling specific channels.
     *
     * @param channel ignored
     */
    virtual void turnOff(unsigned channel);
};

#endif  // AGILENTE3631APS_H
