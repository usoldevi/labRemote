#ifndef AGILENTE3634APS_H
#define AGILENTE3634APS_H

#include <string>

#include "AgilentPs.h"

/**
 * Implementation for the Agilent E3633A/E3634A Single Output
 * DC Power Supply ([Programming
 * Manual](http://literature.cdn.keysight.com/litweb/pdf/E3634-90001.pdf)).
 * Other single-channel Agilent power supplies might be supported too, but have
 * not been checked.
 *
 * The command set seems to be the same as E364xA, thus the implemetation
 * is inherited from there.
 */
class AgilentE3634APs : public AgilentPs {
 public:
    AgilentE3634APs(const std::string& name);
    ~AgilentE3634APs() = default;
};

#endif  // AGILENTE3634APS_H
