#include "AgilentE3648APs.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentE3648APs)

AgilentE3648APs::AgilentE3648APs(const std::string& name)
    : AgilentPs(name, {"E3648A"}, 2) {}
