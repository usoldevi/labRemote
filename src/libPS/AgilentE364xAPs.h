#ifndef AGILENTE364XAPS_H
#define AGILENTE364XAPS_H

#include <string>

#include "AgilentPs.h"

/**
 * Implementation for the Agilent E364xA Single Output
 * DC Power Supply ([Programming
 * Manual](https://www.mouser.com/pdfdocs/3ugE3640-90001.pdf)). Other
 * single-channel Agilent power supplies might be supported too, but have not
 * been checked.
 */
class AgilentE364xAPs : public AgilentPs {
 public:
    AgilentE364xAPs(const std::string& name);
    ~AgilentE364xAPs() = default;
};

#endif  // AGILENTE364XAPS_H
