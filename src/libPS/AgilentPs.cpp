#include "AgilentPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentPs)

AgilentPs::AgilentPs(const std::string& name, std::vector<std::string> models,
                     unsigned maxChannels)
    : SCPIPs(name, models, maxChannels) {}

void AgilentPs::beepOff() {
    m_com->send("SYST:BEEP:STAT OFF");  // disable beep
}
