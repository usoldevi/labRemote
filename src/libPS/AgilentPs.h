#ifndef AGILENTPS_H
#define AGILENTPS_H

#include <chrono>
#include <memory>
#include <string>

#include "SCPIPs.h"
#include "SerialCom.h"

/**
 * Base implemetation for Agilent/Keysight power supplies that share
 * a very similar command set.
 *
 * Options are possible to add additional for error checking on
 *  - model compatibility (list of strings partially matched against *idn?,
 * empty means no check is performed)
 *  - channel checking (maximum number of channels, 0 means no check is
 * performed)
 */
class AgilentPs : public SCPIPs {
 public:
    /**
     * @param name Name of the power supply
     * @param models List of supported models (empty means no check is
     * performed)
     * @param maxChannels Maximum number of channels in the power supply (0
     * means no maximum)
     */
    AgilentPs(const std::string& name, std::vector<std::string> models = {},
              unsigned maxChannels = 0);
    ~AgilentPs() = default;

    /** Disable the audible beep
     */
    void beepOff();
};

#endif
