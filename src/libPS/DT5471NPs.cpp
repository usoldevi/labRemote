#include "DT5471NPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"
#include "TextSerialCom.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(DT5471NPs)

DT5471NPs::DT5471NPs(const std::string& name)
    : DT54xxPs(name, {"DT5471"}, Polarity::Negative, 51e-6) {}
