#ifndef DT5472NPS_H
#define DT5472NPS_H

#include "DT54xxPs.h"

//! \brief Implementation for the CAEN DT5472N power supplies
/**
 * [Reference](https://www.caen.it/products/dt5472/)
 *
 * Note: This is the negative output voltage variation.
 */
class DT5472NPs : public DT54xxPs {
 public:
    DT5472NPs(const std::string& name);
    ~DT5472NPs() = default;
};

#endif  // DT5472NPS_H
