#include "DT54xxPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"
#include "ScopeLock.h"
#include "TextSerialCom.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(DT54xxPs)

DT54xxPs::DT54xxPs(const std::string& name,
                   const std::vector<std::string>& models, Polarity output,
                   double imaxl)
    : IPowerSupply(name, models), m_output(output), m_imaxl(imaxl) {}

void DT54xxPs::reset() {
    command("SET", "OFF");
    command("SET", "BDCLR");

    std::string result = identify();
    if (result.empty())
        throw std::runtime_error("No communication after reset.");
}

std::string DT54xxPs::identify() {
    std::string idn = command("MON", "BDNAME");
    return idn;
}

bool DT54xxPs::ping() {
    std::string result = command("MON", "BDNAME");
    return !result.empty();
}

void DT54xxPs::checkCompatibilityList() {
    IPowerSupply::checkCompatibilityList();

    Polarity pol = polarity();
    if (pol != m_output) throw std::runtime_error("Wrong polarity detected");
}

void DT54xxPs::turnOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    ScopeLock lock(m_com);
    command("SET", "ON");
    waitRamp();
}

void DT54xxPs::turnOff(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    ScopeLock lock(m_com);
    command("SET", "OFF");
    waitRamp();
}

void DT54xxPs::setCurrentLevel(double cur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    cur = checkPolarity(cur);

    // Set the optimal range for the requested maximum current
    if (cur < m_imaxl)  // set low range for small currents
        setIMonRange(IMonRange::Low, channel);
    else  // use high range for large currents
        setIMonRange(IMonRange::High, channel);

    // Set the maximum current
    command("SET", "ISET", std::to_string(cur * 1e6));
}

double DT54xxPs::getCurrentLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return convertPolarity(std::stod(command("MON", "ISET")) / 1e6);
}

void DT54xxPs::setCurrentProtect(double maxcur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    setCurrentLevel(convertPolarity(maxcur), channel);
}

double DT54xxPs::getCurrentProtect(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::fabs(getCurrentLevel(channel));
}

double DT54xxPs::measureCurrent(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return convertPolarity(std::stod(command("MON", "IMON")) / 1e6);
}

void DT54xxPs::setVoltageLevel(double volt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    command("SET", "VSET", std::to_string(checkPolarity(volt)));
    waitRamp();
}

double DT54xxPs::getVoltageLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return convertPolarity(std::stod(command("MON", "VSET")));
}

void DT54xxPs::setVoltageProtect(double maxvolt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    setVoltageLevel(convertPolarity(maxvolt), channel);
}

double DT54xxPs::getVoltageProtect(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::fabs(getVoltageLevel(channel));
}

double DT54xxPs::measureVoltage(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return convertPolarity(std::stod(command("MON", "VMON")));
}

uint16_t DT54xxPs::status(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stoi(command("MON", "STAT")) & 0xFFFF;
}

DT54xxPs::Polarity DT54xxPs::polarity(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    std::string polstr = command("MON", "POLARITY");
    if (polstr == "-")
        return Polarity::Negative;
    else
        return Polarity::Positive;
}

void DT54xxPs::setIMonRange(IMonRange range, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    command("SET", "IMRANGE", (range == IMonRange::Low) ? "LOW" : "HIGH");
}

DT54xxPs::IMonRange DT54xxPs::getIMonRange(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return (command("MON", "IMRANGE") == "LOW") ? IMonRange::Low
                                                : IMonRange::High;
}

void DT54xxPs::waitRamp(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    do {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> ramping: " << measureVoltage(channel) << "V";
    } while (status(channel) & (Status::RampingUp | Status::RampingDown));
}

std::string DT54xxPs::command(const std::string& cmd, const std::string& par,
                              const std::string& value) {
    // Build command
    std::string tosend = "$CMD:" + cmd + ",PAR:" + par;
    if (!value.empty()) tosend += ",VAL:" + value;

    // Send command and receive response
    std::string resp = m_com->sendreceive(tosend);

    // Parse response
    if (resp.empty()) throw "DT54xx: No response :(";

    std::string retvalue;
    std::string cmdvalue;

    std::string token;
    std::stringstream ss(resp);
    while (std::getline(ss, token, ',')) {
        size_t seppos = token.find(':');
        if (seppos == std::string::npos) continue;  // Not a valid part
        if (token.substr(0, seppos) == "VAL") {     // This is the value part!
            retvalue = token.substr(seppos + 1);
        } else if (token.substr(0, seppos) ==
                   "#CMD") {  // This is the value part!
            cmdvalue = token.substr(seppos + 1);
        }
    }

    if (cmdvalue.empty())
        throw std::runtime_error("DT54xx: No CMD in return statement :(");

    if (cmdvalue == "ERR")
        throw std::runtime_error("DT54xx: CMD shows an error :(");

    return retvalue;
}

double DT54xxPs::checkPolarity(double input) {
    if (m_output == Polarity::Negative && input > 0)
        throw std::runtime_error(
            "Specified positive output value for a power supply that only "
            "supports negative output.");
    if (m_output == Polarity::Positive && input < 0)
        throw std::runtime_error(
            "Specified negative output value for a power supply that only "
            "supports positive output.");

    return std::fabs(input);
}

double DT54xxPs::convertPolarity(double value) {
    return (m_output == Polarity::Negative) ? -value : value;
}
