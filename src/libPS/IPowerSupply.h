#ifndef IPOWERSUPPLY_H
#define IPOWERSUPPLY_H

#include <nlohmann/json.hpp>
#include <string>

#include "ICom.h"

/** Generic power supply interface.

    Functions that should be optional but expected to be generic enough
    to end up in this class should not be virtual pure and instead should
    have a dummy implementation in IPowerSupply.cpp throwing an error that such
    functionality is not implemented.

    The hardware configuration is given via a json object and can be harware
    specific.
    A typical workflow would be:
      IPowerSupply *ps;
      ps->setConfiguration(specificPSConfigJSON);
      ps->setCom(...);
      ps->checkCompatibilityList();
      ps->turnOn();
      ps->setVoltage(1.2, 1); //to change the voltage
      //....
      ps->turnOff();

    More commonly the power supply will be retrieved from the equipment
   registry, which will take care of its configuration and connection already,
   e.g.: EquipConf hw(completeConfigJSON); td::shared_ptr<IPowerSupply> PS =
   hw.getPowerSupply("PS"); ps->turnOn(); ps->setVoltage(1.2, 1); //to change
   the voltage
      //....
      ps->turnOff();
 */
class IPowerSupply {
 public:
    /** Constructor.
     * Note: do NOT do any active command in the constructor. Only initialize
     * local variables.
     * @param name Name of the power supply
     * @param models List of tested models (empty means no check is performed)
     */
    IPowerSupply(const std::string& name,
                 const std::vector<std::string>& models = {});

    /** Set communication object
     *
     * The object is initialized if not already.
     *
     * Communication with the power supply is checked
     * after setting the communication object. If the device does not
     * respond to `ping()`, an exception is thrown.
     *
     * \param com ICom instance for communicating with the power supply
     */
    virtual void setCom(std::shared_ptr<ICom> com);

    /** Store JSON hardware configuration
     * @param config JSON configuration for the given power supply only
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** \name Communication
     * @{
     */

    /** \brief Check if power-supply responds
     *
     * Send a message to the power suppy that does not
     * change the internal state to check if communicaton
     * is working.
     *
     * \return True if communication works, false otherwise
     */
    virtual bool ping() = 0;

    /*
     * Check that the device model is supported by checking that
     * model identifier of the connected power supply contains
     * one of the supported models for that power supply
     * Throws exception if compatibility is not found
     * If the list of model identifiers is empty, no check is performed
     */
    virtual void checkCompatibilityList();

    /** @} */

    /** \name Power Supply Control
     * @{
     */

    /** \brief Execute reset of the device  */
    virtual void reset() = 0;

    /** Return list of supported models. **/
    virtual std::vector<std::string> getListOfModels();

    /** Returns the model of the power supply connected.
     * PS classes should return an empty string if this functionality does not
     *exist
     **/
    virtual std::string identify() = 0;

    /** Turn on power supply.
     * @param channel channel, if any
     */
    virtual void turnOn(unsigned channel = 0) = 0;

    /** Turn on all channels of the power supply.
     */
    virtual void turnOnAll();

    /** \brief Turn off power supply
     * @param channel channel, if any
     */
    virtual void turnOff(unsigned channel = 0) = 0;

    /** Turn off all channels of the power supply.
     */
    virtual void turnOffAll();

    /** Check whether an output channel is enabled.
     */
    virtual bool isOn(unsigned channel = 0);

    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    /** \brief Ramp current of PS
     *
     * Slowly changes the current level every second in discrete
     * `rate` steps until `cur` is reached. The number of steps
     * is determined by initial current measurement.
     *
     * @param cur current [A]
     * @param rate absolute rate of current change [A/s]
     * @param channel channel (if any)
     */
    virtual void rampCurrentLevel(double cur, double rate,
                                  unsigned channel = 0);

    /** \brief Set current of PS
     * @param cur current [A]
     * @param channel channel (if any)
     */
    virtual void setCurrentLevel(double cur, unsigned channel = 0) = 0;

    /** \brief Get set current of PS
     * @param channel channel (if any)
     * @return current, convert to [A]
     */
    virtual double getCurrentLevel(unsigned channel = 0) = 0;

    /** \brief Set current protection [optional]
     * @param cur maximum current [A]
     * @param channel channel (if any)
     */
    virtual void setCurrentProtect(double cur, unsigned channel = 0);

    /** \brief Get current protection [optional]
     * @param channel channel (if any)
     * @return current, convert to [A]
     */
    virtual double getCurrentProtect(unsigned channel = 0);

    /** \brief Get measured current
     * @param channel channel (if any)
     */
    virtual double measureCurrent(unsigned channel = 0) = 0;

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    /** \brief Ramp voltage of PS
     *
     * Slowly changes the voltage level every second in discrete
     * `rate` steps until `volt` is reached. The number of steps
     * is determined by initial voltage measurement.
     *
     * @param volt taget voltage [V]
     * @param rate absolute rate of voltage change [V/s]
     * @param channel channel (if any)
     */
    virtual void rampVoltageLevel(double cur, double rate,
                                  unsigned channel = 0);

    /** \brief Set voltage of PS
     * @param volt voltage [V]
     * @param channel channel (if any)
     */
    virtual void setVoltageLevel(double volt, unsigned channel = 0) = 0;

    /** \brief Get set voltage of PS.
     * @param channel channel (if any)
     * @return voltage, convert to [V]
     */
    virtual double getVoltageLevel(unsigned channel = 0) = 0;

    /** \brief Set voltage protection
     * @param volt maximum current [A]
     * @param channel channel (if any)
     */
    virtual void setVoltageProtect(double volt, unsigned channel = 0);

    /** \brief Get voltage protection [optional]
     * @param channel channel (if any)
     * @return voltage, convert to [V]
     */
    virtual double getVoltageProtect(unsigned channel = 0);

    /** \brief Get measured voltage
     * @param channel channel (if any)
     */
    virtual double measureVoltage(unsigned channel = 0) = 0;

    /** @} */

 protected:
    /** Communication */
    std::shared_ptr<ICom> m_com = nullptr;

    /** Store device configuration name */
    std::string m_name;

    std::vector<std::string> m_models;
};

#endif
