#include "Keithley24XX.h"

#include <algorithm>
#include <thread>

#include "Logger.h"
#include "ScopeLock.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Keithley24XX)

Keithley24XX::Keithley24XX(const std::string& name)
    : IPowerSupply(name, {"2410", "2400", "2401"}) {}

void Keithley24XX::setConfiguration(const nlohmann::json& config) {
    for (const auto& kv : config.items()) {
        if (kv.key() == "autoVoltageRange") {
            m_autoVoltageRange = kv.value();
        } else if (kv.key() == "autoCurrentRange") {
            m_autoCurrentRange = kv.value();
        }
    }

    IPowerSupply::setConfiguration(config);
}

bool Keithley24XX::ping() {
    std::string result = m_com->sendreceive("*IDN?");
    return !result.empty();
}

void Keithley24XX::reset() {
    m_com->send("OUTPUT OFF");
    m_com->send("*RST");
    m_com->send(":TRIGGER:COUNT 1");
    m_com->send(":FORMAT:ELEMENTS TIME,VOLT,CURR");
    m_com->send(":SYST:BEEP:STAT OFF");  // disable beep

    if (!ping()) throw std::runtime_error("No communication after reset.");
}

void Keithley24XX::turnOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OUTPUT ON");
}

void Keithley24XX::turnOff(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OUTPUT OFF");
}

bool Keithley24XX::isOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");
    // search for substring in case value is returned with a terminator
    return m_com->sendreceive(":OUTPUT?").find("1") == 0;
}

std::string Keithley24XX::identify() {
    std::string idn = m_com->sendreceive("*IDN?");
    return idn;
}

void Keithley24XX::autoCurrentRange(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_autoCurrentRange = true;
}

void Keithley24XX::setCurrentRange(double cur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_autoCurrentRange = false;
    m_com->send(":SOURCE:CURR:RANGE " + to_string_with_precision(cur));
}

void Keithley24XX::setCurrentLevel(double cur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    ScopeLock lock(m_com);
    m_com->send(":SOURCE:FUNC CURR");
    if (m_autoCurrentRange)
        m_com->send(":SOURCE:CURR:RANGE " + to_string_with_precision(cur));
    m_com->send(":SOURCE:CURR " + to_string_with_precision(cur));
}

double Keithley24XX::getCurrentLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive(":SOURCE:CURR?"));
}

void Keithley24XX::setCurrentProtect(double maxcur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send(":SENSE:CURR:PROTECTION " + std::to_string(maxcur));
    m_com->send(":SENSE:CURR:RANGE " + std::to_string(maxcur));
}

double Keithley24XX::getCurrentProtect(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive(":SENSE:CURR:PROTECTION?"));
}

double Keithley24XX::measureCurrent(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    ScopeLock lock(m_com);

    if (!isOn(channel)) return 0.;

    m_com->send(":FORMAT:ELEMENTS CURR");
    return std::stod(m_com->sendreceive(":READ?"));
}

void Keithley24XX::autoVoltageRange(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_autoCurrentRange = true;
}

void Keithley24XX::setVoltageRange(double volt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_autoCurrentRange = false;
    m_com->send(":SOURCE:VOLT:RANGE " + to_string_with_precision(volt));
}

void Keithley24XX::setVoltageLevel(double volt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    ScopeLock lock(m_com);
    m_com->send(":SOURCE:FUNC VOLT");
    if (m_autoVoltageRange)
        m_com->send(":SOURCE:VOLT:RANGE " + to_string_with_precision(volt));
    m_com->send(":SOURCE:VOLT " + to_string_with_precision(volt));
}

double Keithley24XX::getVoltageLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive(":SOURCE:VOLT?"));
}

void Keithley24XX::setVoltageProtect(double maxvolt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send(":SENSE:VOLT:RANGE " + std::to_string(maxvolt));
    m_com->send(":SENSE:VOLT:PROTECTION " + std::to_string(maxvolt));
}

double Keithley24XX::getVoltageProtect(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive(":SENSE:VOLT:PROTECTION?"));
}

double Keithley24XX::measureVoltage(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    ScopeLock lock(m_com);

    if (!isOn(channel)) return 0.;

    m_com->send(":FORMAT:ELEMENTS VOLT");
    return std::stod(m_com->sendreceive(":READ?"));
}
