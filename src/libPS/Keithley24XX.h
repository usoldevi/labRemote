#ifndef KEITHLEY24XX_H
#define KEITHLEY24XX_H

#include <chrono>
#include <iomanip>
#include <memory>
#include <sstream>
#include <string>

#include "IPowerSupply.h"
#include "SerialCom.h"

/** \brief Keithley 24xx
 *
 * Implementation for the Keithley 2410 power supply.
 *
 * [Programming
 * Manual](https://download.tek.com/manual/2400S-900-01_K-Sep2011_User.pdf)
 *
 * Other Keithley 24xx power supplies might be supported too, but have
 * not been checked.
 *
 * # Output Level Auto-Ranging
 * The Keithley 24xx requires a range to be set on the expected output
 * level (both current or voltage). Setting the auto-ranging option
 * causes this class to pick the best range depending on the requested
 * voltage level.
 *
 * There are seperate settings to control whether auto-ranging is enabled
 * for current and voltage source modes. The default value is to enable
 * auto-ranging for convenience. Setting the range manually via
 * `setCurrentRange` or `setVoltageRange` causes the auto-ranging to be
 * disabled. It can also be set at configuration via `autoCurrentRange` and
 * `autoVoltageRange` JSON options.
 *
 * The sense ranges are always automatically set to the best choice on the
 * requested compliance.
 */
class Keithley24XX : public IPowerSupply {
 public:
    Keithley24XX(const std::string& name);
    ~Keithley24XX() = default;

    //! Keithley24xx specific hardware configuration
    /**
     * Valid keys:
     *  - `autoVoltageRange`: Boolean indicating whether to auto-range the
     * output voltage level
     *  - `autoCurrentRange`: Boolean indicating whether to auto-range the
     * output current level
     *
     * @param config JSON configuration for the given power supply only
     */
    virtual void setConfiguration(const nlohmann::json& config);

    /** \name Communication
     * @{
     */

    virtual bool ping();

    virtual std::string identify();

    /** @} */

    /** \name Power Supply Control
     * @{
     */

    virtual void reset();
    virtual void turnOn(unsigned channel);
    virtual void turnOff(unsigned channel);
    virtual bool isOn(unsigned channel);

    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    //! @brief Enable auto-ranging for current output
    /**
     * @param channel channel (if any)
     */
    virtual void autoCurrentRange(unsigned channel = 0);

    //! @brief Set current range
    /**
     * Set the range of the Keithely current output using
     * the `:SOURCE:CURR:RANGE` command.
     *
     * Calling this method disables auto-ranging.
     *
     * @param cur maximum expected current [A]
     * @param channel channel (if any)
     */
    virtual void setCurrentRange(double cur, unsigned channel = 0);

    virtual void setCurrentLevel(double cur, unsigned channel = 0);
    virtual double getCurrentLevel(unsigned channel = 0);
    virtual void setCurrentProtect(double maxcur, unsigned channel = 0);
    virtual double getCurrentProtect(unsigned channel = 0);
    virtual double measureCurrent(unsigned channel = 0);

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    //! @brief Enable auto-ranging for voltage output
    /**
     * @param channel channel (if any)
     */
    virtual void autoVoltageRange(unsigned channel = 0);

    //! @brief Set voltage range
    /**
     * Set the range of the Keithely current output using
     * the `:SOURCE:VOLT:RANGE` command.
     *
     * Calling this method disables auto-ranging.
     *
     * @param cur maximum expected voltage [V]
     * @param channel channel (if any)
     */
    virtual void setVoltageRange(double volt, unsigned channel = 0);

    virtual void setVoltageLevel(double volt, unsigned channel = 0);
    virtual double getVoltageLevel(unsigned channel = 0);
    virtual void setVoltageProtect(double maxvolt, unsigned channel = 0);
    virtual double getVoltageProtect(unsigned channel = 0);
    virtual double measureVoltage(unsigned channel = 0);

    /** @} */

 private:
    /**
     * Convert number to scientific notation
     *
     * \param a_value number to convert
     * \param n number of digitcs after the decimal
     *
     * \return x.xxxxxxEyy
     */
    template <typename T>
    std::string to_string_with_precision(const T a_value, const int n = 6) {
        std::ostringstream out;
        out << std::setprecision(n) << a_value;
        return out.str();
    }

    /** \name Settings
     * @{
     */

    //! Auto-range the output voltage level
    bool m_autoVoltageRange = true;

    //! Auto-range the output current level
    bool m_autoCurrentRange = true;
};

#endif
