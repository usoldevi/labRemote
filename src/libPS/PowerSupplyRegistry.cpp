#include "PowerSupplyRegistry.h"

#include <iostream>

namespace EquipRegistry {

typedef ClassRegistry<IPowerSupply, std::string> RegistryPS;

static RegistryPS& registry() {
    static RegistryPS instance;
    return instance;
}

bool registerPowerSupply(
    const std::string& model,
    std::function<std::shared_ptr<IPowerSupply>(const std::string&)> f) {
    return registry().registerClass(model, f);
}

std::shared_ptr<IPowerSupply> createPowerSupply(const std::string& model,
                                                const std::string& name) {
    auto result = registry().makeClass(model, name);
    if (result == nullptr) {
        std::cout << "No Power Supply (IPowerSupply) of type " << model
                  << ", matching the name '" << name << "' found\n";
    }
    return result;
}

std::vector<std::string> listPowerSupply() { return registry().listClasses(); }

}  // namespace EquipRegistry
