#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "ClassRegistry.h"
#include "IPowerSupply.h"

// Configuration helpers and registry for specific PS implementations
namespace EquipRegistry {
/** Register new PS type into registry */
bool registerPowerSupply(
    const std::string& model,
    std::function<std::shared_ptr<IPowerSupply>(const std::string&)> f);

/** Get new instance of given power supply type */
std::shared_ptr<IPowerSupply> createPowerSupply(const std::string& model,
                                                const std::string& name);

/** List available Ps types */
std::vector<std::string> listPowerSupply();
}  // namespace EquipRegistry

#define REGISTER_POWERSUPPLY(model)                                       \
    static bool _registered_##model = EquipRegistry::registerPowerSupply( \
        #model,                                                           \
        std::function<std::shared_ptr<IPowerSupply>(const std::string&)>( \
            [](const std::string& name) {                                 \
                return std::shared_ptr<IPowerSupply>(new model(name));    \
            }));
