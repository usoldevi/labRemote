#include "RS_HMP4040.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RS_HMP4040)

RS_HMP4040::RS_HMP4040(const std::string& name)
    : SCPIPs(name, {"HMP4040"}, 4) {}
