#ifndef SCPIPS_H
#define SCPIPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"
#include "SerialCom.h"

/**
 * Base implemetation for Agilent/Keysight power supplies that share
 * a very similar command set.
 *
 * Options are possible to add additional for error checking on
 *  - model compatibility (list of strings partially matched against *idn?,
 * empty means no check is performed)
 *  - channel checking (maximum number of channels, 0 means no check is
 * performed)
 */
class SCPIPs : public IPowerSupply {
 public:
    /**
     * @param name Name of the power supply
     * @param models List of supported models (empty means no check is
     * performed)
     * @param maxChannels Maximum number of channels in the power supply (0
     * means no maximum)
     */
    SCPIPs(const std::string& name, std::vector<std::string> models = {},
           unsigned maxChannels = 0);
    ~SCPIPs() = default;

    /** \name Communication
     * @{
     */

    virtual bool ping();

    /** @} */

    /** \name Power Supply Control
     * @{

    /**
     * Returns the model identifier of the PS connected
     *
     */
    virtual std::string identify();

    virtual void reset();

    /** Turn on all channels
     *
     * Many Agilent power supplies don't have channel-specific enable
     * control.
     *
     * @param channel ignored
     */
    virtual void turnOn(unsigned channel);

    /** Turn off all channels
     *
     * Many Agilent power supplies don't have channel-specific enable
     * control.
     *
     * @param channel ignored
     */
    virtual void turnOff(unsigned channel);

    /**
     * Returns 1 if the channel's output is on, and 0 otherwise
     *
     */
    virtual bool isOn(unsigned channel);

    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    virtual void setCurrentLevel(double cur, unsigned channel = 0);
    virtual double getCurrentLevel(unsigned channel = 0);
    virtual void setCurrentProtect(double maxcur, unsigned channel = 0);
    virtual double getCurrentProtect(unsigned channel = 0);
    virtual double measureCurrent(unsigned channel = 0);

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    virtual void setVoltageLevel(double volt, unsigned channel = 0);
    virtual double getVoltageLevel(unsigned channel = 0);
    virtual void setVoltageProtect(double maxvolt, unsigned channel = 0);
    virtual double getVoltageProtect(unsigned channel = 0);
    virtual double measureVoltage(unsigned channel = 0);

    /** @} */

 protected:
    //\brief Send power supply command and wait for operation complete.
    /**
     * Sends `cmd` to the power supply followed up `*OPC?`. Then blocks until
     * a response is received. This prevents the program from terminating
     * before the program is processed.
     *
     * An error is thrown if no response to `*OPC?` is seen.
     *
     * \param cmd Power supply command to transmit.
     */
    void send(const std::string& cmd);

    //! \brief Add channel set to power supply send
    /**
     * Sends `":INST:NSEL "+channel` before the actual command. This is
     * done without injecting `*OPC?`.
     *
     * The rest is achieved using `SCIPPs::send(constd std::string& cmd)`.
     *
     * \param cmd Power supply command to transmit.
     * \param channel Target channel
     */
    void send(const std::string& cmd, unsigned channel);

    //\brief Send power supply command and return response.
    /**
     * Wrapper around `ICom::sendreceive`.
     *
     * \param cmd Power supply command to transmit.
     *
     * \return Parameter response.
     */
    std::string sendreceive(const std::string& cmd);

    //! \brief Add channel set to power supply sendreceive
    /**
     * Sends `":INST:NSEL "+channel` before the actual command. This is
     * done without injecting `*OPC?`.
     *
     * The rest is achieved using `SCIPPs::sendreceive(constd std::string&
     * cmd)`.
     *
     * \param cmd Power supply command to transmit.
     * \param channel Target channel
     *
     * \return Response for `cmd`
     */
    std::string sendreceive(const std::string& cmd, unsigned channel);

 private:
    /** PS limitations @{ */

    //! Maximum number of channels, 0 means unlimited
    uint32_t m_maxChannels = 0;

    /** Set channel for following commands
     *
     * In-range check is performed.
     */
    void setChannel(unsigned channel);

    /** @} */
};

#endif
