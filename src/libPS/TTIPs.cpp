#include "TTIPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTIPs)

TTIPs::TTIPs(const std::string& name, std::vector<std::string> models,
             unsigned nChannels)
    : IPowerSupply(name, models), m_nchan(nChannels) {}

bool TTIPs::ping() {
    std::string result = m_com->sendreceive("*IDN?");
    return !result.empty();
}

void TTIPs::reset() {
    m_com->send("OPALL 0");
    m_com->send("*RST");

    if (!ping()) throw std::runtime_error("No communication after reset.");
}

std::string TTIPs::identify() {
    std::string idn = m_com->sendreceive("*IDN?");
    return idn;
}

void TTIPs::turnOn(unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    m_com->send("OP" + std::to_string(channel) + " 1");
}

void TTIPs::turnOnAll() { m_com->send("OPALL 1"); }

void TTIPs::turnOff(unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    m_com->send("OP" + std::to_string(channel) + " 0");
}

void TTIPs::turnOffAll() { m_com->send("OPALL 0"); }

bool TTIPs::isOn(unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));
    // search for substring because value is returned with a <RESPONSE MESSAGE
    // TERMINATOR>
    return m_com->sendreceive("OP" + std::to_string(channel) + "?").find("1") ==
           0;
}

void TTIPs::setCurrentLevel(double cur, unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    m_com->send("I" + std::to_string(channel) + " " + std::to_string(cur));
}

double TTIPs::getCurrentLevel(unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    std::string repl = m_com->sendreceive("I" + std::to_string(channel) + "?");
    return std::stod(repl.substr(("I" + std::to_string(channel) + " ").length(),
                                 repl.length()));
}

void TTIPs::setCurrentProtect(double maxcur, unsigned channel) {
    setCurrentLevel(maxcur, channel);
}

double TTIPs::getCurrentProtect(unsigned channel) {
    return getCurrentLevel(channel);
}

double TTIPs::measureCurrent(unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    std::string repl = m_com->sendreceive("I" + std::to_string(channel) + "O?");
    size_t apos = repl.find("A");
    if (apos != std::string::npos)
        repl = repl.substr(0, apos);  // skip "A" at end
    return std::stod(repl);
}

void TTIPs::setVoltageLevel(double volt, unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));
    m_com->send("V" + std::to_string(channel) + " " + std::to_string(volt));
}

double TTIPs::getVoltageLevel(unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    std::string repl = m_com->sendreceive("V" + std::to_string(channel) + "?");
    return std::stod(repl.substr(("V" + std::to_string(channel) + " ").length(),
                                 repl.length()));
}

void TTIPs::setVoltageProtect(double maxvolt, unsigned channel) {
    setVoltageLevel(maxvolt, channel);
}

double TTIPs::getVoltageProtect(unsigned channel) {
    return getVoltageLevel(channel);
}

double TTIPs::measureVoltage(unsigned channel) {
    if (channel < 1 || channel > m_nchan)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    std::string repl = m_com->sendreceive("V" + std::to_string(channel) + "O?");
    size_t vpos = repl.find("V");
    if (vpos != std::string::npos)
        repl = repl.substr(0, vpos);  // skip "V" at end
    return std::stod(repl);
}
