#ifndef TTIPS_H
#define TTIPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"
#include "SerialCom.h"

/** \brief TTi PSUs
 *
 * Implementation for TTi power supplies.
 *
 */
class TTIPs : public IPowerSupply {
 public:
    TTIPs(const std::string& name, std::vector<std::string> models = {},
          unsigned nChannels = 0);
    ~TTIPs() = default;

    /** \name Communication
     * @{
     */

    virtual bool ping();

    virtual std::string identify();

    /** @} */

    /** \name Power Supply Control
     * @{
     */

    virtual void reset();
    virtual void turnOn(unsigned channel);
    virtual void turnOnAll();
    virtual void turnOff(unsigned channel);
    virtual void turnOffAll();
    virtual bool isOn(unsigned channel);

    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    virtual void setCurrentLevel(double cur, unsigned channel = 0);
    virtual double getCurrentLevel(unsigned channel = 0);
    virtual void setCurrentProtect(double maxcur, unsigned channel = 0);
    virtual double getCurrentProtect(unsigned channel = 0);
    virtual double measureCurrent(unsigned channel = 0);

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    virtual void setVoltageLevel(double volt, unsigned channel = 0);
    virtual double getVoltageLevel(unsigned channel = 0);
    virtual void setVoltageProtect(double maxvolt, unsigned channel = 0);
    virtual double getVoltageProtect(unsigned channel = 0);
    virtual double measureVoltage(unsigned channel = 0);

    /** @} */

 private:
    unsigned m_nchan;
};

#endif
