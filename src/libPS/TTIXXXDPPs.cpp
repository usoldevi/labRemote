#include "TTIXXXDPPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTIXXXDPPs)

TTIXXXDPPs::TTIXXXDPPs(const std::string& name)
    : TTIPs(name, {"PL303QMD-P", "CPX400DP", "CPX200DP"}, 2) {}
