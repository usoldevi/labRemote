#include "Tenma72133XX.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Tenma72133XX)

Tenma72133XX::Tenma72133XX(const std::string& name)
    : IPowerSupply(name, {"72-13350", "72-13360"}) {}

bool Tenma72133XX::ping() {
    std::string result = m_com->sendreceive("*IDN?");
    return !result.empty();
}

//
void Tenma72133XX::reset() {
    m_com->send("OUT:0");
    m_com->send("BEEP:1");   // enable beep
    m_com->send("OCP:OFF");  // disable OCP
    m_com->send("OVP:OFF");  // disable OVP

    if (!ping()) throw std::runtime_error("No communication after reset.");
}

void Tenma72133XX::turnOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OUT:1");
}

void Tenma72133XX::turnOff(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OUT:0");
}

bool Tenma72133XX::isOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stoi(m_com->sendreceive("OUT?"));
}

// POWER SUPPLY status: 8 bits in the following format
// Bit Description
// 0 0=CC mode, 1=CV mode
// 1 Output 0=Off, 1=On
// 2 V/C priority
// 3 N/A
// 4 Beep 0=Off, 1=On
// 5 Lock 0=Lock, 1=Unlock
// 6 OVP status
// 7 OCP status

bool Tenma72133XX::isOCP(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");
    std::string status = m_com->sendreceive("STATUS?");
    if (status.length() != 1) throw std::runtime_error("STATUS undefined");
    int ocp = (status[0] >> 7) & 1;
    logger(logDEBUG) << "string successfully reduced to 8-bit set, OCP: "
                     << ocp;
    return ocp;
}

bool Tenma72133XX::isOVP(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");
    std::string status = m_com->sendreceive("STATUS?");
    if (status.length() != 1) throw std::runtime_error("STATUS undefined");
    int ovp = (status[0] >> 7) & 1;
    logger(logDEBUG) << "string successfully reduced to 8-bit set, OVP: "
                     << ovp;
    return ovp;
}

std::bitset<8> Tenma72133XX::getStatus(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");
    std::string status = m_com->sendreceive("STATUS?");
    if (status.length() != 1) throw std::runtime_error("STATUS undefined");
    logger(logDEBUG) << std::bitset<8>(status[0]);

    if ((status[0]) & 1)
        logger(logDEBUG) << "STATUS0: Mode: CV";
    else
        logger(logDEBUG) << "STATUS0: Mode: CC";

    if ((status[0] >> 1) & 1)
        logger(logDEBUG) << "STATUS1: Output: on";
    else
        logger(logDEBUG) << "STATUS1: Output: off";

    if ((status[0] >> 2) & 1)
        logger(logDEBUG) << "STATUS2: Priority: current";
    else
        logger(logDEBUG) << "STATUS2: Priority: voltage";

    logger(logDEBUG) << "STATUS3: N/A";

    if ((status[0] >> 4) & 1)
        logger(logDEBUG) << "STATUS4: Beep: on";
    else
        logger(logDEBUG) << "STATUS4: Beep: off";

    if ((status[0] >> 5) & 1)
        logger(logDEBUG) << "STATUS5: Lock: on";
    else
        logger(logDEBUG) << "STATUS5: Lock: off";

    if ((status[0] >> 6) & 1)
        logger(logDEBUG) << "STATUS6: OVP: on";
    else
        logger(logDEBUG) << "STATUS6: OVP: off";

    if ((status[0] >> 7) & 1)
        logger(logDEBUG) << "STATUS7: OCP: on";
    else
        logger(logDEBUG) << "STATUS7: OCP: off";

    return std::bitset<8>(status[0]);
}

std::string Tenma72133XX::identify() { return m_com->sendreceive("*IDN?"); }

void Tenma72133XX::beepOn() { m_com->send("BEEP:1"); }

void Tenma72133XX::beepOff() { m_com->send("BEEP:0"); }

void Tenma72133XX::saveSetting(unsigned mem) {
    m_com->send("SAV:" + std::to_string(mem));
}

void Tenma72133XX::recallSetting(unsigned mem) {
    m_com->send("RCL:" + std::to_string(mem));
}

void Tenma72133XX::setCurrentLevel(double cur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("ISET:" + to_string_with_precision(cur));
}

double Tenma72133XX::getCurrentLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive("ISET?"));
}

void Tenma72133XX::setCurrentProtect(double maxcur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OCP:" + to_string_with_precision(maxcur));
    m_com->send("OCP:ON");
}

double Tenma72133XX::getCurrentProtect(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive("OCP?"));
}

void Tenma72133XX::OCPOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OCP:ON");
}

void Tenma72133XX::OCPOff(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OCP:OFF");
}

void Tenma72133XX::setCurrentSlope(double slope, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");
    if (slope > 990000)
        throw std::runtime_error(
            "Invalid slope. Max slope is 99A/100us or 990000A/s.");
    m_com->send("ISLOPE:" + to_string_with_precision(slope / 10000.));
}

double Tenma72133XX::getCurrentSlope(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive("ISLOPE?")) * 10000.;
}

double Tenma72133XX::measureCurrent(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    if (!isOn(channel)) return 0.;
    return std::stod(m_com->sendreceive("IOUT?"));
}

void Tenma72133XX::setCurrentPriority(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("PRIORITY:1");
}

void Tenma72133XX::setVoltageLevel(double volt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("VSET:" + to_string_with_precision(volt));
}

double Tenma72133XX::getVoltageLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive("VSET?"));
}

void Tenma72133XX::setVoltageProtect(double maxvolt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OVP:" + to_string_with_precision(maxvolt));
    m_com->send("OVP:ON");
}

void Tenma72133XX::OVPOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OVP:ON");
}

void Tenma72133XX::OVPOff(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OVP:OFF");
}

double Tenma72133XX::getVoltageProtect(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive("OVP?"));
}

void Tenma72133XX::setVoltageSlope(double slope, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");
    if (slope > 990000)
        throw std::runtime_error(
            "Invalid slope. Max slope is 99V/100us or 990000V/s.");

    m_com->send("VSLOPE:" + to_string_with_precision(slope / 10000.));
}

double Tenma72133XX::getVoltageSlope(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(m_com->sendreceive("VSLOPE?")) * 10000.;
}

double Tenma72133XX::measureVoltage(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    if (!isOn(channel)) return 0.;

    return std::stod(m_com->sendreceive("VOUT?"));
}

void Tenma72133XX::setVoltagePriority(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("PRIORITY:0");
}
