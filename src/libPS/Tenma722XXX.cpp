#include "Tenma722XXX.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Tenma722XXX)

Tenma722XXX::Tenma722XXX(const std::string& name)
    : IPowerSupply(name, {"72-2535", "72-2540", "72-2545", "72-2550", "72-2930",
                          "72-2940"}) {}

bool Tenma722XXX::ping() {
    std::string result = m_com->sendreceive("*IDN?");
    return !result.empty();
}

//
void Tenma722XXX::reset() {
    m_com->send("OUT0");
    m_com->send("BEEP0");  // disable beep

    if (!ping()) throw std::runtime_error("No communication after reset.");
}

void Tenma722XXX::turnOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OUT1");
}

void Tenma722XXX::turnOff(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OUT0");
}

// POWER SUPPLY status: 8 bits in the following format
// Bit Description
// 0 CH1 0=CC mode, 1=CV mode
// 1 CH2 0=CC mode, 1=CV mode
// 2, 3 Tracking 00=Independent, 01=Tracking series,11=Tracking parallel
// 4 Beep 0=Off, 1=On
// 5 Lock 0=Lock, 1=Unlock
// 6 Output 0=Off, 1=On
// 7 N/A N/A

bool Tenma722XXX::isOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");
    std::string status = m_com->sendreceive("STATUS?");
    if (status.length() != 1) throw std::runtime_error("STATUS undefined");
    int output = (status[0] >> 6) & 1;
    logger(logDEBUG) << "string successfully reduced to 8-bit set, isOn: "
                     << output;
    return output;
}

std::string Tenma722XXX::identify() {
    std::string idn = m_com->sendreceive("*IDN?");
    logger(logDEBUG) << idn;
    return idn;
}

void Tenma722XXX::beepOn() { m_com->send("BEEP1"); }

void Tenma722XXX::beepOff() { m_com->send("BEEP0"); }

void Tenma722XXX::saveSetting(unsigned mem) {
    m_com->send("SAV" + std::to_string(mem));
}

void Tenma722XXX::recallSetting(unsigned mem) {
    m_com->send("RCL" + std::to_string(mem));
}

void Tenma722XXX::setCurrentLevel(double cur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("ISET" + std::to_string(channel) + ":" +
                to_string_with_precision(cur));
}

double Tenma722XXX::getCurrentLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(
        m_com->sendreceive("ISET" + std::to_string(channel) + "?"));
}

void Tenma722XXX::setCurrentProtect(double maxcur, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OCP1");
}

double Tenma722XXX::measureCurrent(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    if (!isOn(channel)) return 0.;
    return std::stod(
        m_com->sendreceive("IOUT" + std::to_string(channel) + "?"));
}

void Tenma722XXX::setVoltageLevel(double volt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("VSET" + std::to_string(channel) + ":" +
                to_string_with_precision(volt));
}

double Tenma722XXX::getVoltageLevel(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return std::stod(
        m_com->sendreceive("VSET" + std::to_string(channel) + "?"));
}

void Tenma722XXX::setVoltageProtect(double maxvolt, unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    m_com->send("OVP1");
}

double Tenma722XXX::measureVoltage(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    if (!isOn(channel)) return 0.;

    return std::stod(
        m_com->sendreceive("VOUT" + std::to_string(channel) + "?"));
}
