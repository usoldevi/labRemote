#ifndef LABREMOTE_FILEUTILS_H
#define LABREMOTE_FILEUTILS_H

// std/stl
#include <string>

//! \brief Functions for handling common filesystem and file handling operations

namespace utils {
//! \brief Check if a given path exists in the current filesystem
/**
 * \param path_name The path whose existence is to be tested.
 * \return `true` if the path exists, `false` otherwise.
 */
bool path_exists(const std::string& path_name);

//! \brief Get the path to the labRemote json schema file
/**
 * This function attempts to find and return the path to the labRemote JSON
 * schema file. It checks several possible locations for the schema file,
 * based on the priority below (in the order that they are checked):
 *
 *  -# User-defined environment variable `LABREMOTE_RESOURCE_DIR`: If the
 *  environment variable `LABREMOTE_RESOURCE_DIR` is defined, the schema file
 * will be searched for under the path given by
 * `<LABREMOTE_RESOURCE_DIR>/schema/`.
 *  -# User's labRemote build directory (i.e. `CMAKE_BINARY_DIR`): In this case,
 * the schema will be searched for under `<CMAKE_BINARY_DIR>/share/schema/`.
 *  -# User's labRemote installation prefix path (i.e. `CMAKE_INSTALL_PREFIX`):
 * In this case, the schema will be searched for under
 * `<CMAKE_INSTALL_PREFIX>/share/labRemote/schema/`.
 *
 * \return An `std::string` pointing to the path of the found labRemote schema
 * file. An empty string `""` is returned if the schema file could not be
 * located.
 */
std::string labremote_schema_file();
};  // namespace utils

#endif  // LABREMOTE_FILEUTILS_H
