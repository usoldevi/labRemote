#include "Logger.h"

#include "DateTimeUtils.h"

loglevel_e logIt::loglevel = logINFO;
bool logIt::includeTimestamp = false;

const std::string logIt::logString[] = {
    "[ERROR]\033[0m   : ", "[WARNING]\033[0m : ", "[INFO]\033[0m    : ",
    "[DEBUG]\033[0m   : ", "[DEBUG1]\033[0m  : ", "[DEBUG2]\033[0m  : ",
    "[DEBUG3]\033[0m  : ", "[DEBUG4]\033[0m  : "};

const std::string logIt::logStringColor[] = {
    "\033[1;31m", "\033[1;33m", "\033[1;32m", "\033[1;34m",
    "\033[1;34m", "\033[1;34m", "\033[1;34m", "\033[1;34m"};

logIt::logIt(loglevel_e _loglevel) {
    _buffer << logStringColor[_loglevel]
            << (includeTimestamp ? "[" + utils::timestamp() + "]" : "")
            << logString[_loglevel];
}

logIt::~logIt() {
    _buffer << std::endl;
    // This is atomic according to the POSIX standard
    // http://www.gnu.org/s/libc/manual/html_node/Streams-and-Threads.html
    std::cerr << _buffer.str();
}

void logIt::incrDebug() {
    switch (loglevel) {
        case logDEBUG:
            loglevel = logDEBUG1;
            break;
        case logDEBUG1:
            loglevel = logDEBUG2;
            break;
        case logDEBUG2:
            loglevel = logDEBUG3;
            break;
        case logDEBUG3:
            loglevel = logDEBUG4;
            break;
        case logDEBUG4:
            loglevel = loglevel;
            break;
        default:
            loglevel = logDEBUG;
            break;
    }
}

void logIt::setLogLevel(loglevel_e level) { loglevel = level; }

void logIt::setTimestamp(bool doit) { includeTimestamp = doit; }
