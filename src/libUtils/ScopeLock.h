#ifndef SCOPELOCK_H
#define SCOPELOCK_H

#include <memory>

#include "ILockable.h"

//! Simple scope lock handling for `ILockable` objects.
/**
 * Lock is obtained upon object initialization and then released
 * on object descruction.
 *
 * This is very useful for blocks of code with multiple exit points
 * that otherwise would require multiple unlock statements.
 *
 * For example, the following
 *
 * ```c++
 *    bool doStuff(std::shared_ptr<ILockable> dev)
 *    {
 *      dev->lock();
 *      if(!dev->action1())
 *        {
 *          dev->unlock();
 *          return false;
 *        }
 *
 *      if(!dev->action2())
 *        {
 *          dev->unlock();
 *          return false;
 *        }
 *
 *      dev->unlock();
 *      return true;
 *    }
 * ```
 *
 * can be replaced with
 *
 * ```c++
 *    bool doStuff(std::shared_ptr<ILockable> dev)
 *    {
 *      ScopeLock lock(dev);
 *      if(!dev->action1())
 *        return false;
 *
 *      if(!dev->action2())
 *        return false;
 *
 *      return true;
 *    }
 * ```
 *
 * __Note:__ `ScopeLock` stores the `ILockable` reference as a raw
 * pointer, not a smart pointer. This allows the usage of a `ScopeLock`
 * from inside an `ILockable` itself.
 *
 */
class ScopeLock {
 public:
    //! Obtain lock on `ptr`
    ScopeLock(ILockable* ptr);
    //! Obtain lock on `ptr`
    /**
     * Helper function to allow using the constructor via a
     * `std::shared_ptr`. This is useful when a class using
     * an `ILockable` requests a lock.
     */
    ScopeLock(std::shared_ptr<ILockable> ptr);
    //! Release lock on `ptr`
    ~ScopeLock();

    // non-copyable
    ScopeLock(const ScopeLock&) = delete;
    ScopeLock& operator=(const ScopeLock&) = delete;

    // prevent creation on heap
    void* operator new(size_t) = delete;
    void* operator new[](size_t) = delete;
    void operator delete(void*) = delete;
    void operator delete[](void*) = delete;

 private:
    ILockable* m_ptr;
};

#endif  // SCOPELOCK_H
