#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <algorithm>
#include <iomanip>
#include <string>
#include <vector>

namespace utils {
//
// Credit for trim functions: https://stackoverflow.com/a/217605

//! \brief Remove whitespaces from start of string
/**
 * See std::isspace for definition of whitespace
 *
 * \param s String object from which to remove white space from
 */
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
                return !std::isspace(ch);
            }));
}

//! \brief Remove whitespaces from end of string
/**
 * See std::isspace for definition of whitespace
 *
 * \param s String object from which to remove white space from
 */
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](unsigned char ch) { return !std::isspace(ch); })
                .base(),
            s.end());
}

//! \brief Remove whitespaces from both sides of a string
/**
 * See std::isspace for definition of whitespace
 *
 * \param s String object from which to remove white space from
 */
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

//! \brief Split a string around a specified delimeter string
/**
 * See:
 * https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
 *
 * \param input: Input string to split
 * \param delim: Character/sub-string to split by
 * \return Vector of substrings
 */
static inline std::vector<std::string> split(const std::string &input,
                                             const std::string &delim) {
    std::vector<std::string> tokens;
    auto start = 0U;
    auto end = input.find(delim);
    while (end != std::string::npos) {
        std::string sub = input.substr(start, end - start);
        tokens.push_back(sub);
        start = end + delim.length();
        end = input.find(delim, start);
    }
    tokens.push_back(input.substr(start, end));
    return tokens;
}

//! \brief Convert number to scientific notation
/**
 * \param a_value number to convert
 * \param n number of digits after the decimal
 *
 * \return x.xxxxxxEyy
 */
template <typename T>
static inline std::string to_string_with_precision(const T a_value,
                                                   const int n = 3) {
    std::ostringstream out;
    out << std::setprecision(n) << a_value;
    return out.str();
}

};  // namespace utils

#endif  // STRINGUTILS_H
