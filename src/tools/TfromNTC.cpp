#include <getopt.h>
#include <pwd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

#include "EquipConf.h"
#include "IMeter.h"
#include "Logger.h"

using json = nlohmann::json;

void usage(char* argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] command [parameters]"
              << std::endl;
    std::cerr << "List of possible COMMAND:" << std::endl;
    std::cerr << "  meas-from-r           Get reading of temperature [C] from "
                 "measuring the resistance of the NTC."
              << std::endl;
    std::cerr << "  meas-from-v           Get reading of temperature [C] from "
                 "measuring the voltage of the voltage devider."
              << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -e, --equip   Config    Path to JSON equipment configuration file"
        << std::endl;
    std::cerr << " -n, --name    Name      Name of meter object (type: string)"
              << std::endl;
    std::cerr << " -c, --channel Ch        Set the channel to read from (type: "
                 "integer, default: 0)"
              << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
    std::cerr
        << " -h, --help              List the commands and options available"
        << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

float RtoC(float Rntc, float Tref_A, float Rref_B, float Bntc_C) {
    if (Rntc >= 3.e+38) return -273.15;
    float logres = log(Rntc);
    float absT = 1. / (Tref_A + Rref_B * logres + Bntc_C * pow(logres, 3));
    return absT - 273.15;
}

int main(int argc, char* argv[]) {
    // settings
    std::string name;
    int channel = 0;
    std::string channelName;
    std::string configFile;

    // get default hardware config file from ~/.labRemote
    std::string homedir;
    if (getenv("HOME") == NULL)
        homedir = getpwuid(getuid())->pw_dir;
    else
        homedir = getenv("HOME");

    if (access((homedir + "/.labRemote/hardware.json").c_str(), F_OK) != -1)
        configFile = homedir + "/.labRemote/hardware.json";

    //
    // Parse command-line
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"name", required_argument, 0, 'n'},
            {"channel", required_argument, 0, 'c'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "n:c:e:dh", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'n':
                name = optarg;
                break;
            case 'c':
                try {
                    channel = std::stoi(optarg);
                } catch (const std::invalid_argument& e) {
                    std::cerr << "Channel argument must be an integer, you "
                                 "provided \""
                              << optarg << "\"";
                    return 1;
                }
                break;
            case 'e':
                configFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (name.empty()) {
        std::cerr << "Required name argument missing." << std::endl;
        return -1;
    }

    std::string command;
    std::vector<std::string> params;
    if (optind < argc) {
        command = argv[optind++];
        std::transform(command.begin(), command.end(), command.begin(),
                       ::tolower);
        while (optind < argc) {
            std::string p(argv[optind++]);
            std::transform(p.begin(), p.end(), p.begin(), ::tolower);
            params.push_back(p);
        }
    } else {
        std::cerr << "Required command argument missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    // check if config file is provided, if not create JSON config
    EquipConf hw;
    if (configFile.empty()) {
        logger(logERROR)
            << "No input config file, either create default or use --equip.";
        return 1;
    } else {
        hw.setHardwareConfig(configFile);
    }

    logger(logDEBUG) << "Configuring meter \"" << name << "\"";
    std::shared_ptr<IMeter> meter = hw.getMeter(name);

    // Now interpret command
    logger(logDEBUG) << "Sending command to meter: " << command;

    if (command == "meas-from-r") {
        bool use_4w = false;
        // Setup utility to convert NTC resistance to temperature.
        // Note: We read R_ntc directly from a meter, so no need to setup
        // ADCDevice. We had to expose RtoC in NTCSensor class.
        // Default parameters
        float ntc_para_A = 0.8676453371787721E-3;
        float ntc_para_B = 2.541035850140508E-4;
        float ntc_para_C = 1.868520310774293E-7;
        // Set the ntc parameters if provided
        if (params.size() == 3) {
            logger(logDEBUG)
                << "Set parameters to " << params[0] << params[1] << params[2];
            ntc_para_A = std::stod(params[0]);
            ntc_para_B = std::stod(params[1]);
            ntc_para_C = std::stod(params[2]);
        }
        double ntc_R = meter->measureRES(channel, use_4w);
        if (ntc_R <= 0) {
            logger(logERROR) << "Invalid value from measuring the resistance "
                                "with the multimeter!";
            throw std::runtime_error("Exiting the program.");
        }
        double temp = RtoC(ntc_R, ntc_para_A, ntc_para_B, ntc_para_C);
        // Read R
        logger(logDEBUG) << "Resistance: " << ntc_R << " Ohms";
        // Read T
        if (logIt::loglevel >= logDEBUG) std::cout << "Temperature: ";
        std::cout << temp;
        if (logIt::loglevel >= logDEBUG) std::cout << " C";
        std::cout << std::endl;
    } else if (command == "meas-from-v") {
        logger(logERROR) << "Measure from V not implemented.";
        throw std::runtime_error("Exiting the program.");
    }
    logger(logDEBUG) << "All done.";
    return 0;
}
