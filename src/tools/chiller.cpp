#include <getopt.h>
#include <pwd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include "EquipConf.h"
#include "Logger.h"
#include "PolySciLM.h"

void usage(char* argv[]) {
    std::cerr << "" << std::endl;
    std::cerr << "Usage: " << argv[0] << " [options] command [parameters]"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of possible COMMAND:" << std::endl;
    std::cerr
        << " set-temp -- T    Set the target temperature in degrees-Celsius"
        << std::endl;
    std::cerr << " get-temp         Get the set target temperature"
              << std::endl;
    std::cerr << " set-ramp -- R    Set the ramp rate in degrees-Celsius/minute"
              << std::endl;
    std::cerr << " get-ramp         Get the ramp rate" << std::endl;
    std::cerr << " meas-temp        Get reading of current temperature in "
                 "degrees-Celsius"
              << std::endl;
    std::cerr << " turn-on          Turn on the chiller" << std::endl;
    std::cerr << " turn-off         Turn off the chiller" << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -e, --equip config.json  Configuration file with the "
                 "chiller definition"
              << std::endl;
    std::cerr << " -n, --name Chiller       Name of the chiller from equipment "
                 "configuration"
              << std::endl;
    std::cerr << " -d, --debug              Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
    std::cerr << " -h, --help               Print this help message"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char* argv[]) {
    // inputs
    float target_temp = 15;
    std::string configFile = "";
    std::string chillerName = "";

    std::string homedir;
    if (getenv("HOME") == NULL)
        homedir = getpwuid(getuid())->pw_dir;
    else
        homedir = getenv("HOME");

    if (access((homedir + "/.labRemote/hardware.json").c_str(), F_OK) != -1)
        configFile = homedir + "/.labRemote/hardware.json";

    // Parse command-line
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    int c;
    while (true) {
        int option_index = 0;
        static struct option long_options[] = {
            {"name", required_argument, 0, 'n'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "n:e:dh", long_options, &option_index);
        if (c == -1) break;
        switch (c) {
            case 'n':
                chillerName = optarg;
                break;
            case 'e':
                configFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                usage(argv);
                return 1;
        }
    }

    std::string command;
    std::vector<std::string> params;
    if (optind < argc) {
        command = argv[optind++];
        std::transform(command.begin(), command.end(), command.begin(),
                       ::tolower);
        while (optind < argc) {
            std::string p(argv[optind++]);
            std::transform(p.begin(), p.end(), p.begin(), ::tolower);
            params.push_back(p);
        }
    } else {
        std::cerr << "Required command argument missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    // Create hardware cache
    EquipConf hw;
    if (configFile.empty()) {
        logger(logERROR)
            << "No input config file, either create default or use --equip.";
        return 1;
    } else {
        hw.setHardwareConfig(configFile);
    }

    logger(logDEBUG) << "Configuring chiller.";
    std::shared_ptr<IChiller> chiller = hw.getChiller(chillerName);
    chiller->init();

    // interpret the command
    logger(logDEBUG) << "Sending command to chiller.";

    if (command == "set-temp") {
        if (params.size() != 1) {
            logger(logERROR)
                << "Invalid number of parameters to set-temp command.";
            usage(argv);
            return 1;
        }
        logger(logDEBUG) << "Set target temperature to " << params[0]
                         << " deg-C";
        chiller->setTargetTemperature(std::stod(params[0]));
    } else if (command == "get-temp") {
        if (logIt::loglevel >= logDEBUG) std::cout << "Target-temp: ";
        std::cout << chiller->getTargetTemperature();
        if (logIt::loglevel >= logDEBUG) std::cout << " deg-C";
        std::cout << std::endl;
    } else if (command == "set-ramp") {
        if (params.size() != 1) {
            logger(logERROR)
                << "Invalid number of parameters to set-ramp command.";
            usage(argv);
            return 1;
        }
        logger(logDEBUG) << "Set ramp rate to " << params[0] << " deg-C/minute";
        chiller->setRampRate(std::stof(params[0]));
    } else if (command == "get-ramp") {
        if (logIt::loglevel >= logDEBUG) std::cout << "Ramp-Rate: ";
        std::cout << chiller->getRampRate();
        if (logIt::loglevel >= logDEBUG) std::cout << " deg-C/minute";
        std::cout << std::endl;
    } else if (command == "meas-temp") {
        if (logIt::loglevel >= logDEBUG) std::cout << "Measured-temp: ";
        std::cout << chiller->measureTemperature();
        if (logIt::loglevel >= logDEBUG) std::cout << " deg-C";
        std::cout << std::endl;
    } else if (command == "turn-on") {
        logger(logDEBUG) << "Turning chiller ON";
        chiller->turnOn();
    } else if (command == "turn-off") {
        logger(logDEBUG) << "Turning chiller OFF";
        chiller->turnOff();
    } else {
        usage(argv);
    }

    logger(logDEBUG) << "All done.";
    return 0;
}
