// ftdi/usb
#ifdef LIBUSB_ENABLED
#include <libftdi1/ftdi.h>
#include <libusb-1.0/libusb.h>
#endif

// std/stl
#include <getopt.h>

#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

// labremote
#include "Logger.h"

void usage(char* argv[]) {
    std::cerr << "Usage: " << argv[0] << " [OPTIONS] [vendor:product]"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << "Options:" << std::endl;
    std::cerr << " -d, --debug   Increase verbosity of logging output"
              << std::endl;
    std::cerr << " -h, --help    Print this help message" << std::endl;
    std::cerr << "Optional positional arguments:" << std::endl;
    std::cerr << " vendor:product   FTDI (USB) device's vendor and product "
                 "hex-string IDs (default: 0:0, to list all FTDI devices)"
              << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char* argv[]) {
#ifndef LIBUSB_ENABLED
    std::cerr << "libusb marked as not enabled, cannot run program \""
              << argv[0] << "\"" << std::endl;
    return 1;
#else

    int _vid = 0;
    int _pid = 0;

    static struct option long_options[] = {{"debug", no_argument, NULL, 'd'},
                                           {"help", no_argument, NULL, 'h'},
                                           {0, 0, 0, 0}};
    int c;
    while ((c = getopt_long(argc, argv, "hd", long_options, NULL)) != -1) {
        switch (c) {
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 0;
            case '?':
                std::cerr << "Invalid option \"" << c << "\" provided"
                          << std::endl;
                return 1;
        }  // switch
    }      // while

    if ((argc - optind) > 1) {
        logger(logERROR) << "Too many position arguments provided";
        return 1;
    }
    if (optind == (argc - 1)) {
        std::string idstring = argv[optind++];
        std::size_t sep = idstring.find(":");
        if (sep == std::string::npos) {
            logger(logERROR) << "Device vendor:product string formatted "
                                "incorrectly, must be 0000:0000 (hex strings)";
            return 1;
        }
        _vid = std::stoi(idstring.substr(0, sep), 0, 16);
        _pid = std::stoi(idstring.substr(sep + 1), 0, 16);
    }

    logger(logDEBUG) << "Listing devices with VID:PID = 0x" << std::hex << _vid
                     << ":0x" << _pid << std::dec;

    int rc = 0;
    int device_count = 0;

    struct ftdi_context ftdi;
    struct ftdi_device_list *devlist, *curdev;
    if ((rc = ftdi_init(&ftdi)) != 0) {
        logger(logERROR) << "ftdi_init failed";
        return 1;
    }

    if ((device_count = ftdi_usb_find_all(&ftdi, &devlist, _vid, _pid) < 0)) {
        logger(logERROR) << "ftdi_usb_find_all failed with error " << rc;
        ftdi_usb_close(&ftdi);
        return 1;
    }
    logger(logDEBUG) << "Found " << device_count
                     << " FTDI devices connected on this machine";

    std::vector<std::string> _mfr;
    std::vector<std::string> _des;
    std::vector<std::string> _ser;
    std::vector<uint16_t> _vendor;
    std::vector<uint16_t> _product;

    int idev = 0;
    for (curdev = devlist; curdev != NULL; idev++) {
        libusb_device_descriptor desc = {0};
        libusb_get_device_descriptor(curdev->dev, &desc);
        _vendor.push_back(desc.idVendor);
        _product.push_back(desc.idProduct);

        char mfr[128], des[128], ser[128];
        if ((rc = ftdi_usb_get_strings(&ftdi, curdev->dev, mfr, 128, des, 128,
                                       ser, 128)) != 0) {
            _mfr.push_back("");
            _des.push_back("");
            _ser.push_back("");
        } else {
            _mfr.push_back(mfr);
            _des.push_back(des);
            _ser.push_back(ser);
        }

        // move to the next FTDI device in the list
        curdev = curdev->next;
    }
    ftdi_usb_close(&ftdi);

    logger(logINFO) << "Found " << _mfr.size() << " FTDI devices matching "
                    << std::setfill('0') << std::setw(4) << std::hex << _vid
                    << ":" << std::setfill('0') << std::setw(4) << _pid
                    << std::dec;
    for (size_t ii = 0; ii < _mfr.size(); ii++) {
        logger(logINFO) << "Device #" << ii << " (" << std::hex
                        << std::setfill('0') << std::setw(4) << _vendor.at(ii)
                        << ":" << std::setfill('0') << std::setw(4)
                        << _product.at(ii) << ")" << std::dec;
        logger(logINFO) << "    MANUFRACTURER ID : " << _mfr.at(ii);
        logger(logINFO) << "    PRODUCT ID       : " << _des.at(ii);
        logger(logINFO) << "    SERIAL NUMBER    : " << _ser.at(ii);
    }
#endif
    return 0;
}
