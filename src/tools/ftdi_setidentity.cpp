#include <getopt.h>
#include <libftdi1/ftdi.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <iomanip>
#include <iostream>

#include "Logger.h"

//------ SETTINGS
uint32_t vendor;
uint32_t product;
std::string mode;
std::string value;
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0]
              << " vendor:product manufacturer/product/serial value"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -d, --debug       Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    // Parse command-line
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {{"debug", no_argument, 0, 'd'},
                                               {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (optind > argc - 3) {
        logger(logERROR) << "Extra positional arguments.";
        usage(argv);
        return 1;
    }

    std::string idstring = argv[optind++];
    mode = argv[optind++];
    value = argv[optind++];

    // parse the id string
    std::size_t sep = idstring.find(':');
    if (sep == std::string::npos) {
        logger(logERROR) << "vendor:product must be in format 0000:0000";
        usage(argv);
        return 1;
    }
    vendor = std::stoi(idstring.substr(0, sep), 0, 16);
    product = std::stoi(idstring.substr(sep + 1), 0, 16);

    logger(logDEBUG) << "Settings:";
    logger(logDEBUG) << " vendor = 0x" << std::hex << std::setw(4)
                     << std::setfill('0') << vendor << std::dec;
    logger(logDEBUG) << " product = 0x" << std::hex << std::setw(4)
                     << std::setfill('0') << product << std::dec;
    logger(logDEBUG) << " mode = " << mode;
    logger(logDEBUG) << " value = " << value;

    //
    // Open FTDI
    int ret = 0;
    struct ftdi_context ftdi;

    if ((ret = ftdi_init(&ftdi)) != 0) {
        logger(logERROR) << "ftdi_init failed: " << ret;
        return 1;
    }

    if ((ret = ftdi_usb_open(&ftdi, 0x0403, 0x6014)) < 0) {
        logger(logERROR) << "ftdi_usb_open failed: " << ret;
        return 1;
    }

    //
    // Read EEPROM
    if ((ret = ftdi_read_eeprom(&ftdi)) < 0) {
        logger(logERROR) << "ftdi_read_eeprom failed: " << ret;
        return 1;
    }

    if ((ret = ftdi_eeprom_decode(&ftdi, 0)) < 0) {
        logger(logERROR) << "ftdi_eeprom_decode failed: " << ret;
        return 1;
    }

    //
    // Update strings
    char *cstr = new char[value.length() + 1];  // non-const :(
    strcpy(cstr, value.c_str());

    if (mode == "manufacturer") {
        ret = ftdi_eeprom_set_strings(&ftdi, cstr, NULL, NULL);
    } else if (mode == "product") {
        ret = ftdi_eeprom_set_strings(&ftdi, NULL, cstr, NULL);
    } else if (mode == "serial") {
        ret = ftdi_eeprom_set_strings(&ftdi, NULL, NULL, cstr);
    } else {
        logger(logERROR) << "Mode must be manufacturer or product or serial";
        usage(argv);
    }

    if (ret < 0) {
        logger(logERROR) << "ftdi_eeprom_set_strings failed: " << ret;
        return 1;
    }

    delete[] cstr;

    //
    // Build and save the EEPROM
    if ((ret = ftdi_eeprom_build(&ftdi)) < 0) {
        logger(logERROR) << "ftdi_eeprom_decode failed: " << ret;
        return 1;
    }

    if ((ret = ftdi_write_eeprom(&ftdi)) < 0) {
        logger(logERROR) << "ftdi_eeprom_decode failed: " << ret;
        return 1;
    }

    //
    // Clean up
    ftdi_usb_close(&ftdi);

    return 0;
}
