#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "Logger.h"
#include "SerialCom.h"

#ifdef LINUXGPIB
#include <gpib/ib.h>

#include "GPIBNICom.h"
#endif

//------ SETTINGS
std::string port;
//---------------

void usage(char* argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] usbport" << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -d, --debug       Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
#ifdef LINUXGPIB
    std::cerr << " -l, --linuxgpib   Use linux-GPIB package to earch for "
                 "devices instead of serial."
              << std::endl;
#endif
    std::cerr << "" << std::endl;
    std::cerr << "" << std::endl;
}

int main(int argc, char* argv[]) {
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    // Parse command-line
    bool useLG = false;
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"debug", no_argument, 0, 'd'},
#ifdef LINUXGPIB
            {"linuxgpib", no_argument, 0, 'l'},
#endif
            {0, 0, 0, 0}};

#ifdef LINUXGPIB
        c = getopt_long(argc, argv, "dl", long_options, &option_index);
#else
        c = getopt_long(argc, argv, "d", long_options, &option_index);
#endif
        if (c == -1) break;

        switch (c) {
            case 'd':
                logIt::incrDebug();
                break;
            case 'l':
                useLG = true;
                break;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (argc != optind + 1) {
        std::cerr << "Required usbport argument missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }
    port = argv[optind++];

    //
#ifdef LINUXGPIB
    if (useLG) {
        Addr4882_t addresses[32];
        Addr4882_t instruments[31];

        // create addresses array
        for (int i = 0; i < 30; i++) {
            addresses[i] = (Addr4882_t)(i + 1);
        }
        addresses[30] = NOADDR;

        // find listeners and fill instruments array
        FindLstn(std::stoi(port), addresses, instruments, 31);
        int numbInstruments = ibcntl;
        if (numbInstruments == 0) {
            std::cout << "WARNING : No instruments found on interface GPIB"
                      << port << std::endl;
            return 1;
        }
        // loop over instruments array
        for (int i = 0; i < numbInstruments; i++) {
            unsigned int PAD = GetPAD(instruments[i]);
            unsigned int SAD = GetSAD(instruments[i]);
            std::shared_ptr<GPIBNICom> com(
                new GPIBNICom(std::stoi(port), PAD, SAD));
            com->init();
            std::string idrep = com->sendreceive(std::string("*IDN?"));
            std::cout << "GPIB" << port << ", PAD " << PAD << ", SAD " << SAD
                      << ": " << idrep << std::endl;
        }

    }

    else {
#endif
        // Create usb connection and scan address ranges

        // Create and initialize connection
        std::shared_ptr<SerialCom> com(new SerialCom(port));
        com->send("++auto 0\n\r");

        // Start scanning
        std::string buf;
        for (uint32_t addr = 0; addr < 31; addr++) {
            std::cout << std::setw(2) << addr << ": ";

            com->send("++addr " + std::to_string(addr) + "\n\r");
            com->send("*IDN?\n\r");
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            com->send("++read eoi\n\r");
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            try {
                buf = com->receive();
                buf.erase(std::remove(buf.begin(), buf.end(), '\n'), buf.end());
                buf.erase(std::remove(buf.begin(), buf.end(), '\r'), buf.end());
                std::cout << buf;
            } catch (const std::runtime_error& e) { /* Nothing connected here */
            }
            std::cout << std::endl;
        }
#ifdef LINUXGPIB
    }
#endif

    return 0;
}
