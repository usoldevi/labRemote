#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "ChillerRegistry.h"
#include "EquipConf.h"
#include "IChiller.h"
#include "Logger.h"

loglevel_e loglevel = logINFO;

int main() {
    // get list of registered classes
    std::vector<std::string> chillerList = EquipRegistry::listChiller();

    EquipConf hw;

    int classLength = 20;
    int modelLength = 40;
    std::cout << "| Class" << std::setfill(' ') << std::setw(classLength - 6)
              << "|"
              << " Models" << std::setw(modelLength - 7) << "|" << std::endl;
    std::cout << "|" << std::setfill('-') << std::setw(classLength) << "|"
              << std::setfill('-') << std::setw(modelLength) << "|"
              << std::endl;

    int l;

    for (int i = 0; i < chillerList.size(); i++) {
        l = 0;

        std::cout << "| " << chillerList.at(i) << std::setfill(' ')
                  << std::setw(classLength - chillerList.at(i).size() - 1)
                  << "|";
        const std::string name = chillerList.at(i);

        // create instance of IChiller
        std::shared_ptr<IChiller> chiller =
            EquipRegistry::createChiller(name, name);

        // get list of models
        std::vector<std::string> models = chiller->getListOfModels();

        if (models.empty()) {
            std::cout << " N.A" << std::setfill(' ')
                      << std::setw(modelLength - 4) << "|" << std::endl;
        } else {
            for (int j = 0; j < models.size(); j++) {
                std::cout << " " << models.at(j);
                l = l + models.at(j).size() + 1;
            }
            std::cout << std::setfill(' ') << std::setw(modelLength - l) << "|"
                      << std::endl;
        }
    }

    return 0;
}
