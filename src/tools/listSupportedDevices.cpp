#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "DeviceComRegistry.h"
#include "Logger.h"

loglevel_e loglevel = logINFO;

int main() {
    // get list of registered classes
    std::map<std::string, std::vector<std::string>> devcom_list =
        DeviceComRegistry::listDevCom();

    std::vector<std::string> types;
    std::vector<std::string> device_listings;
    size_t max_len_type = 0;
    size_t max_len_listing = 0;

    std::stringstream sx;
    for (auto devcom : devcom_list) {
        std::string device_type = devcom.first;
        if (device_type.length() > max_len_type)
            max_len_type = device_type.length();
        types.push_back(" " + device_type);

        auto device_names = devcom.second;
        std::sort(device_names.begin(), device_names.end());
        sx.str("");
        for (auto device_name : device_names) {
            sx << " " << device_name;
        }  // device
        if (sx.str().length() > max_len_listing)
            max_len_listing = sx.str().length();
        device_listings.push_back(sx.str());
    }  // devcom

    std::string type_header = " Device Type";
    if (type_header.length() > max_len_type)
        max_len_type = type_header.length();

    std::string listing_header = " Supported Devices";
    if (listing_header.length() > max_len_listing)
        max_len_listing = listing_header.length();

    size_t type_length = max_len_type + 10;
    size_t listing_length = max_len_listing + 10;

    std::cout << "|" << std::setfill(' ') << std::setw(type_length) << std::left
              << type_header << "|" << std::setfill(' ')
              << std::setw(listing_length) << std::left << listing_header << "|"
              << std::endl;
    std::cout << "|" << std::setfill('-') << std::setw(type_length) << ""
              << "|" << std::setfill('-') << std::setw(listing_length) << ""
              << "|" << std::endl;
    for (size_t i = 0; i < types.size(); i++) {
        std::cout << "|" << std::setfill(' ') << std::setw(type_length)
                  << std::left << types.at(i) << "|" << std::setfill(' ')
                  << std::setw(listing_length) << std::left
                  << device_listings.at(i) << "|" << std::endl;
    }
    return 0;
}
