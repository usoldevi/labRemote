import labRemote
import argparse
import logging
import pathlib
import sys

logging.basicConfig()
logger = logging.getLogger(__name__)


def set_current(hw, ps, args):
    logger.debug(f"Set current to {args.current}")
    ps.setCurrentLevel(args.current)
    if args.max_voltage >= 0:
        logger.debug(f"Set maximum voltage to {args.max_voltage}")
        ps.setVoltageProtect(args.max_voltage)
    return 0


def get_current(hw, ps, args):
    current = ps.getCurrentLevel()
    logger.debug(f"Current: {current} A")
    logger.info(current)
    return 0


def meas_current(hw, ps, args):
    current = ps.measureCurrent()
    logger.debug(f"Current: {current} A")
    logger.info(current)
    return 0


def set_voltage(hw, ps, args):
    logger.debug(f"Set voltage to {args.voltage}")
    ps.setVoltageLevel(args.voltage)
    if args.max_current >= 0:
        logger.debug(f"Set maximum voltage to {args.max_current}")
        ps.setVoltageProtect(args.max_current)
    return 0


def get_voltage(hw, ps, args):
    voltage = ps.getVoltageLevel()
    logger.debug(f"Voltage: {voltage} V")
    logger.info(voltage)
    return 0


def meas_voltage(hw, ps, args):
    voltage = ps.measureVoltage()
    logger.debug(f"Voltage: {voltage} V")
    logger.info(voltage)
    return 0


def program(hw, ps, args):
    logger.debug("Programming power-supply.")
    ps.program()
    if args.on:
        logger.debug("Powering ON.")
        ps.turnOn()
    return 0


def power_on(hw, ps, args):
    logger.debug("Initializing power-supply.")
    if args.voltage_level and args.current_protect:
        ps.setVoltageLevel(args.voltage_level)
        ps.setCurrentProtect(args.current_protect)
    logger.debug("Powering ON.")
    ps.turnOn()
    return 0


def power_off(hw, ps, args):
    logger.debug("Powering OFF.")
    ps.turnOff()
    return 0


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n",
        "--name",
        default="PS",
        help="Name of the power supply from equipment list",
    )
    parser.add_argument(
        "-c",
        "--channel",
        default="",
        help="Set PS channel (name or number when used in conjunction with --name) to control",
    )
    parser.add_argument(
        "-e",
        "--equip",
        default=str(
            pathlib.Path.home()
            .joinpath(".labRemote")
            .joinpath("hardware.json")
            .absolute()
        ),
        help="config.json Configuration file with the power supply definition",
    )
    parser.add_argument(
        "-d",
        "--debug",
        help="Enable more verbose printout, use multiple for increased debug level",
        action="store_true",
    )

    subparsers = parser.add_subparsers(help="command action")
    parser_set_current = subparsers.add_parser(
        "set-current", help="Set current [A] with maximum voltage [V]"
    )
    parser_set_current.add_argument("current", type=float)
    parser_set_current.add_argument(
        "max_voltage",
        type=float,
        nargs="?",
        default=-1,
        help="Set maximum voltage, -1 for no protection.",
    )
    parser_set_current.set_defaults(func=set_current)

    parser_get_current = subparsers.add_parser(
        "get-current", help="Get set current level [A]"
    )
    parser_get_current.set_defaults(func=get_current)

    parser_meas_current = subparsers.add_parser(
        "meas-current", help="Get reading of current [A]"
    )
    parser_meas_current.set_defaults(func=meas_current)

    parser_set_voltage = subparsers.add_parser(
        "set-voltage", help="Set voltage [V] with maximum current [I]"
    )
    parser_set_voltage.add_argument("voltage", type=float)
    parser_set_voltage.add_argument(
        "max_current",
        type=float,
        nargs="?",
        default=-1,
        help="Set maximum current, -1 for no protection.",
    )
    parser_set_voltage.set_defaults(func=set_voltage)

    parser_get_voltage = subparsers.add_parser(
        "get-voltage", help="Get set voltage level [V]"
    )
    parser_get_voltage.set_defaults(func=get_voltage)

    parser_meas_voltage = subparsers.add_parser(
        "meas-voltage", help="Get reading of voltage [V]"
    )
    parser_meas_voltage.set_defaults(func=meas_voltage)

    parser_program = subparsers.add_parser(
        "program",
        help="Execute the program block and optionally turn on the power supply",
    )
    parser_program.add_argument("on", choices=["", "on"], nargs="?", type=str)
    parser_program.set_defaults(func=program)

    parser_power_on = subparsers.add_parser(
        "power-on",
        help="Power ON PS, optionally setting voltage to V in Volts and current to I in Ampere",
    )
    parser_power_on.add_argument("voltage_level", nargs="?", type=float)
    parser_power_on.add_argument("current_protect", nargs="?", type=float)
    parser_power_on.set_defaults(func=power_on)

    parser_power_off = subparsers.add_parser("power-off", help="Power OFF PS")
    parser_power_off.set_defaults(func=power_off)

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        labRemote.incrDebug()

    logging.debug("Debug mode is on")

    hw = labRemote.ec.EquipConf()
    hw.setHardwareConfig(args.equip)
    logger.debug("Configuring power-supply")
    if not args.channel:
        psReal = hw.getPowerSupply(args.name)
        ps = labRemote.ps.PowerSupplyChannel(f"{args.name}1", psReal, 1)
    else:
        ps = hw.getPowerSupplyChannel(args.channel)

    code = args.func(hw, ps, args)
    logger.debug("All done.")
    sys.exit(code)
