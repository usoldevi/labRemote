#include <getopt.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include "DataSinkConf.h"
#include "EquipConf.h"
#include "IDataSink.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

//------ SETTINGS
uint32_t tsleep = 10;
std::string configFile;
std::vector<std::string> channelNames;
std::string streamName;
//---------------

bool quit = false;
void cleanup(int signum) { quit = true; }

void usage(char* argv[]) {
    std::cerr
        << "Usage: " << argv[0]
        << " configfile.json datastreamName channelname0 [channelname1 ...]"
        << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -t, --time        Milliseconds between measurements (default: "
        << tsleep << ")" << std::endl;
    std::cerr << " -d, --debug       Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "" << std::endl;
}

int main(int argc, char* argv[]) {
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    // Parse command-line
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {{"time", no_argument, 0, 't'},
                                               {"debug", no_argument, 0, 'd'},
                                               {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "t:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 't':
                tsleep = std::atoi(optarg);
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (optind > argc - 3) {
        logger(logERROR) << "Missing positional arguments.";
        usage(argv);
        return 1;
    }

    configFile = argv[optind++];
    streamName = argv[optind++];
    channelNames = std::vector<std::string>(&argv[optind], &argv[argc]);

    logger(logDEBUG) << "Settings:";
    logger(logDEBUG) << " Config file: " << configFile;
    logger(logDEBUG) << " stream name: " << streamName;
    logger(logDEBUG) << " Channels: ";
    for (const std::string& channelName : channelNames)
        logger(logDEBUG) << "   " << channelName;

    // Register interrupt for cleanup
    signal(SIGINT, cleanup);

    // Create sink
    DataSinkConf ds;
    ds.setHardwareConfig(configFile);
    std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

    //
    // Create hardware database
    EquipConf hw;
    hw.setHardwareConfig(configFile);

    logger(logDEBUG) << "Configuring power-supply.";
    std::vector<std::shared_ptr<PowerSupplyChannel>> PSs;
    std::transform(channelNames.begin(), channelNames.end(),
                   std::back_inserter(PSs),
                   [&hw](const std::string& channelName) {
                       return hw.getPowerSupplyChannel(channelName);
                   });

    while (!quit) {
        std::chrono::time_point<std::chrono::system_clock> time =
            std::chrono::system_clock::now();
        for (std::shared_ptr<PowerSupplyChannel> PS : PSs) {
            stream->setTag("Channel", PS->getName());
            stream->startMeasurement("powersupply", time);
            stream->setField("Voltage", PS->measureVoltage());
            stream->setField("Current", PS->measureCurrent());
            stream->recordPoint();
            stream->endMeasurement();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(tsleep));
    }

    return 0;
}
